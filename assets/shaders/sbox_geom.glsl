#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec2 rect;
out vec2 sh_texcoord;

#define ATLAS_SIZE 8
#define TEXTURE_ID 35

void box(vec4 pos, vec2 rect)
{
    gl_Position = pos + vec4(-rect.x, -rect.y, 0.0, 0.0);
    // sh_texcoord = vec2(0.9999f/ATLAS_SIZE, 1.0f/ATLAS_SIZE);
    sh_texcoord = vec2(1.0f/ATLAS_SIZE, 1.0f/ATLAS_SIZE);
    EmitVertex();

    gl_Position = pos + vec4( rect.x, -rect.y, 0.0, 0.0);
    // sh_texcoord = vec2(-0.0001f, 1.0f/ATLAS_SIZE);
    sh_texcoord = vec2(0.0f, 1.0f/ATLAS_SIZE);
    EmitVertex();

    gl_Position = pos + vec4(-rect.x, rect.y, 0.0, 0.0);
    // sh_texcoord = vec2(0.9999f/ATLAS_SIZE, 0.0f);
    sh_texcoord = vec2(1.0f/ATLAS_SIZE, 0.0f);
    EmitVertex();

    gl_Position = pos + vec4(rect.x, rect.y, 0.0, 0.0);
    // sh_texcoord = vec2(-0.0001f, 0.0f);
    sh_texcoord = vec2(0.0f, 0.0f);
    EmitVertex();

    EndPrimitive();
}

void main() {
    box(gl_in[0].gl_Position, rect);
}
