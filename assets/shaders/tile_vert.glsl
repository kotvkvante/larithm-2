#version 330 core

uniform float tile_size;
uniform vec2 tile_offset;
uniform int tile_texture;
uniform mat4 mvp;

out vec2 texture_coord;

#define TEXTURE_MAP_SIZE 8
#define TMSI TEXTURE_MAP_SIZE
#define TMSF float(TEXTURE_MAP_SIZE)

void main()
{
    vec4 vertex_position;

    vertex_position.x = (gl_VertexID + 1) / 2 % 2;
    vertex_position.y = gl_VertexID / 2;
    vertex_position.z = 0.0;
    vertex_position.w = 1.0;

    texture_coord.x = (vertex_position.x + float(tile_texture % TMSI)) / TMSF;
    texture_coord.y = (1-vertex_position.y + float(tile_texture / TMSI)) / TMSF;

    vertex_position.x = (vertex_position.x + tile_offset.x) * tile_size;
    vertex_position.y = (vertex_position.y + tile_offset.y) * tile_size;

    vertex_position = mvp * vec4(vertex_position);
    gl_Position = vertex_position;
};
