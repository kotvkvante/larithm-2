#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec2 vertex_texcoord;

uniform ivec2 texmap_coord;
uniform mat4  mvp;
out vec2 texcoord;

void main()
{
   texcoord.x = (float(texmap_coord.x) + vertex_texcoord.x) / 8;
   texcoord.y = (float(texmap_coord.y) + vertex_texcoord.y) / 8;
   gl_Position = mvp * vec4(vertex_position, 1.0);
};
