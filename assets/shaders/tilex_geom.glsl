#version 330 core

#define NOMIRROR (0)       				/// 00 00
#define	MIRRORX   (1 << 2)				/// 00 10
#define MIRRORY   (1 << 3)				/// 00 01
#define MIRRORXY  (MIRRORX | MIRRORY)	/// 00 11
#define ROTATE0   (0)					/// 00 00
#define ROTATE90  (1)					/// 10 00
#define ROTATE180 (2)					/// 01 00
#define ROTATE270 (3)					/// 11 00

#define ROTATION  (ROTATE0 | ROTATE180 )
#define MIRROR 	  (MIRRORXY)

uniform vec2 wh;

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in TOGEOM {
	int transform;
	int texture;
} fromgeom[];

out vec3 tofrag_texcoord;

#define d (64)
float get_actual_layer(int layer)
{
	return max(0, min(d - 1, floor(layer + 0.5)));
}

void main()
{
	vec4 positions[4];
	positions[0] = vec4(-1.0, -1.0, 0.0, 0.0 );
	positions[1] = vec4( 1.0, -1.0, 0.0, 0.0 );
	positions[2] = vec4(-1.0,  1.0, 0.0, 0.0 );
	positions[3] = vec4( 1.0,  1.0, 0.0, 0.0 );

	float z = float(fromgeom[0].texture);
	vec3 texcoords[4];
	texcoords[0] = vec3(0.0, 1.0, z);
	texcoords[1] = vec3(1.0, 1.0, z);
	texcoords[2] = vec3(0.0, 0.0, z);
	texcoords[3] = vec3(1.0, 0.0, z);

	switch (fromgeom[0].transform & ROTATION) {
		case 0: break;
		case 1: {
			vec3 tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = texcoords[3];
			texcoords[3] = texcoords[2];
			texcoords[2] = tmp;
		} break;
		case 2: {
			vec3 tmp;
			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case 3: {
			vec3 tmp = texcoords[0];
			texcoords[0] = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = texcoords[1];
			texcoords[1] = tmp;
		} break;
	}

	switch (fromgeom[0].transform & MIRROR) {
		case 0: break;
		case MIRRORX: {
			vec3 tmp;
			tmp = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case MIRRORY: {
			vec3 tmp;
			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case MIRRORXY: {
			vec3 tmp;
			tmp = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
	}

	vec4 pos = gl_in[0].gl_Position;


	gl_Position = pos + vec4(-wh.x, -wh.y, 0.0, 0.0);
    tofrag_texcoord = texcoords[0];
    EmitVertex();

    gl_Position = pos + vec4( wh.x, -wh.y, 0.0, 0.0);
    tofrag_texcoord = texcoords[1];
    EmitVertex();

    gl_Position = pos + vec4(-wh.x, wh.y, 0.0, 0.0);
    tofrag_texcoord = texcoords[2];
    EmitVertex();

    gl_Position = pos + vec4(wh.x, wh.y, 0.0, 0.0);
    tofrag_texcoord = texcoords[3];
    EmitVertex();

	EndPrimitive();
}
