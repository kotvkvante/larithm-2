#version 330 core

in vec2 texcoord;

uniform sampler2D texture_map;
out vec4 output_color;

void main()
{
   output_color = texture(texture_map, texcoord);
};
