#version 330 core

in vec2 texcoord;

uniform sampler2D texture_map;
out ivec2 output_color;

flat in int _id;

void main()
{
    vec4 color;

    color = texture(texture_map, texcoord);

    if (color.a == 0.0f) discard;

    output_color = ivec2(_id, 0);
};
