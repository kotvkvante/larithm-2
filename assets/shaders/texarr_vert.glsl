#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;

uniform mat4 mvp;
uniform int layer;

out vec3 sh_texcoord;
#define d (64)

void main()
{
	float actual_layer = max(0, min(d - 1, floor(layer + 0.5)));
	sh_texcoord = vec3(texcoord, actual_layer);
    gl_Position = mvp * vec4(position, 0.0, 1.0);
}
