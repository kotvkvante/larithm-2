#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
uniform mat4 mvp;
uniform int texture_id;

// out vec2 sh_texcoord;

// #define TEXTURE_MAP_SIZE 8
// #define TMSI TEXTURE_MAP_SIZE
// #define TMSF float(TEXTURE_MAP_SIZE)

void main()
{
    // sh_texcoord.x = (texcoord.x + float(texture_id % TMSI)) / TMSF;
    // sh_texcoord.y = (texcoord.y + float(texture_id / TMSI)) / TMSF;
    gl_Position = mvp * vec4(position, 0.0, 1.0);
}
