#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in int transform;
layout (location = 2) in int texture;

uniform mat4 mvp;

out TOGEOM {
	int transform;
	int texture;
} togeom;

void main()
{
	togeom.transform = transform;
	togeom.texture = texture;
    gl_Position = mvp * vec4(position, 0.0, 1.0);
}
