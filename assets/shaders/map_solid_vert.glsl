#version 330 core

// layout (location = 0) in vec2 vertex_position;
layout (location = 0) in vec2 tile_offset;
layout (location = 1) in int tile_texture;
layout (location = 2) in int id;

uniform mat4 mvp;
out vec2 texcoord;
flat out int _id;
out ivec2 _tile_offset;
// out vec2 _tile_offset;

#define TEXTURE_MAP_SIZE 8
#define TMSI TEXTURE_MAP_SIZE
#define TMSF float(TEXTURE_MAP_SIZE)

#define TILE_SIZE 64.0f

void main()
{
    vec2 vertex_position;
    _id = id;
    // _tile_offset = tile_offset;

    // ((i + 1) >> 1) % 2
    vertex_position.x = (gl_VertexID + 1) / 2 % 2;
    vertex_position.y = gl_VertexID / 2;

    texcoord.x = (vertex_position.x + float( tile_texture % TMSI)) / TMSF;
    texcoord.y = (vertex_position.y + float( tile_texture / TMSI)) / TMSF;

    vertex_position.x = (vertex_position.x + tile_offset.x) * TILE_SIZE;
    vertex_position.y = (vertex_position.y + tile_offset.y) * TILE_SIZE;

    gl_Position = mvp * vec4(vertex_position, 0.0, 1.0);
};
