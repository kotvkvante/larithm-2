#version 330 core

uniform mat4 mvp;
uniform vec2 position;

void main()
{
    vec2 pos = position * 64.0f;
    gl_Position = mvp * vec4(pos, 0.0, 1.0);
}
