#version 330 core

layout (location = 0) in vec2 vertex_position;
layout (location = 1) in int vertex_uv;

uniform mat4 mvp;
out vec2 texcoord;

void main()
{
    vec2 temp;

    texcoord.x = vertex_position.x / 32 / 4 + float( vertex_uv / 4) / 4.0f;
    texcoord.y = vertex_position.y / 32 / 4 + float( vertex_uv % 4) / 4.0f;

    temp.x = vertex_position.x + 32 * (gl_InstanceID / 18);
    temp.y = vertex_position.y + 32 * (gl_InstanceID % 18);

    gl_Position = mvp * vec4(temp, 0.0, 1.0);
};
