#version 330 core

#define NOMIRROR  (0)       			/// 00 00
#define	MIRRORX   (1 << 2)				/// 00 10
#define MIRRORY   (1 << 3)				/// 00 01
#define MIRRORXY  (MIRRORX | MIRRORY)	/// 00 11
#define ROTATE0   (0)					/// 00 00
#define ROTATE90  (1)					/// 10 00
#define ROTATE180 (2)					/// 01 00
#define ROTATE270 (3)					/// 11 00

#define ROTATION  (ROTATE270)
#define MIRROR 	  (MIRRORXY)

#define TEXTURE_LAYER_SHIFT 6
#define TEXTURE_1 (63)
#define TEXTURE_2 (4032)

uniform vec2 wh;
uniform int texture;
uniform int transform;

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

out TOFRAG_texcoord {
	vec2 uv;
	float texture_1;
	float texture_2;
} tofrag_texcoord;

#define d (64)
float get_actual_layer(int layer)
{
	return max(0, min(d - 1, floor(float(layer) + 0.5)));
}

void main()
{
	vec4 positions[4];
	positions[0] = vec4( -1.0, -1.0,  0.0,  0.0 );
	positions[1] = vec4(  1.0, -1.0,  0.0,  0.0 );
	positions[2] = vec4( -1.0,  1.0,  0.0,  0.0 );
	positions[3] = vec4(  1.0,  1.0,  0.0,  0.0 );

	tofrag_texcoord.texture_1 = float(get_actual_layer(texture & TEXTURE_1));
	tofrag_texcoord.texture_2 = float(get_actual_layer((texture & TEXTURE_2) >> TEXTURE_LAYER_SHIFT));
	// tofrag_texcoord.texture_2 = float((texture & TEXTURE_2) >> TEXTURE_1);
	// tofrag_texcoord.texture_2 = float(get_actual_layer(8));
	// float z = float(texture);

	vec2 texcoords[4];
	texcoords[0] = vec2(0.0, 1.0);
	texcoords[1] = vec2(1.0, 1.0);
	texcoords[2] = vec2(0.0, 0.0);
	texcoords[3] = vec2(1.0, 0.0);

	switch (transform & ROTATION) {
		case 0: break;
		case 1: {
			vec2 tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = texcoords[3];
			texcoords[3] = texcoords[2];
			texcoords[2] = tmp;
		} break;
		case 2: {
			vec2 tmp;
			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case 3: {
			vec2 tmp = texcoords[0];
			texcoords[0] = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = texcoords[1];
			texcoords[1] = tmp;
		} break;
	}

	/// Нужно: переписать, возможны ошибки и лишний код
	switch (transform & MIRROR) {
		case 0: break;
		case MIRRORX: {
			vec2 tmp;
			tmp = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case MIRRORY: {
			vec2 tmp;
			tmp = texcoords[2];
			texcoords[2] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[0];
			texcoords[0] = texcoords[1];
			texcoords[1] = tmp;
		} break;
		case MIRRORXY: {
			vec2 tmp;
			tmp = texcoords[0];
			texcoords[0] = texcoords[3];
			texcoords[3] = tmp;

			tmp = texcoords[2];
			texcoords[2] = texcoords[1];
			texcoords[1] = tmp;
		} break;
	}

	vec4 pos = gl_in[0].gl_Position;
	// pos.x = pos.x * 64.0f;
	// pos.y = pos.y * 64.0f;
	float w = 64.0f / wh.x;
	float h = 64.0f / wh.y;

	gl_Position = pos + vec4(0, 0, 0, 0);
	// gl_Position = pos + vec4(-1, -1, 0, 0);
    tofrag_texcoord.uv = texcoords[0];
    EmitVertex();

    gl_Position = pos + vec4( 2*w, 0, 0, 0);
    // gl_Position = pos + vec4( 1, -1.0, 0.0, 0.0);
    tofrag_texcoord.uv = texcoords[1];
    EmitVertex();

    gl_Position = pos + vec4(0, 2*h, 0, 0);
    // gl_Position = pos + vec4(-1.0, 1.0, 0.0, 0.0);
    tofrag_texcoord.uv = texcoords[2];
    EmitVertex();

    gl_Position = pos + vec4(2*w, 2*h, 0.0, 0.0);
    // gl_Position = pos + vec4(1.0, 1.0, 0.0, 0.0);
    tofrag_texcoord.uv = texcoords[3];
    EmitVertex();

	EndPrimitive();
}
