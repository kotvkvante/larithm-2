#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 36) out;

uniform vec2 rect;
uniform int corner;
uniform int border;
uniform int background;
uniform int width;
uniform int height;

out vec3 tofrag_texcoord;
// #define ATLAS_SIZE 8.0
#define ATLAS_SIZE 1.0
#define TEXTURE_ID 35
#define TMSI 8
#define SIZE 128.0f

#define d (64)
float get_actual_layer(int layer)
{
	return max(0, min(d - 1, floor(layer + 0.5)));
}

void box(vec4 pos, vec2 rect, vec2 offset, int texture, bvec2 flip)
{
	pos.x += offset.x;
	pos.y += offset.y;
	// vec2 toffset = vec2(float(texture % TMSI)/ATLAS_SIZE, float(texture / TMSI)/ATLAS_SIZE);

	float z = get_actual_layer(texture);

	float x1 = 0.0f;
	float x2 = 1.0f;
	float y1 = 0.0f;
	float y2 = 1.0f;

	if (flip.x) {
		float tmp = x1; x1 = x2; x2 = tmp;
	}
	if (flip.y) {
		float tmp = y1; y1 = y2; y2 = tmp;
	}

	gl_Position = pos + vec4(-rect.x, -rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x2, y2, z);
    EmitVertex();

    gl_Position = pos + vec4( rect.x, -rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x1, y2, z);
    EmitVertex();

    gl_Position = pos + vec4(-rect.x, rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x2, y1, z);
    EmitVertex();

    gl_Position = pos + vec4(rect.x, rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x1, y1, z);
    EmitVertex();

    EndPrimitive();
}

void box_rotated(vec4 pos, vec2 rect, vec2 offset, int texture, bvec2 flip)
{
	pos.x += offset.x;
	pos.y += offset.y;

	float z = get_actual_layer(texture);

	float x1 = 0.0f;
	float x2 = 1.0f;
	float y1 = 0.0f;
	float y2 = 1.0f;

	if (flip.x) {
		float tmp = x1; x1 = x2; x2 = tmp;
	}
	if (flip.y) {
		float tmp = y1; y1 = y2; y2 = tmp;
	}

	gl_Position = pos + vec4(-rect.x, -rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x1, y2, z);
    EmitVertex();

    gl_Position = pos + vec4( rect.x, -rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x1, y1, z);
    EmitVertex();

    gl_Position = pos + vec4(-rect.x, rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x2, y2, z);
    EmitVertex();

    gl_Position = pos + vec4(rect.x, rect.y, 0.0, 0.0);
    tofrag_texcoord = vec3(x2, y1, z);
    EmitVertex();

    EndPrimitive();
}


void main()
{
	vec4 base = gl_in[0].gl_Position;
	// vec4 base = vec4(-0.5, -0.5, 0.0, gl_in[0].gl_Position.w);

	vec2 background_rect = vec2(rect.x * width, rect.y * height);
	box(base, background_rect, vec2(0.0, 0.0), background, bvec2(0, 0));

	vec2 corner_rect = vec2(rect.x, rect.y);
	box(base, rect,
		vec2(rect.x*width, rect.y*height), corner, bvec2(0, 0));
	box(base, rect,
		vec2(-rect.x*width, rect.y*height), corner, bvec2(1, 0));
	box(base, rect,
		vec2(-rect.x*width, -rect.y*height), corner, bvec2(1, 1));
	box(base, rect,
		vec2(rect.x*width, -rect.y*height), corner, bvec2(0, 1));

	vec2 border_rect = vec2(rect.x * (width), rect.y);
	box(base, border_rect, vec2(0.0, rect.y*height), border, bvec2(0, 0));
	// box(base, border_rect, vec2(0.0, -rect.y*height), border, bvec2(0, 0));
	box(base, border_rect, vec2(0.0,-rect.y*height), border, bvec2(0, 1));

	border_rect = vec2(rect.x, rect.y * (height));
	box_rotated(base, border_rect,
		vec2(rect.x * width, 0.0), border, bvec2(0, 0));

	box_rotated(base, border_rect,
		vec2(-rect.x * width, 0.0), border, bvec2(0, 1));
	// box_rotated(base, border_rect,
	// 	vec2(-rect.x * width, 0.0), border, bvec2(0, 0));
}
