#version 330 core

in vec2 texture_coord;

uniform sampler2D texture_map;
uniform vec2 u_dimensions;
uniform vec2 u_border;

out vec4 output_color;

float map(float value, float originalMin, float originalMax, float newMin, float newMax) {
    return (value - originalMin) / (originalMax - originalMin) * (newMax - newMin) + newMin;
}

// Helper function, because WET code is bad code
// Takes in the coordinate on the current axis and the borders
float processAxis(float coord, float textureBorder, float windowBorder) {
    if (coord < windowBorder)
        return map(coord, 0, windowBorder, 0, textureBorder) ;
    if (coord < 1 - windowBorder)
        return map(coord,  windowBorder, 1 - windowBorder, textureBorder, 1 - textureBorder);
    return map(coord, 1 - windowBorder, 1, 1 - textureBorder, 1);
}

void main(void) {
    vec2 newUV = vec2(
        processAxis(texture_coord.x, u_border.x, u_dimensions.x),
        processAxis(texture_coord.y, u_border.y, u_dimensions.y)
    );
    newUV.x+=1.0f; // which image to use: 0, 1, or 2
    newUV.y+=2.0f; // which image to use: 0, 1, or 2
    newUV.x*=32.0/256.0f; // clip.w / texture.w
    newUV.y*=32.0/256.0f; // clip.h / texture.h
    output_color = texture(texture_map, newUV);
    // output_color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
