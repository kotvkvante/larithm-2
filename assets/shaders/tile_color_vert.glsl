#version 330 core

uniform float tile_size;
uniform vec2  tile_offset;
uniform ivec3 tile_color;
uniform mat4  mvp;

flat out ivec3 color;

void main()
{
    vec2 vertex_position;
    color = tile_color;

    vertex_position.x = (gl_VertexID + 1) / 2 % 2;
    vertex_position.y = gl_VertexID / 2;

    vertex_position.x = (vertex_position.x + tile_offset.x) * tile_size;
    vertex_position.y = (vertex_position.y + tile_offset.y) * tile_size;

    gl_Position = mvp * vec4(vertex_position, 0.0, 1.0);
};
