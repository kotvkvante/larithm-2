#version 330 core

layout (location = 0) in vec2 vertex_position;
layout (location = 1) in vec3 vertex_color;
uniform mat4 mvp;

out vec3 color;
in
void main()
{
	color = vertex_color;
    gl_Position = mvp * vec4(vertex_position, 0.0, 1.0);
}
