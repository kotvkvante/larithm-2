#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

out vec3 color;
void box(vec4 pos)
{
    gl_Position = pos + vec4(-0.1, -0.1, 0.0, 0.0);
	color = vec3(1.0, 0.0, 0.0)
    EmitVertex();

    gl_Position = pos + vec4( 0.1, -0.1, 0.0, 0.0);
    color = vec3(1.0, 0.0, 0.0)
    EmitVertex();

    gl_Position = pos + vec4(-0.1, 0.1, 0.0, 0.0);
    color = vec3(0.0, 1.0, 0.0)

	EmitVertex();
    gl_Position = pos + vec4(0.1, 0.1, 0.0, 0.0);
    color = vec3(0.0, 1.0, 0.0)
    EmitVertex();

    EndPrimitive();
}

void main() {
    box(gl_in[0].gl_Position);
}
