#version 330 core

layout (location = 0) in vec2 vertex_position;
layout (location = 1) in vec2 vertex_offset;
layout (location = 2) in int vertex_uv;

uniform mat4 mvp;
out vec2 texcoord;

#define TEXTURE_MAP_SIZE 4
#define TMSI TEXTURE_MAP_SIZE
#define TMSF float(TEXTURE_MAP_SIZE)

#define TILE_SIZE 32.0f

void main()
{
    vec2 temp;

    texcoord.x = (vertex_position.x + float( vertex_uv / TMSI)) / TMSF;
    texcoord.y = (vertex_position.y + float( vertex_uv % TMSI)) / TMSF;

    temp.x = (vertex_position.x + vertex_offset.x) * TILE_SIZE;
    temp.y = (vertex_position.y + vertex_offset.y) * TILE_SIZE;

    gl_Position = mvp * vec4(temp, 0.0, 1.0);
};
