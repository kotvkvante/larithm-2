#version 330 core

layout (location = 0) in vec2 vertex_position;
layout (location = 1) in vec2 vertex_uv;

uniform float tile_size;
uniform vec2  tile_offset;
uniform int tile_texture;
uniform mat4 mvp;

out vec2 texture_coord;

#define TEXTURE_MAP_SIZE 8
#define TMSI TEXTURE_MAP_SIZE
#define TMSF float(TEXTURE_MAP_SIZE)

void main()
{
    texture_coord = vertex_uv;
    gl_Position = vec4(vertex_position, 0.0, 1.0);
}
