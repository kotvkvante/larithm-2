#version 330 core

layout (location = 0) in vec2 tile_offset;
layout (location = 1) in int tile_texture;

uniform mat4 mvp;
out vec2 texcoord;

#define TEXTURE_MAP_SIZE 4
#define TMSI TEXTURE_MAP_SIZE
#define TMSF float(TEXTURE_MAP_SIZE)

#define TILE_SIZE 32.0f

void main()
{
    vec2 vertex_position;

    // ((i + 1) >> 1) % 2
    vertex_position.x = (gl_VertexID + 1) / 2 % 2;
    vertex_position.y = gl_VertexID / 2;

    texcoord.x = (vertex_position.x + float( tile_texture % TMSI)) / TMSF;
    texcoord.y = (vertex_position.y + float( tile_texture / TMSI)) / TMSF;

    vertex_position.x = (vertex_position.x + tile_offset.x) * TILE_SIZE;
    vertex_position.y = (vertex_position.y + tile_offset.y) * TILE_SIZE;

    gl_Position = mvp * vec4(vertex_position, 0.0, 1.0);
};
