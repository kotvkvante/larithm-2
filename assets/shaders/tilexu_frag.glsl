#version 330 core

#define TXU_NOSTYLE    (0)        /// 0000
#define TXU_GRAYSCALE  (1 << 0)   /// 1000
#define TXU_APPLYRED   (1 << 1)	  /// 0100
#define TXU_APPLYCOLOR (1 << 2)	  /// 0010 0000 [0000 0000] [0000 0000] [0000 0000]
// #define TXU_APPLYCOLOR (1 << 2)	 /// 0010

#define COLORMASK (255) /// 1111 1111

uniform sampler2DArray atlas;
uniform int style;
out vec4 color;

in TOFRAG_texcoord {
	vec2 uv;
	float texture_1;
	float texture_2;
} tofrag_texcoord;

void main()
{
	vec4 scolor, scolor_1, scolor_2;
    vec3 texcoord = vec3(tofrag_texcoord.uv, 0);

    texcoord.z = tofrag_texcoord.texture_1;
    scolor_1 = texture(atlas, texcoord);

	scolor = scolor_1;
	if (tofrag_texcoord.texture_2 != 0) {
        texcoord.z = tofrag_texcoord.texture_2;
        scolor_2 = texture(atlas, texcoord);
        scolor = scolor_2 * scolor_2.a + (1 - scolor_2.a) * scolor_1;
    }

	if (style == TXU_NOSTYLE) {
		color = scolor;
		return;
    }

	if ((style & TXU_GRAYSCALE) == TXU_GRAYSCALE) {
        float grey = (scolor.r + scolor.g + scolor.b) / 3.0;
        scolor = vec4(grey, grey, grey, scolor.a);

        /// Второй вариант алгоритма.
        // float grey = 0.21 * scolor.r + 0.71 * scolor.g + 0.07 * scolor.b;
        // float u_colorFactor = 0.0;
        // scolor = vec4(
        //     scolor.r * u_colorFactor + grey * (1.0 - u_colorFactor),
        //     scolor.g * u_colorFactor + grey * (1.0 - u_colorFactor),
        //     scolor.b * u_colorFactor + grey * (1.0 - u_colorFactor), scolor.a);
    }
    if ((style & TXU_APPLYRED) == TXU_APPLYRED) {
        scolor.r = 1.0f;
    }

	if ((style & TXU_APPLYCOLOR) == TXU_APPLYCOLOR) {
		// 0010 0000 0000 0000
		// if (scolor.a < 0.4f)
		// {
	        // vec4 color = vec4(1.0f, 0, 0, scolor.a);
			vec4 color = vec4(
				float((style >>  8) & COLORMASK) / 256.0f,
				float((style >> 16) & COLORMASK) / 256.0f,
				float((style >> 24) & COLORMASK) / 256.0f,
				scolor.a);
			scolor = color;
		// }
	}

    color = scolor;
}
