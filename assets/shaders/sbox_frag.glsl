#version 330 core

uniform sampler2D atlas;

in vec2 sh_texcoord;

out vec4 color;

void main()
{
   color = texture(atlas, sh_texcoord);
   // color.r = 1.0f;
}
