#version 330 core

in vec3 tofrag_texcoord;

uniform sampler2DArray atlas;

out vec4 color;

void main()
{
   color = texture(atlas, tofrag_texcoord);
   // color.b = 1.0f;
}
