#version 330 core

flat in ivec3 color;
out ivec3 output_color;

void main()
{
    output_color = ivec3(color);
};
