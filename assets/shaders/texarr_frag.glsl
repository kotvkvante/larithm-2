#version 330 core

in vec3 sh_texcoord;
uniform sampler2DArray atlas;

out vec4 color;

// #define layer (0)

void main()
{
   // vec3 tc = vec3(sh_texcoord, actual_layer);
   color = texture(atlas, sh_texcoord);
   // color.r = 1.0f;
}
