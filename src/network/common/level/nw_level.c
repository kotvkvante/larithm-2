#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <curl/curl.h>
#include "frozen.h"

#include "common/level/levels_list.h"
#include "network/common/level/nw_level.h"

#define NW_API_RAW_DATA_LINK "http://127.0.0.1:8001/levels/%d/raw_data"
#define LVL_LINK "http://127.0.0.1:8001/levels/%d"

struct MemoryStruct {
  char *memory;
  size_t size;
};

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
static void nw_api_lvl_download_raw_data(int id, struct MemoryStruct* ms);

void nw_api_lvl_download(lvlem_t* lvlem)
{
	struct MemoryStruct ms = {
		  .memory = malloc(1),  /* grown as needed by the realloc above */
		  .size = 0,            /* no data at this point */
	};

	nw_api_lvl_download_raw_data(lvlem->id, &ms);

    if (lvlem->fname == NULL) {
        char buf[256];
        snprintf(buf, 256, "./assets/lvls/community/%d.%s", lvlem->id, lvlem->format);
        lvlem->fname = strdup(buf);
    }


	FILE* fp = fopen(lvlem->fname, "wb");
    if (fp == NULL) {
        printf("[E] `%s`: fp == NULL.\n", __func__);
        exit(-1);
    }


    fwrite(ms.memory, sizeof(char), ms.size, fp);
    fclose(fp);

	free(ms.memory);
}

static void nw_api_lvl_download_raw_data(int id, struct MemoryStruct* ms)
{
	CURL *curl_handle;
	CURLcode res;

	curl_global_init(CURL_GLOBAL_ALL);
	curl_handle = curl_easy_init();

	/* specify URL to get */
	char link[256];
	if (sprintf(link, NW_API_RAW_DATA_LINK, id) < 0) {
		printf("sprintf(link, NW_API_RAW_DATA_LINK, id) != 1");
		exit(-1);
	}
	curl_easy_setopt(curl_handle, CURLOPT_URL, link);

	/* send all data to this function  */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

	/* we pass our 'chunk' struct to the callback function */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)ms);

	/* some servers do not like requests that are made without a user-agent
	field, so we provide one */
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	res = curl_easy_perform(curl_handle);
	if(res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
	}
	curl_easy_cleanup(curl_handle);
	curl_global_cleanup();
}

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  char *ptr = realloc(mem->memory, mem->size + realsize + 1);
  if(!ptr) {
    /* out of memory! */
    printf("[E] Not enough memory (realloc returned NULL)\n");
    return 0;
  }

  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}


// static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
// {
// 	size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
// 	return written;
// }
//
// void nw_lvl_save_to_file(int id);
// {
// 	CURL *curl_handle;
// 	FILE *pagefile;
// 	curl_global_init(CURL_GLOBAL_ALL);
// 	curl_handle = curl_easy_init();
// 	char link[256];
// 	if (sprintf(link, LVL_LINK, id) != 1) { printf("sprintf(link, LVL_LINK, id) != 1"); exit(-1); }
//
// 	curl_easy_setopt(curl_handle, CURLOPT_URL, link);
// 	curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
// 	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
// 	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
//
// 	pagefile = fopen(filename, "wb");
// 	if(pagefile) {
// 		curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
// 		curl_easy_perform(curl_handle);
// 		fclose(pagefile);
// 	}
// 	curl_easy_cleanup(curl_handle);
// 	curl_global_cleanup();
// }

// char* nw_lvl_retrive_raw_data(int id, int* size)
// {
//   CURL *curl_handle;
//   CURLcode res;
//   struct MemoryStruct chunk;
//
//   chunk.memory = malloc(1);  /* grown as needed by the realloc above */
//   chunk.size = 0;    /* no data at this point */
//
//   curl_global_init(CURL_GLOBAL_ALL);
//
//   /* init the curl session */
//   curl_handle = curl_easy_init();
//
//   /* specify URL to get */
// 	char link[256];
// 	if (sprintf(link, LVL_RAW_DATA_LINK, id) < 0) { printf("sprintf(link, LVL_RAW_DATA_LINK, id) != 1"); exit(-1); }
// 	curl_easy_setopt(curl_handle, CURLOPT_URL, link);
//
//   /* send all data to this function  */
//   curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
//
//   /* we pass our 'chunk' struct to the callback function */
//   curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
//
//   /* some servers do not like requests that are made without a user-agent
//      field, so we provide one */
//   curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
//
//   /* get it! */
//   res = curl_easy_perform(curl_handle);
//
//   /* check for errors */
//   if(res != CURLE_OK) {
//     fprintf(stderr, "curl_easy_perform() failed: %s\n",
//             curl_easy_strerror(res));
//   }
//   else {
//     /*
//      * Now, our chunk.memory points to a memory block that is chunk.size
//      * bytes big and contains the remote file.
//      *
//      * Do something nice with it!
//      */
//
// 	}
//
//     printf("%lu bytes retrieved\n", (unsigned long)chunk.size);
//     printf("`%s`\n", chunk.memory);
//
// 	/* cleanup curl stuff */
// 	curl_easy_cleanup(curl_handle);
//
// 	// free(chunk.memory);
// 	*size = chunk.size;
// 	/* we are done with libcurl, so clean it up */
// 	curl_global_cleanup();
// 	return chunk.memory;
// }
//
// void nw_lvl_retrive_header(int id)
// {
//   CURL *curl_handle;
//   CURLcode res;
//   struct MemoryStruct chunk;
//
//   chunk.memory = malloc(1);  /* grown as needed by the realloc above */
//   chunk.size = 0;    /* no data at this point */
//
//   curl_global_init(CURL_GLOBAL_ALL);
//
//   /* init the curl session */
//   curl_handle = curl_easy_init();
//
//   /* specify URL to get */
// 	char link[256];
// 	if (sprintf(link, LVL_LINK, id) < 0) { printf("sprintf(link, LVL_LINK, id) != 1"); exit(-1); }
// 	curl_easy_setopt(curl_handle, CURLOPT_URL, link);
//
//   /* send all data to this function  */
//   curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
//
//   /* we pass our 'chunk' struct to the callback function */
//   curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
//
//   /* some servers do not like requests that are made without a user-agent
//      field, so we provide one */
//   curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
//
//   /* get it! */
//   res = curl_easy_perform(curl_handle);
//
//   /* check for errors */
//   if(res != CURLE_OK) {
//     fprintf(stderr, "curl_easy_perform() failed: %s\n",
//             curl_easy_strerror(res));
//   }
//   else {
//     /*
//      * Now, our chunk.memory points to a memory block that is chunk.size
//      * bytes big and contains the remote file.
//      *
//      * Do something nice with it!
//      */
// 	char* author; char* format; int id;
// 	}
//
//     printf("%lu bytes retrieved\n", (unsigned long)chunk.size);
//
// 	/* cleanup curl stuff */
// 	curl_easy_cleanup(curl_handle);
//
// 	free(chunk.memory);
//
// 	/* we are done with libcurl, so clean it up */
// 	curl_global_cleanup();
//
// }


// void nw_lvl_save_to_file(int id)
// {
//   CURL *curl_handle;
//   CURLcode res;
//   return 0;
// }
