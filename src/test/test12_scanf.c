#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {
	int ind;
	const char delims[] = " ,";
	char str[] = " \n ";
	// char str[] = "5";
	char* p;
	p = strtok(str, delims);
	int res = sscanf(p, "%d", &ind);
	if (!res) printf("HEY!\n");
	printf("\t res=`%d` -> `%s`", res, p);
	printf("-> \t %d \n", ind);

	/* code */
	return 0;
}
