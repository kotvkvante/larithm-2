#include <stdio.h>
#include <curl/curl.h>

// size_t my_write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
//     printf("Received %ld bytes.\n", nmemb);
//     return nmemb;
// }

int main(void)
{

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();
	if(curl) {
		// curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_write_callback);

		curl_easy_setopt(curl, CURLOPT_URL, "http://127.0.0.1:8000/get_lvlist");
		/* example.com is redirected, so we tell libcurl to follow redirection */
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		/* Perform the request, res gets the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK)
		  fprintf(stderr, "curl_easy_perform() failed: %s\n",
	          curl_easy_strerror(res));

		/* always cleanup */
		curl_easy_cleanup(curl);
	}
	return 0;
}
