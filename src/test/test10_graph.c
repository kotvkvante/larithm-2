#include <stdlib.h>
#include <stdio.h>

#include "common/map/graph.h"

int main(int argc, char const *argv[]) {
	graph_t g;
	char* graph = "8 3 3 2 4 5 4 2";

    graph_read_from_str(&g, false, graph);
	graph_print(&g);
	return 0;
}
