#include <stdlib.h>
#include <stdio.h>

#include "common/map/graph.h"
#include "common/map/logical_map.h"
#include "common/elems/types.h"
#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

// elem_output_t elem0_out = {
// 	.type = ELEM_TYPE_OUTPUT,
// 	.x = 3,
// 	.y = 1
// };
// elem_output_t elem1_out = {
// 	.type = ELEM_TYPE_OUTPUT,
// 	.x = 1,
// 	.y = 0
// };
// elem_output_t elem2_out = {
// 	.type = ELEM_TYPE_OUTPUT,
// 	.x = 1,
// 	.y = 3
// };
//
// elem_logical_t elem3_or = {
// 	.type = ELEM_TYPE_OUTPUT,
// 	.x = 1,
// 	.y = 3
// };
// elem_input_t elem4_input;
// elem_logical_t elem5_and;
// elem_input_t elem6_input;
// elem_input_t elem7_input;
// elem_logical_t elem8_or;
// elem_input_t elem_9_input;
//
// elem_base_t* elems[10] = {
// 	(elem_base_t*)&elem0_out,
// 	(elem_base_t*)&elem1_out,
// 	(elem_base_t*)&elem2_out,
// 	(elem_base_t*)&elem3_or,
// 	(elem_base_t*)&elem4_input,
// 	(elem_base_t*)&elem5_and,
// 	(elem_base_t*)&elem6_input,
// 	(elem_base_t*)&elem7_input,
// 	(elem_base_t*)&elem8_or,
// 	(elem_base_t*)&elem_9_input,
// };
//
// int main(int argc, char const *argv[]) {
// 	graph_t g;
//
// 	logical_map_t* lmap = logical_map_new();
// 	lmap_init(lmap, 4, 10);
// 	for (size_t i = 0; i < 10; i++) {
// 		logical_map_insert_elem(lmap, elems[i]);
// 	}
//
// 	char* graph = "10 9 0 3 1 3 2 8 3 4 3 5 5 6 5 7 8 7 8 9";
//
//     graph_read_from_str(&g, true, graph);
//
// 	for (size_t i = 0; i < g.nvertices; i++) {
// 		edgenode_t* n = g.edges[i];
// 		while (n != NULL)
// 		{
// 			printf("[%ld] y = %d\n", i, n->y);
// 			n = n->next;
// 		}
// 	}
//
// 	graph_print(&g);
// 	return 0;
// }

typedef struct link_t {
	htilex_t ht;

} link_t;

elem_output_t elem0_out = {
	.type = ELEM_TYPE_OUTPUT,
	.x = 0,
	.y = 0
};
elem_input_t elem1_input = {
	.type = ELEM_TYPE_INPUT,
	.x = 1,
	.y = 0
};

elem_base_t* elems[2] = {
	(elem_base_t*)&elem0_out,
	(elem_base_t*)&elem1_input,
};

int calculate_direction(int px, int py, int cx, int cy)
{
	int dx = cx - px;
	int dy = cy - py;
	// int vlen = sqrt(dx ** 2 + dy ** 2);
	// int ndx = dx / vlen;
	// int ndy = dy / vlen;
	if (dy == 0) {
		if (dx > 0) {
			return 0;
		} else {
			return 1;
		}

	}
	if (dx == 0) {
		if (dy > 0) {
			return 1;
		} else {
			return 3;
		}
	}
	float tan = dy / dx;
	if (dx > 0) {
		if (abs(tan) < 1) {
			return 0;
		} else if (tan > 0) {
			return 1;
		} else {
			return 2;
		}
	}
	if (dx < 0) {
		if (abs(tan) < 1) {
			return 2;
		} else if (tan > 0) {
			return 3;
		} else {
			return 1;
		}
	}
	return 0;
}

void gen_link(link_t* link, elem_base_t* p, elem_base_t* c)
{
	// int direction_x = p
	//
	// htilex push head point;
	//
	// cycle push links
	//
	// htilex push tail point

}

int main(int argc, char const *argv[]) {
	logical_map_t* lmap = logical_map_new();
	lmap_init(lmap, 2, 2);
	graph_t* g = &lmap->g;

	for (size_t i = 0; i < 2; i++) {
		logical_map_insert_elem(lmap, elems[i]);
	}

	char* graph = "2 1 0 1";

    graph_read_from_str(g, true, graph);

	for (size_t i = 0; i < g->nvertices; i++) {
		edgenode_t* n = g->edges[i];
		while (n != NULL)
		{
			printf("[%ld] y = %d\n", i, n->y);
			n = n->next;
		}
	}
	graph_print(g);

	// int d1 = calculate_direction(0, 0, 1, 0); // 0
	// int d2 = calculate_direction(0, 0, 0, 1); // 1
	// int d3 = calculate_direction(0, 0, -1, 0); // 2
	// int d4 = calculate_direction(0, 0, 0, -1); // 3
	// int d5 = calculate_direction(0, 0, 2, 1); // 0
	// int d6 = calculate_direction(0, 0, -2, 1); // 2
	// int d7 = calculate_direction(0, 0, -2, -1); // 2
	// printf("%d, %d\n", d1, 0);
	// printf("%d, %d\n", d2, 1);
	// printf("%d, %d\n", d3, 2);
	// printf("%d, %d\n", d4, 3);
	// printf("%d, %d\n", d5, 0);
	// printf("%d, %d\n", d6, 2);
	// printf("%d, %d\n", d7, 2);








	return 0;
}
