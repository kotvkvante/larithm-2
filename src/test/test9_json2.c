#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "frozen.h"

#include "utils/utils.h"
#include "common/game/game.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"
#include "common/game/gsff.h"
#include "common/player/sequence.h"

char* str = "{ \"header\": {\"fversion\": 1},  \"body\": 2, \"trash\": 4 }";

void f1()
{
	int fversion, body;
	json_scanf(str, strlen(str),
		"{"
		"header: {fversion: %d},"
		"body: %d"
		"}",
		&fversion,
		&body);
	printf("%d %d\n", fversion, body);
}

void f2()
{
	struct json_token fversion = {0};
	struct json_token trash = {0};
	int body = 0;
	int n = json_scanf(str, strlen(str),
		"{"
		"header: %T,"
		"body: %d,"
		"trash: %T"
		"}",
		&fversion,
		&body,
		&trash);

	printf("> %d\n", n);
	printf("%d\n", body);
	printf("%d `%.*s`\n", fversion.type, fversion.len, fversion.ptr);
	printf("%d `%.*s`\n", trash.type, trash.len, trash.ptr);
	}

int main(int argc, char const *argv[])
{
	// f1();
	// f2();

	game_t* game = game_new();
	char* str = file_read("assets/gamesaves/gs4.la2j");

	gsff_init_from_str_JSON(game, str);
	map_t* map = game_get_map(game);
	logical_map_t* lmap = game_get_lmap(game);

	game_info(game);
	map_info(map);
	lmap_info(lmap);

	sqn_print(&game->sqns_list[0]);
	sqn_print(&game->sqns_list[1]);

	return 0;
}
