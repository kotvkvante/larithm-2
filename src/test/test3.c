#include <stdio.h>
#include <dirent.h>

int main()
{
    DIR *folder;
    struct dirent *entry;
    int files = 0;

    folder = opendir(".");
    if(folder == NULL)
        perror("Unable to read directory");
		{
        return(1);
    }

    while( (entry=readdir(folder)) )
    {
        files++;
        printf("File %3d: %s\n", files, entry->d_name );
    }

    closedir(folder);

    return(0);
}

// #include<dirent.h>
//
// typedef struct files {
// 		long long mtime;
// 		string file_path;
// 		bool operator<(const files &rhs)const {
// 		    return mtime < rhs.mtime;
// 	}
// } files;
//
// using namespace std;
// vector<struct files> files_list;
//
//     void build_files_list(const char *path)  {
//     struct dirent *entry;
//     DIR *dp;
//     if (dp = opendir(path)) {
//         struct _stat64 buf;
//         while ((entry = readdir(dp))){
//             std::string p(path);
//             p += "\\";
//             p += entry->d_name;
//             if (!_stat64(p.c_str(), &buf)){
//                 if (S_ISREG(buf.st_mode)){
//                     files new_file;
//                     new_file.mtime = buf.st_mtime;
//                     new_file.file_path = entry->d_name;
//                     files.push_back(new_file);
//                 }
//
//             if (S_ISDIR(buf.st_mode) &&
//                 // the following is to ensure we do not dive into directories "." and ".."
//                 strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")){
//                 //do nothing
//             }
//         }
//     }
//     closedir(dp);
//    }
//    else{
//        printf_s("\n\tERROR in openning dir %s\n\n\tPlease enter a correct
//         directory", path);
//    }
// }
//
// void main(){
//     char *dir_path="dir_path";
//     build_files_list(dir_path);
//     sort(files_list.begin(), files_list.end());
//     //the rest of things to do
// }
