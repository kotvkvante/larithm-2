#include <stdlib.h>
#include <stdio.h>

#include "common/game/game.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"
#include "common/player/sequence.h"

int main(int argc, char const *argv[]) {
	game_t* game = game_new();

	game_init_from_file(game, "assets/gamesaves/gs1.la2c");
	map_t* map = game_get_map(game);
	logical_map_t* lmap = game_get_lmap(game);

	game_info(game);
	map_info(map);
	lmap_info(lmap);

	// gsff_init_sqns_compr(game, )

	sqn_print(&game->sqns_list[0]);
	sqn_print(&game->sqns_list[1]);

	return 0;
}
