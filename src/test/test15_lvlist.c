#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common/level/levels_list.h"
#include "common/level/llff_json.h"
#include "common/level/llff_plain.h"


void f1()
{
	lvlist_t* lvlist = lvlist_new();
	lvlist_init_from_file(lvlist, "./assets/lvls/default.la2lj");
	lvlist_print(lvlist);

	lvlist_free(lvlist);
}



void f2()
{
	lvlist_t* lvlist = lvlist_new();
	// lvlist_init_from_str_PLAIN(lvlist, "./assets/lvls/test.txt");

	// char* templ = "1\n./assets/lvls/test/1.la2c:Firts lvl";
	char* templ = "2\n./assets/lvls/test/1.la2c:Firts lvl\n./assets/lvls/test/2.la2c:Second\n";
	char* str = strdup(templ);
	lvlist_init_from_str_PLAIN(lvlist, str);
	lvlist_print(lvlist);

	int a = 0;
	for (size_t i = 0; i < lvlist->lvlems_count; i++) {
		a += strlen(lvlist->lvlems_list[i].fname);
		a += strlen(lvlist->lvlems_list[i].desc);
	}
	printf("2) i = %d\n", a);

	lvlist_free(lvlist);

	free(str);
}

void f2_1()
{
	lvlist_t* lvlist = lvlist_new();
	lvlist_init_from_file(lvlist, "./assets/lvls/test1.txt");
	lvlist_print(lvlist);

	lvlist_free(lvlist);
}

void f3()
{
	lvlist_t* lvlist = lvlist_new();
	lvlist_init_from_file(lvlist, "./assets/lvls/community.la2lj");
	lvlist_print(lvlist);

	lvlist_free(lvlist);

}

int main(int argc, char const *argv[])
{
	// f1();
	f2();
	// f2_1();
	// f3();
	return 0;
}
