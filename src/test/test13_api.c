#include <stdlib.h>
#include <stdio.h>

#include "common/level/levels_list.h"
#include "network/common/level/nw_level.h"
#include "network/common/level/nw_levels_list.h"

int main(int argc, char const *argv[]) {

	lvlist_t* lvlist = lvlist_new();
	nw_lvlist_save_to_file("assets/lvls/tmp/tmp.la2lj");
	lvlist_init_from_file(lvlist, "assets/lvls/tmp/tmp.la2lj");
    lvlist_print(lvlist);

	lvlist_set_selected(lvlist, 1);
	lvlem_t* lvlem = lvlist_get_selected(lvlist);

    nw_api_lvl_download(lvlem);
	lvlem_append_to_file_JSON(lvlem, "assets/lvls/list_of_levels.la2lj");
    // lvlem_append_to_file(lvlem, "assets/lvls/test.txt");


	return 0;
}
