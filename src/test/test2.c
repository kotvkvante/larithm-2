#include <stdio.h>
#include "common/tile/tilex.h"

int main(int argc, char const *argv[]) {
	printf("sizeof float: %ld\n", sizeof(float));
	printf("sizeof matrix: %ld\n", 16 * sizeof(float));
	tilexf_t tf = MIRRORXY;
	tilex_t tx;
	tilex_init(&tx, 0.0f, 0.0f, 0, tf);

	tilex_print(&tx);

	tilex_mirrorx(&tx);
	tilex_print(&tx);

	tilex_mirrorx(&tx);
	tilex_print(&tx);


	return 0;
}
