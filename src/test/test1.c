#include "common/level/levels_list.h"

int main(int argc, char const *argv[]) {
	lvlist_t ll;
	lvlist_init_from_file(&ll, "./assets/lvls/lvls.txt");
	lvlist_print(&ll);
	return 0;
}
