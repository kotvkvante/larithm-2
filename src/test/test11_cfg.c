#include <stdlib.h>
#include <stdio.h>


#include "utils/utils.h"
#include "common/game/game.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"
#include "common/game/gsff.h"
#include "common/player/sequence.h"


int main(int argc, char const *argv[])
{
	game_t* game = game_new();
	char* str = file_read("assets/gamesaves/gs1.la2c");

	gsff_init_from_str_CFG(game, str);
	map_t* map = game_get_map(game);
	logical_map_t* lmap = game_get_lmap(game);

	game_info(game);
	map_info(map);
	lmap_info(lmap);

	sqn_print(&game->sqns_list[0]);
	sqn_print(&game->sqns_list[1]);

	return 0;
}
