#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/game/gsff.h"

int main(int argc, char const *argv[])
{
	char* string[256];
	char test1[] = "first string in this world!";
	char test2[] = "  3,    0, 2  ,  3  3  , 0, 1, 3";
	char test3[] = "1, 3; 3 , 3; 14, 232 , 123123 ,2 ; ";
	char test4[] = "1, 2; 3 , 4 ";
	// int i1 = atoi(test1);
	// int i2 = atoi(test2);

	// printf("i1 = %d\n", i1);
	// printf("i2 = %d\n", i2);

	// char test4[] = "  1   ,";
	// int i;
	// int b;
	// int n = sscanf(test4, "%d%n", &i, &b);
	// printf("i = %d, %d %d\n", i, b, n);

    // string[i] = strtok(test2, ",");
	// while(string[i] != NULL)
	// {
	// 	printf("string [%d]=%s\n", i, string[i]);
	// 	i++;
	// 	string[i] = strtok(NULL, ",");
	// }

	int i = 0;
	char* p1 = NULL;
	char* p2 = NULL;
	char* s1 = strtok_r(test4, ";", &p1);
	while(s1 != NULL)
	{
		printf("string [%d]=`%s`\n", i, s1);

		// char* s2 = strtok_r(s1 , ",", &p2);
		// char* s3 = strtok_r(NULL , ",", &p2);
		// char* s4 = strtok_r(NULL , ",", &p2);
		// printf("> `%s`\n", s2);
		// printf("> `%s`\n", s3);
		// printf("> `%s`\n", s4);

		char* s2 = strtok_r(s1 , ",", &p2);
		while (s2 != NULL)
		{
			printf("> `%s`\n", s2);
			s2 = strtok_r(NULL, ",", &p2);
		}
		i++;
		s1 = strtok_r(NULL, ";", &p1);

	}

	// char str1[] = "     	\
	// 1"; int d1; size_t du1;
	// char str2[] = "      -1"; int d2; size_t du2;
	// int n1 = sscanf(str1, "%d", &d1);
	// int n2 = sscanf(str1, "%zu", &du1);
	// int n3 = sscanf(str2, "%d", &d2);
	// int n4 = sscanf(str2, "%zu", &du2);
	//
	// printf("%d %zu %d %zu\n", d1, du1, d2, du2);
	// printf("%d %d %d %d\n", n1, n2, n3, n4);

	// int a1;
	// char str1[] = " -24";
	// int r1 = atoi(str1, &a1);
	// printf("%d\n", a1);
	// printf("%d\n", r1);
	// unsigned int a2;
	// char str2[] = " -24";
	// int r2 = atoi(str2, &a2);
	// printf("%u\n", a2);
	// printf("%d\n", r2);

	return 0;
}
