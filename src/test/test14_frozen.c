#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "frozen.h"

// /*
//  * Update given JSON string `s,len` by changing the value at given `json_path`.
//  * The result is saved to `out`. If `json_fmt` == NULL, that deletes the key.
//  * If path is not present, missing keys are added. Array path without an
//  * index pushes a value to the end of an array.
//  * Return 1 if the string was changed, 0 otherwise.
//  *
//  * Example:  s is a JSON string { "a": 1, "b": [ 2 ] }
//  *   json_setf(s, len, out, ".a", "7");     // { "a": 7, "b": [ 2 ] }
//  *   json_setf(s, len, out, ".b", "7");     // { "a": 1, "b": 7 }
//  *   json_setf(s, len, out, ".b[]", "7");   // { "a": 1, "b": [ 2,7 ] }
//  *   json_setf(s, len, out, ".b", NULL);    // { "a": 1 }
//  */
// int json_setf(const char *s, int len, struct json_out *out,
//               const char *json_path, const char *json_fmt, ...);
//
// int json_vsetf(const char *s, int len, struct json_out *out,
//                const char *json_path, const char *json_fmt, va_list ap);

void f1()
{
	char result[256] = { 0 };
	struct json_out out = JSON_OUT_BUF(result, 256);
	char templ[] = "{ \"a\": 1, \"b\": [ 122, 422 ] }";
	printf("@>>> `%s`\n", templ);

	int i = 5;
	json_setf(templ, strlen(templ), &out, ".b[]", "%d", i);
	printf("@>>> `%s`\n", result);
}

void f2()
{
	char result[256] = { 0 };
	struct json_out out = JSON_OUT_BUF(result, 256);
	char templ[] = "{ \"a\": 1, \"b\": [] }";
	printf("@>>> `%s`\n", templ);

	// char* s = "a";
	int s = 2;
	json_setf(templ, strlen(templ), &out, ".b[]", "%d", s);
	printf("@>>> `%s`\n", result);
}

void f3()
{
	char result[256] = { 0 };
	struct json_out out = JSON_OUT_BUF(result, 256);
	char templ[] = "{ \"a\": 1, \"b\": 4 }";
	int a, b, c = 0;

	int i = json_scanf(templ, strlen(templ),
		"{a: %d, b: %d, c: %d}",
		&a, &b, &c);

	printf("a=%d, b=%d, c=%d\n", a, b, c);
	printf("i=%d\n", i);
}

void f4()
{
	
}



int main(int argc, char const *argv[])
{
	// f1();
	// f2();
	f3();

	// {
	// 	char result[256];
	// 	struct json_out out = JSON_OUT_BUF(result, 256);
	// 	char templ[] = "{ \"a\": 1, \"b\": [ \"a\", \"b\" ] }";
	// 	printf("@>>> `%s`\n", templ);
	// 	char* s = "a";
	// 	json_setf(templ, strlen(templ), &out, ".b", "%Q", s);
	// 	printf("@>>> `%s`\n", result);
	// }

	return 0;
}
