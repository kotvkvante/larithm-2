/* Larithm2 */
#include <stdio.h>
#include <stdlib.h>

#include "client/client.h"

client_t client = {
    .debug = false,

    .gfx = NULL,
    .window = NULL,
    .ctx = NULL,

    .sdata = NULL,
    .pdata = NULL
};

int main(int argc, char const *argv[])
{
    clt_parse_args(&client, argc, argv);
    clt_init(&client);

    while(!clt_should_stop(&client))
    {
        // checkGLError();
        clt_handle_events(&client);
        clt_update(&client);
        clt_draw(&client);
    }

    clt_close(&client);

    return 0;
}
