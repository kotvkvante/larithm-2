#include <stdio.h>

#include "nuklear.h"

#include "client/client.h"
#include "utils/utils.h"
#include "common/game/game.h"
#include "common/player/sequence.h"

#define STATE_GAME
#include "client/state/state.h"
#include "client/graphics/gwrap/gw_sqnm.h"

static size_t gui_sqnm_pass_(client_t* clt, action_t* a)
{
	LA2_UNUSED(clt);
	LA2_UNUSED(a);
	return 0;
}


size_t gui_sqnm_start(client_t* clt, action_t* a)
{
    // game_t* game = ((sd_game_t*)clt->sdata)->game;
    // a_plin_t* data = (a_plin_t*)(a->data);
    // data->i = game_get_curplayer_index(game);
    return gui_sqnm_pass_(clt, a);
}


size_t gui_sqnm_plmove(client_t* clt, action_t* a)
{
    static direction_e option = RIGHT;

    a_plmove_t* data = (a_plmove_t*)(a->data);
    struct nk_context* ctx = clt->ctx;

    gw_sqnm_t* gw_sqnm = &((sd_game_t*)clt->sdata)->gw_game.gw_sqnm;

    nk_layout_row_dynamic(ctx, 30, 1);
    if (nk_option_label(ctx, "right", option == RIGHT)) option = RIGHT;
    if (nk_option_label(ctx, "down",  option == DOWN))  option = DOWN;
    if (nk_option_label(ctx, "left",  option == LEFT))  option = LEFT;
    if (nk_option_label(ctx, "up",    option == UP))    option = UP;

    if (option != data->d)
    {
        data->d = option;
        gw_sqnm_update_tx(gw_sqnm, a);
    }

 	return sizeof(a_plmove_t);
}

size_t gui_sqnm_interract(client_t* clt, action_t* a)
{
    return gui_sqnm_pass_(clt, a);
}

size_t gui_sqnm_plin(client_t* clt, action_t* a)
{
    static size_t player_id = 0;
    a_plin_t* data = (a_plin_t*)(a->data);

    struct nk_context* ctx = clt->ctx;
    game_t* game = ((sd_game_t*)clt->sdata)->game;
    gw_sqnm_t* gw_sqnm = &((sd_game_t*)clt->sdata)->gw_game.gw_sqnm;

    nk_layout_row_dynamic(ctx, 30, 1);
    for (size_t i = 0; i < game->players_cnt; i++)
    {
        char buf[16] = "";
        sprintf(buf, "Player #%ld", i);
        if (nk_option_label(ctx, buf, player_id == i)) player_id = i;
    }

    if (player_id != data->i) /// Если обновился выбор, обновляем графику
    {
        data->i = player_id;
        gw_sqnm_update_tx(gw_sqnm, a);
    }

	return sizeof(a_plin_t);
}

size_t gui_sqnm_plout(client_t* clt, action_t* a)
{
    static size_t player_id = 0;
    a_plout_t* data = (a_plout_t*)(a->data);

    struct nk_context* ctx = clt->ctx;
    game_t* game = ((sd_game_t*)clt->sdata)->game;
    gw_sqnm_t* gw_sqnm = &((sd_game_t*)clt->sdata)->gw_game.gw_sqnm;

    nk_layout_row_dynamic(ctx, 30, 1);
    for (size_t i = 0; i < game->players_cnt; i++)
    {
        char buf[16] = "";
        sprintf(buf, "Player #%ld", i);
        if (nk_option_label(ctx, buf, player_id == i)) player_id = i;
    }

    if (player_id != data->o) /// Если обновился выбор, обновляем графику
    {
        data->o = player_id;
        gw_sqnm_update_tx(gw_sqnm, a);
    }

	return sizeof(a_plout_t);
}

size_t gui_sqnm_end(client_t* clt, action_t* a)
{
    // a_plout_t* data = (a_plout_t*)(a->data);
    // game_t* game = ((sd_game_t*)clt->sdata)->game;
    // data->o = game_get_curplayer_index(game);
	return gui_sqnm_pass_(clt, a);
}
