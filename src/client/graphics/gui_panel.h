#ifndef GUI_PANEL_H
#define GUI_PANEL_H

#define LG_DEFAULT_WINDOW NK_WINDOW_BORDER | NK_WINDOW_TITLE | NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_MOVABLE

#define LG_YOFFSET (20)
#define LG_XOFFSET (20)
#define LG_OFFSET LG_XOFFSET, LG_YOFFSET
#define LG_DEFAULT_SPACE (25)

#define LG_GAME_PANEL_WIDTH (200)
#define LG_TILE_PANEL_WIDTH (200)
#define LG_MENU_PANEL_WIDTH (200)
#define LG_DEBUG_PANEL_WIDTH (200)
#define LG_SQNM_PANEL_WIDTH 200

#define LG_GAME_PANEL_X  (LG_XOFFSET)
#define LG_TILE_PANEL_X  (LG_GAME_PANEL_X  + LG_GAME_PANEL_WIDTH + LG_DEFAULT_SPACE)
//#define LG_MENU_PANEL_X (WINDOW_HEIGHT
#define LG_DEBUG_PANEL_X (LG_TILE_PANEL_X  + LG_TILE_PANEL_WIDTH + LG_DEFAULT_SPACE)
#define LG_SQNM_PANEL_X  (LG_GAME_PANEL_X + LG_TILE_PANEL_WIDTH + LG_DEFAULT_SPACE)
#define LG_GAME_PANEL nk_rect(\
	LG_XOFFSET, LG_YOFFSET,\
	LG_GAME_PANEL_WIDTH, 250)

#define LG_DEBUG_PANEL nk_rect(\
	LG_DEBUG_PANEL_X, LG_YOFFSET, LG_DEBUG_PANEL_WIDTH, 250)

#define LG_SQNM_PANEL nk_rect(\
	LG_SQNM_PANEL_X, LG_YOFFSET, LG_SQNM_PANEL_WIDTH, 250)

// typedef struct client_t client_t;
//
// void panel_debug(client_t* clt);
// void panel_game(client_t* clt);

/// Game panels
// void panel_menu(client_t* clt);
// void panel_tile(client_t* clt);
// void panel_sqnm(client_t* clt);


#endif /* end of include guard: GUI_PANEL_H */
