#include <stdlib.h>
#include <stdio.h>

#include "camera.h"
#include "math/matrix.h"

// typedef struct
// {
//     float projection[16];
//     float identity[16];
//     float view[16];
//     float map_projection[16];
//
//     float scale;
//
//     point2f_t position;
//     point2f_t default_position;
//
// } camera_t;

typedef struct camera_t
{
    float projection[16];
    float view[16];

    float vp[16];

    float identity[16];
} camera_t;

camera_t camera;
void camera_update_projection(float k);

camera_t* camera_new()
{
    return malloc(sizeof(camera_t));
}

void camera_init(camera_t* camera, int w, int h)
{
    camera_init_projection_wh(camera, (float)w, (float)h);
    camera_init_view(camera);
    camera_init_identity(camera);
}

void camera_update(camera_t* camera, int w, int h)
{
    camera_init(camera, w, h);
}

void camera_init_projection_wh(camera_t* camera, float width, float height)
{
    matrix_ortho(camera->projection, 0, height, 0, width, 1.0f, -1.0f);
    matrix_translate(camera->projection, 1, 1, 0);
}

void camera_init_view(camera_t* camera)
{
    matrix_identity(camera->view);
}

void camera_init_identity(camera_t* camera)
{
    matrix_identity(camera->identity);
}

float* camera_get_projection(camera_t* camera)
{
    return camera->projection;
}

float* camera_get_view(camera_t* camera)
{
    return camera->view;
}

float* camera_get_identity(camera_t* camera)
{
    return camera->identity;
}
