#include <stdlib.h>
#include <stdio.h>

#include "graphics.h"
#include "shader.h"

static void shader_print_log(GLuint s, char* filepath);

/// you must free returned value char* if not NULL
static char* shader_read_file(char* filepath);

char* shader_read_file(char* filepath)
{
    FILE *f = fopen(filepath, "rb");
    if (!f)
    {
        printf("> Failed to read: <%s>\n", filepath);
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    long size = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char* buffer = malloc(size + 1);
    fread(buffer, size, 1, f);
    fclose(f);

    buffer[size] = '\0';
    return buffer;
}

SHADER_DECLARE(shader_tile);
SHADER_INIT(shader_tile);
SHADER_SOURCE(shader_tile, GL_VERTEX_SHADER, "tile_vert.glsl");
SHADER_SOURCE(shader_tile, GL_FRAGMENT_SHADER, "tile_frag.glsl");
SHADER_LINK(shader_tile);
SHADER_INIT_UNIFORM(shader_tile, tile_size);
SHADER_INIT_UNIFORM(shader_tile, tile_offset);
SHADER_INIT_UNIFORM(shader_tile, tile_texture);
SHADER_INIT_UNIFORM(shader_tile, mvp);
SHADER_INIT_END(shader_tile)
SHADER_USE(shader_tile)

SHADER_DECLARE(shader_tile_color);
SHADER_INIT(shader_tile_color);
SHADER_SOURCE(shader_tile_color, GL_VERTEX_SHADER, "tile_color_vert.glsl");
SHADER_SOURCE(shader_tile_color, GL_FRAGMENT_SHADER, "tile_color_frag.glsl");
SHADER_LINK(shader_tile_color);
    SHADER_INIT_UNIFORM(shader_tile_color, tile_size);
    SHADER_INIT_UNIFORM(shader_tile_color, tile_offset);
    SHADER_INIT_UNIFORM(shader_tile_color, tile_color);
    SHADER_INIT_UNIFORM(shader_tile_color, mvp);
SHADER_INIT_END(shader_tile_color)
SHADER_USE(shader_tile_color)

/// shader for nineslice textures
SHADER_DECLARE(shader_nineslice);
SHADER_INIT(shader_nineslice);
SHADER_SOURCE(shader_nineslice, GL_VERTEX_SHADER, "nineslice_vert.glsl");
SHADER_SOURCE(shader_nineslice, GL_FRAGMENT_SHADER, "nineslice_frag.glsl");
SHADER_LINK(shader_nineslice);
SHADER_INIT_UNIFORM(shader_nineslice, u_dimensions);
SHADER_INIT_UNIFORM(shader_nineslice, u_border);
SHADER_INIT_END(shader_nineslice)
SHADER_USE(shader_nineslice)

/// shader for tframe
SHADER_DECLARE(shader_tframe);
SHADER_INIT(shader_tframe);
    SHADER_SOURCE(shader_tframe, GL_GEOMETRY_SHADER, "tframe_geom.glsl");
    SHADER_SOURCE(shader_tframe, GL_VERTEX_SHADER, "tframe_vert.glsl");
    SHADER_SOURCE(shader_tframe, GL_FRAGMENT_SHADER, "tframe_frag.glsl");
    SHADER_LINK(shader_tframe);
    SHADER_INIT_UNIFORM(shader_tframe, mvp);
    SHADER_INIT_UNIFORM(shader_tframe, atlas);
    SHADER_INIT_UNIFORM(shader_tframe, rect);
    SHADER_INIT_UNIFORM(shader_tframe, corner);
    SHADER_INIT_UNIFORM(shader_tframe, border);
    SHADER_INIT_UNIFORM(shader_tframe, background);
    SHADER_INIT_UNIFORM(shader_tframe, width);
    SHADER_INIT_UNIFORM(shader_tframe, height);
SHADER_INIT_END(shader_tframe)
SHADER_USE(shader_tframe)

/// shader for sbox := simple box
SHADER_DECLARE(shader_sbox);
SHADER_INIT(shader_sbox);
    SHADER_SOURCE(shader_sbox, GL_VERTEX_SHADER, "sbox_vert.glsl");
    SHADER_SOURCE(shader_sbox, GL_GEOMETRY_SHADER, "sbox_geom.glsl");
    SHADER_SOURCE(shader_sbox, GL_FRAGMENT_SHADER, "sbox_frag.glsl");
    SHADER_LINK(shader_sbox);
    SHADER_INIT_UNIFORM(shader_sbox, mvp);
    SHADER_INIT_UNIFORM(shader_sbox, rect);
SHADER_INIT_END(shader_sbox)
SHADER_USE(shader_sbox)

/// shader for texarr
SHADER_DECLARE(shader_texarr);
SHADER_INIT(shader_texarr);
    SHADER_SOURCE(shader_texarr, GL_VERTEX_SHADER, "texarr_vert.glsl");
    SHADER_SOURCE(shader_texarr, GL_FRAGMENT_SHADER, "texarr_frag.glsl");
    SHADER_LINK(shader_texarr);
    SHADER_INIT_UNIFORM(shader_texarr, mvp);
    SHADER_INIT_UNIFORM(shader_texarr, layer);
SHADER_INIT_END(shader_texarr)
SHADER_USE(shader_texarr)

/// shader for tilex
SHADER_DECLARE(shader_tilex);
SHADER_INIT(shader_tilex);
    SHADER_SOURCE(shader_tilex, GL_VERTEX_SHADER, "tilex_vert.glsl");
    SHADER_SOURCE(shader_tilex, GL_GEOMETRY_SHADER, "tilex_geom.glsl");
    SHADER_SOURCE(shader_tilex, GL_FRAGMENT_SHADER, "tilex_frag.glsl");
    SHADER_LINK(shader_tilex);
    SHADER_INIT_UNIFORM(shader_tilex, mvp);
    SHADER_INIT_UNIFORM(shader_tilex, wh);
SHADER_INIT_END(shader_tilex)
SHADER_USE(shader_tilex)

/// shader for tilexu
SHADER_DECLARE(shader_tilexu);
SHADER_INIT(shader_tilexu);
    SHADER_SOURCE(shader_tilexu, GL_VERTEX_SHADER, "tilexu_vert.glsl");
    SHADER_SOURCE(shader_tilexu, GL_GEOMETRY_SHADER, "tilexu_geom.glsl");
    SHADER_SOURCE(shader_tilexu, GL_FRAGMENT_SHADER, "tilexu_frag.glsl");
    SHADER_LINK(shader_tilexu);
    /// vertex shader
    SHADER_INIT_UNIFORM(shader_tilexu, mvp);
    SHADER_INIT_UNIFORM(shader_tilexu, position);
    /// geometry shader
    SHADER_INIT_UNIFORM(shader_tilexu, wh);
    SHADER_INIT_UNIFORM(shader_tilexu, texture);
    SHADER_INIT_UNIFORM(shader_tilexu, transform);
    /// fragment shader
    SHADER_INIT_UNIFORM(shader_tilexu, style);
SHADER_INIT_END(shader_tilexu)
SHADER_USE(shader_tilexu)

/// shader for line
SHADER_DECLARE(shader_line);
SHADER_INIT(shader_line);
    SHADER_SOURCE(shader_line, GL_VERTEX_SHADER, "line_vert.glsl");
    SHADER_SOURCE(shader_line, GL_FRAGMENT_SHADER, "line_frag.glsl");
    SHADER_LINK(shader_line);
    /// vertex shader
    SHADER_INIT_UNIFORM(shader_line, mvp);
SHADER_INIT_END(shader_line)
SHADER_USE(shader_line)


static void shader_print_log(GLuint s, char* filepath)
{
    GLint length;
    GLchar* log;
    glGetShaderiv(s, GL_INFO_LOG_LENGTH, &length);
    log = malloc(sizeof(GLchar) * length);

    glGetShaderInfoLog(s, length, NULL, log);
    fprintf(stdout, "[GL]: failed to compile <%s>: \n%s\n", filepath, log);

    free(log);
}
