#ifndef GUI_H
#define GUI_H

// #include <stdbool.h>

struct nk_context;
struct nk_colorf;

typedef struct client_t client_t;
typedef struct GLFWwindow GLFWwindow;
typedef struct game_t game_t;
typedef struct level_t level_t;

/// gui main menu
// void gui_main_menu(client_t* clt);

/// gui game menu
// void gui_game_menu(client_t* clt);

/// gui game
// void gui_game(client_t* clt);

// void gui_game_basic(client_t* clt);
// /// if leave confirmed -> state is changed -> no need to render other parts of GUI
// bool gui_game_leave_confirm(client_t* clt);
// void gui_game_tile(client_t* clt);
// // void gui_game_controls(client_t* clt);
// // void gui_game_compute(client_t* clt);
// void gui_game_debug(client_t* clt);

/// gui settings menu
// void stt_settings_gui_menu(client_t* clt);

#endif /* end of include guard: GUI_H */
