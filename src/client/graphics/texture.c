#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "texture.h"

#define TEXTURES_FOLDER "assets/textures/"
#define TEXTURE_PATH "assets/textures/tilemap19.png"

static char* texture_get_last_atlas(char* path);

GLuint texture_id;

void texture_init()
{
    // char* atlas_path = texture_get_last_atlas("./assets/textures/");
    texture_load(TEXTURE_PATH);
}

static char* texture_get_last_atlas(char* path)
{
    DIR *folder;
    struct dirent *entry;
    int files = 0;

    folder = opendir(path);
    if(folder == NULL)
    {
        printf("[E] Unable to read directory: `%s`", path);
    }

    while((entry=readdir(folder))) {
        files++;
        printf("File %3d: %s\n", files, entry->d_name);
    }
    closedir(folder);
}

void texture_load(char* filepath)
{
    int width, height, channels;

    unsigned char *data = NULL;
    data = stbi_load(filepath, &width, &height, &channels, 0);
    if (data == NULL) printf("[W] > Unable to load: `%s`.\n", filepath);


    glCreateTextures(GL_TEXTURE_2D, 1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7); // pick mipmap level 7 or lower
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTextureParameteri(texture_id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(texture_id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // glTextureParameteri(texture_id, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // glTextureParameteri(texture_id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTextureStorage2D(texture_id, 1, GL_RGBA8, width, height);
    glTextureSubImage2D(texture_id,
                        0, 0, 0,
                        width, height,
                        GL_RGBA, GL_UNSIGNED_BYTE, data);

    free(data);
}
