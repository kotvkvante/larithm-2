#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

typedef struct camera_t camera_t;

camera_t* camera_new();
void camera_init(camera_t* camera, int w, int h);
void camera_update(camera_t* camera, int w, int h);

void camera_init_projection_wh(camera_t* camera, float width, float height);
void camera_init_view(camera_t* camera);
void camera_init_identity(camera_t* camera);

float* camera_get_projection(camera_t* camera);
float* camera_get_view(camera_t* camera);
float* camera_get_identity(camera_t* camera);

#endif // CAMERA_H_INCLUDED
