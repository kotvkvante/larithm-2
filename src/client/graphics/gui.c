#include <stdio.h>

#include "common/objs/base.h"
#include "common/objs/lever.h"

#include "common/game/game.h"
#include "common/level/levels_list.h"
#include "common/map/logical_map.h"
#include "common/map/map.h"
#include "common/player/player.h"
#include "common/player/sequence.h"

#include "client/graphics/graphics.h"
#include "picker.h"
#include "utils/utils.h"
#include "nuklear.h"
#include "client/graphics/gui_panel.h"
#include "client/graphics/gui.h"

#include "client/client.h"

#define STATE_GAME
#include "client/state/state.h"
#include "client/state/community_levels/state.h"
#include "client/state/choose_level/state.h"
#include "client/state/settings/state.h"
#include "client/state/main/state.h"

// void gui_panel_controls(struct nk_context* ctx)
// {
//     if (nk_begin(ctx, "Controls", nk_rect(300, 50, 230, 250),
//         NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
//         NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
//     {
//         nk_layout_row_dynamic(ctx, 30, 3);
//         nk_spacing(ctx, 1);
//         if (nk_button_label(ctx, " Up  "))
//             printf("Up\n");
//         nk_spacing(ctx, 1);
//         if (nk_button_label(ctx, "Left "))
//             printf("Left\n");
//         if (nk_button_label(ctx, "Use"))
//             printf("Used...\n");
//         if (nk_button_label(ctx, "Right"))
//             printf("Right\n");
//         nk_spacing(ctx, 1);
//         if (nk_button_label(ctx, "Down "))
//             printf("Down\n");
//         nk_spacing(ctx, 1);
//
//     } nk_end(ctx);
// }
//
// void gui_panel_compute(struct nk_context* ctx)
// {
//     if (nk_begin(ctx, "Compute", nk_rect(550, 50, 230, 250),
//         NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
//         NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
//     {
//         nk_layout_row_dynamic(ctx, 35, 1);
//
//         if (nk_button_label(ctx, "Compute"))
//             printf("Computed...\n");
//
//         if (nk_button_label(ctx, "Backspace"))
//             printf("Backspace...\n");
//
//         if (nk_button_label(ctx, "Reset"))
//             printf("Reseted...\n");
//
//     } nk_end(ctx);
// }
