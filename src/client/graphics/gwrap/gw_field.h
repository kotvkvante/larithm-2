#ifndef GW_FIELD_H
#define GW_FIELD_H

#include "client/graphics/tframe.h"

typedef struct gw_field_t {
    float mvp[16]; /// field matrix
	tframe_t bg;  /// tframe for field background
} gw_field_t;

#endif /* end of include guard: GW_FIELD_H */
