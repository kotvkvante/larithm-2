#ifndef GW_LVLIST_H
#define GW_LVLIST_H

void gw_lvlist_update(client_t* clt, lvlist_t* lvlist, float x, float y);

#endif /* end of include guard: GW_LVLIST_H */
