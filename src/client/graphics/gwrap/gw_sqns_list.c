#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "utils/utils.h"

#include "common/player/sequence.h"
#include "common/tile/tilex.h"
#include "common/tile/htilex.h"
#include "common/game/game.h"
#include "common/player/player.h"

#include "client/graphics/texture.h"
#include "client/graphics/gwrap/gw_sqns_list.h"

extern int SQN_TEXTURES[SQN_COUNT];
extern tilexf_t ACT_PLMOVE[4];
extern sqn_ftable_t sqn_ftable;

// static void get_texture_tt_tf_(game_t* game, action_t* act, int* tt, tilexf_t* tf);
static void gw_sqn_remove_(sqn_t* sqn, size_t n, void* ctx);
static void gw_sqn_insert_(sqn_t* sqn, size_t n, void* ctx);
// static void gw_sqn_remove_last_(sqn_t* sqn, void* ctx);

void gw_sqns_list_init(gw_sqns_list_t* gw_sqns_list, game_t* game)
{
	gw_sqns_list->cur       = &game->player_cur;
	gw_sqns_list->cnt	    = game->players_cnt;
	gw_sqns_list->sqns_list = game->sqns_list;
	gw_sqns_list->hts_list  = malloc(sizeof(htilex_t) * gw_sqns_list->cnt);
	tframe_init(&gw_sqns_list->defbg, 0, 1,
		TEXTURE_TFRAME_CORNER, TEXTURE_TFRAME_BORDER,
		TEXTURE_TFRAME_BACKGROUND);

	size_t n = 0;
	for (size_t i = 0; i < gw_sqns_list->cnt; i++)
	{
		n += game->sqns_list[i].acts_max;
	}

	tilex_t* tl = malloc(sizeof(tilex_t) * n);

    for (size_t i = 0; i < gw_sqns_list->cnt; i++)
    {
		sqn_t*   sqn = &gw_sqns_list->sqns_list[i];
		htilex_t* ht = &gw_sqns_list->hts_list[i];

        gw_sqn_init(sqn, ht, tl);
		tl += sqn->acts_max;
    }

	 sqn_ftable.ctx = game;
	 sqn_ftable.remove = gw_sqn_remove_;
	 sqn_ftable.insert = gw_sqn_insert_;
	 // sqn_ftable.remove_last = gw_sqn_remove_last_;
	 // sqn_ftable.inc = gfx_sqn_inc_;
	 // sqn_ftable.dec = gfx_sqn_dec_;
}

void gw_sqn_init(sqn_t* sqn, htilex_t* ht, tilex_t* tl)
{
	ht->tilexs_max = sqn->acts_count;
	ht->tilexs_cnt = 0;
	ht->tilexs_list = tl;

	int tt = 0;
	tilexf_t tf = 0;
	for (size_t i = 0; i < sqn->acts_count; i++)
	{
		action_t* act = &sqn->acts_list[i];
		gw_sqn_get_texture_tt_tf(act, &tt, &tf);
		ht_tilex_append_ex(ht, NULL, (float)i, 0, tt, tf);
	}

	sqn->gd = (void*)ht;
}

void gw_sqn_get_texture_tt_tf(action_t* act, int* tt, tilexf_t* tf)
{
	*tt = SQN_TEXTURES[act->action];
	*tf = 0;

	switch (act->action) {
		case SQN_START: break;

		case SQN_PLMOVE: {
			*tf = ACT_PLMOVE[((a_plmove_t*)act->data)->d];
		} break;

		case SQN_PLIN: {
			size_t i = ((a_plin_t*)act->data)->i;
			*tt = (*tt) | ( (TEXTURE_SEQUENCE_PLAYER_1 + i) << TEXTURE_LAYER_SHIFT);
		} break;

		case SQN_PLOUT: {
			size_t o = ((a_plout_t*)act->data)->o;
			*tt = (*tt) | ( (TEXTURE_SEQUENCE_PLAYER_1 + o) << TEXTURE_LAYER_SHIFT);
		} break;

		case SQN_END: break;

		default: break;
	}
}

// static void gw_sqn_inc_(sqn_t* sqn, void* ctx)
// {
// 	// htilex_t* ht = (htilex_t*)sqn->gd;
// 	// size_t n = act_next - 1;
// }

// static void gw_sqn_dec_(sqn_t* sqn, void* ctx)
// {
// 	// htilex_t* ht = (htilex_t*)sqn->gd;
// 	// size_t n = act_next + 1;
// }

static void gw_sqn_insert_(sqn_t* sqn, size_t n, void* ctx)
{
	LA2_UNUSED(ctx);
	htilex_t* ht = (htilex_t*)sqn->gd;
	action_t* act = &sqn->acts_list[n];

    for (size_t j = sqn->acts_count; j > n; j--) {
		ht->tilexs_list[j] = ht->tilexs_list[j-1];
		ht->tilexs_list[j].x = (float)j;
	}

	int tt = 0;
	tilexf_t tf = 0;
	gw_sqn_get_texture_tt_tf(act, &tt, &tf);
	ht->tilexs_list[n] = (tilex_t){
		.x = (float)n, .y = 0.0f,
		.texture = tt, .transform = tf,
	};
	ht->tilexs_cnt++;
}

static void gw_sqn_remove_(sqn_t* sqn, size_t n, void* ctx)
{
	LA2_UNUSED(ctx);
	htilex_t* ht = (htilex_t*)sqn->gd;

	for (size_t i = n; i < ht->tilexs_cnt; i++) {
        ht->tilexs_list[i] = ht->tilexs_list[i+1];
		ht->tilexs_list[i].x = (float)i;
	}
	ht->tilexs_cnt--;
}

// static void gw_sqn_remove_last_(sqn_t* sqn, void* ctx)
// {
// 	LA2_UNUSED(ctx);
// 	htilex_t* ht = (htilex_t*)sqn->gd;
// 	ht->tilexs_cnt--;
// }
