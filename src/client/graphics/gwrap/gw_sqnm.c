#include "common/tile/tilex.h"

#include "common/player/sequence.h"
#include "common/player/sqnm.h"

#include "client/graphics/texture.h"
#include "client/graphics/gui_sqnm.h"
#include "client/graphics/gwrap/gw_sqnm.h"
#include "client/graphics/gwrap/gw_sqns_list.h"

sqnm_handler_f handlers[SQN_COUNT] =
{
	[SQN_NONE] = NULL,
	[SQN_START] = gui_sqnm_start,
	[SQN_PLMOVE] = gui_sqnm_plmove,
	[SQN_PLINTERACT] = gui_sqnm_interract,
	[SQN_PLOUT] = gui_sqnm_plin,
	[SQN_PLIN] = gui_sqnm_plout,
	[SQN_END] = gui_sqnm_end
};

int SQN_TEXTURES[SQN_COUNT] = {
		[SQN_NONE		] = 1,
		[SQN_START		] = 54,
		[SQN_PLMOVE		] = 24,
		[SQN_PLINTERACT	] = 39,
		[SQN_PLOUT		] = 38,
		[SQN_PLIN		] = 46,
		[SQN_END		] = 55,
};

tilexf_t ACT_PLMOVE[4] = {
	[RIGHT]	= ROTATE0,
	[DOWN]	= ROTATE90,
	[LEFT]	= ROTATE180,
	[UP]	= ROTATE270,
};

void gw_sqnm_init(gw_sqnm_t* gw_sqnm, sqnm_t* sqnm)
{
    gw_sqnm->sqnm = sqnm;
    gw_sqnm->ht.tilexs_list = malloc(sizeof(tilex_t) * (sqnm->cnt - 1));
    gw_sqnm->ht.tilexs_cnt  = 0;
    gw_sqnm->ht.tilexs_max  = sqnm->cnt - 1;

	tframe_init(&gw_sqnm->bg, sqnm->cnt - 1, 1,
		TEXTURE_TFRAME_CORNER,
		TEXTURE_TFRAME_BORDER,
		TEXTURE_TFRAME_BACKGROUND);

    for (size_t i = 1; i < sqnm->cnt; i++)
    {
		action_t* act = &sqnm->acts_list[i];
		int tt = 0;
		tilexf_t tf = 0;
		gw_sqn_get_texture_tt_tf(act, &tt, &tf);
		ht_tilex_append(&gw_sqnm->ht, (float)(i - 1), 0.0, tt, tf);
    }

	tilex_init(&gw_sqnm->th, 0, 0, TEXTURE_HOVERED, 0);
	tilex_init(&gw_sqnm->ts, 0, 0, TEXTURE_SELECTED, 0);
}

void gw_sqnm_update_tx(gw_sqnm_t* gw_sqnm, action_t* act)
{
    int tt = 0;
    tilexf_t tf = 0;
    gw_sqn_get_texture_tt_tf(act, &tt, &tf);
    ht_tilex_update(&gw_sqnm->ht, act->action-1, (float)(act->action-1), 0, tt, tf);
}

void gw_sqnm_set_slcd(gw_sqnm_t* gw_sqnm, size_t i)
{
	sqnm_t* sm = gw_sqnm->sqnm;
	sqnm_set_selected(sm, i);
	action_t* a = sqnm_get_slcd_act(sm);
	sm->handler = handlers[a->action];

	tilex_set_x(&gw_sqnm->ts, (float)(i - 1));
}

void gw_sqnm_set_hvrd(gw_sqnm_t* gw_sqnm, size_t i)
{
	sqnm_set_hovered(gw_sqnm->sqnm, i);
	tilex_set_x(&gw_sqnm->th, (float)(i - 1));
}

void gw_sqnm_unset_slcd(gw_sqnm_t* gw_sqnm)
{
	sqnm_unset_selected(gw_sqnm->sqnm);
}

void gw_sqnm_unset_hvrd(gw_sqnm_t* gw_sqnm)
{
	sqnm_unset_hovered(gw_sqnm->sqnm);
}
