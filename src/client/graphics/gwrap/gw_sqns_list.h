#ifndef GW_SQNS_LIST
#define GW_SQNS_LIST

typedef struct game_t game_t;
typedef struct htilex_t htilex_t;
typedef struct sqn_t sqn_t;

#include "client/graphics/tframe.h"

typedef struct gw_sqns_list_t {
	size_t* cur;
	size_t cnt;
	sqn_t* sqns_list;
	htilex_t* hts_list;
	tframe_t defbg; /// default background;
} gw_sqns_list_t;

void gw_sqns_list_init(gw_sqns_list_t* gw_sqn_list, game_t* game);
void gw_sqn_init(sqn_t* sqn, htilex_t* ht, tilex_t* tl);
void gw_sqn_get_texture_tt_tf(action_t* act, int* tt, tilexf_t* tf);

#endif /* end of include guard: GW_SQNS_LIST */
