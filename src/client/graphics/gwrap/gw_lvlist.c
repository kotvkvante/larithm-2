#include "client/client.h"
#include "client/graphics/graphics.h"
#include "client/graphics/picker.h"
#include "common/level/levels_list.h"

#include "client/graphics/gwrap/gw_lvlist.h"

void gw_lvlist_update(client_t* clt, lvlist_t* lvlist, float x, float y)
{
	int i, j;
	if (picker_get_hovered_ij((int)x, clt->window_height - (int)y, &i, &j) != PICKER_LEVELS_LIST)
	{
		lvlist_unset_hovered(lvlist);
		return;
	}

	lvlist_set_hovered(lvlist, i);
	if (glfwGetMouseButton(clt->window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		lvlist_set_selected(lvlist, i);
	}
}
