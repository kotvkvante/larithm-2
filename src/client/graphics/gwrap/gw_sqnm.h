#ifndef GW_SQNM_T
#define GW_SQNM_T

#include "common/tile/htilex.h"
#include "client/graphics/tframe.h"
// #include "client/graphics/gwrap/gw_group.h"

typedef struct sqnm_t sqnm_t;
typedef struct action_t action_t;

typedef struct gw_sqnm_t {
	sqnm_t* sqnm;
	float mvp[16];
	tframe_t bg;
	htilex_t ht;

	tilex_t ts;
	tilex_t th;
} gw_sqnm_t;

void gw_sqnm_init(gw_sqnm_t* gw_sqnm, sqnm_t* sqnm);
void gw_sqnm_update_tx(gw_sqnm_t* gw_sqnm, action_t* act);
void gw_sqnm_set_slcd(gw_sqnm_t* gw_sqnm, size_t i);
void gw_sqnm_set_hvrd(gw_sqnm_t* gw_sqnm, size_t i);
void gw_sqnm_unset_slcd(gw_sqnm_t* gw_sqnm);
void gw_sqnm_unset_hvrd(gw_sqnm_t* gw_sqnm);

#endif /* end of include guard: GW_SQNM_T */
