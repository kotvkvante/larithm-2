#ifndef GW_GROUP_H
#define GW_GROUP_H

#include <stdint.h>

typedef struct gw_sgroup_t {
	size_t cnt;
	float x, y;
	float dx, dy;
} gw_sgroup_t;

// typedef struct gw_tfgroup_t {
// 	size_t cnt;
// 	float x, y;
// 	float dx, dy;
// 	float* mvp_list[16];
// 	tframe_t* tf_list;
// } gw_tfgroup_t;

// typedef struct gw_htgroup_t {
// 	size_t cnt;
// 	float x, y;
// 	float dx, dy;
// 	// float* mvp_list[16];
// 	htilex_t* ht_list;
// 	tframe_t* tf_list;
// } gw_tfgroup_t;

#endif /* end of include guard: GW_GROUP_H */
