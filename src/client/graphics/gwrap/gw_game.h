#ifndef GW_GAME_H
#define GW_GAME_H

#include "common/player/sequence.h"
#include "common/player/sqnm.h"

#include "client/graphics/tframe.h"
#include "client/graphics/gwrap/gw_field.h"
#include "client/graphics/gwrap/gw_group.h"
#include "client/graphics/gwrap/gw_sqnm.h"
#include "client/graphics/gwrap/gw_sqns_list.h"

typedef struct gfx_t gfx_t;
typedef struct game_t game_t;
typedef struct tilex_t tilex_t;
typedef struct map_t map_t;

typedef struct gw_game_t {
	game_t* game;

	gw_field_t mf;  /// map field
	htilex_t mht; 	/// map ht

	gw_field_t lmf; /// logical map field
	htilex_t lmht; 	/// logical map ht
	htilex_t lmlht; /// logical map links ht

	// char* connectors_list; /// Размер этого массива определяется
	// 					   /// количеством элементов lmap


	gw_sqns_list_t gw_sqns_list;
	// size_t sqnhts;
	// htilex_t* sqnhts_list;

	gw_sqnm_t gw_sqnm;

	// gw_htgroup_t gw_sqns;
	// gw_tfgroup_t gw_stfg; /// sqn tfgroup
	// gw_tfgroup_t stfg;

	// tframe_t* sqnf;
	// gw_sqnms_t sqnms; /// sequences bgroup

	// float lmap_m[16]; /// lmap model
    // tframe_t lmap_bg; // tframe for logical map background

    // size_t   tilexs_count;
	// tilex_t* tilexs_list;
} gw_game_t;

void gw_game_init(gfx_t* gfx, gw_game_t* gw_game);


#endif /* end of include guard: GW_GAME_H */
