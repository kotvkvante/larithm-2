#include <stdio.h>
#include <math.h>

#include "math/matrix.h"

#include "common/objs/base.h"
#include "common/objs/lever.h"
#include "common/objs/door.h"
#include "common/objs/star.h"
#include "common/game/game.h"
#include "common/map/logical_map.h"
#include "common/map/map.h"
#include "common/player/player.h"
#include "common/player/sqnm.h"
#include "common/tile/htilex.h"

#include "common/elems/base.h"
#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/texture.h"
#include "client/graphics/camera.h"
#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "client/graphics/gwrap/gw_game.h"
#include "client/graphics/gwrap/gw_sqnm.h"

static void gw_game_map_init_(map_t* map);
static void gw_game_lmap_init_(logical_map_t* lmap);

static float position_correction__(float x);
static void obj_lever_update_gfx_(obj_lever_t* lever, void* data);
static void obj_door_update_gfx_(obj_door_t* door, void* data);
static void obj_star_update_gfx_(obj_star_t* star, void* data);

static void elem_input_update_gfx_(elem_input_t* input, void* data);
static void elem_output_update_gfx_(elem_output_t* output, void* data);
static void elem_logical_update_gfx_(elem_logical_t* logical, void* data);

extern obj_lever_update_gfx_f obj_lever_update_gfx;
extern obj_door_update_gfx_f obj_door_update_gfx;
extern obj_star_update_gfx_f obj_star_update_gfx;
extern void* obj_gfx_data;

extern elems_gfx_callback_table_t elems_gfx_callback_table;

void gw_game_init(gfx_t* gfx, gw_game_t* gw_game)
{
    // sd_game_t* data = (sd_game_t*)(gfx->client->sdata);
    game_t* game        = gw_game->game;
    map_t* map          = game_get_map(game);
    logical_map_t* lmap = game_get_lmap(game);
    camera_t* camera    = gfx_get_camera(gfx);
    float* projection   = camera_get_projection(camera);

    /// prepare map stuff
    /// prepare map's matrices
    float h = gfx->client->window_height;
    float w = gfx->client->window_width;
    float dx = position_correction__(w); /// correction for odd screen width
    float dy = position_correction__(h); /// correction for odd screen height

    /// prepare map mvp matrix
    /// Нужно: учесть размер тайла 64px -> 32px.
    /// Аналогично ниже, для logical map.
    float dm = -((float)map->size)/2; /// := base delta for map
    float dxm = 64.0f/w * 2 * (dm);   /// := delta x for map
    float dym = 64.0f/h * 2 * (dm);
    float mposx = dxm + (-0.5f) - dx; /// := map position x
    float mposy = dym + (-0.5f) - dy; /// := map position y
    matrix_identity(gw_game->mf.mvp);
    matrix_multiply(gw_game->mf.mvp, gw_game->mf.mvp, projection);
    matrix_translate(gw_game->mf.mvp, mposx, mposy, 0);

    ht_init(&gw_game->mht, map->objs_max);
    map->ht = &gw_game->mht;

    obj_lever_update_gfx = obj_lever_update_gfx_;
    obj_door_update_gfx  = obj_door_update_gfx_;
    obj_star_update_gfx  = obj_star_update_gfx_;
    obj_gfx_data = map->ht;

    gw_game_map_init_(map);
    tframe_init_default(&gw_game->mf.bg, map->size, map->size);
    tilex_init(&map->ts, 0, 0, TEXTURE_SELECTED, 0);
    tilex_init(&map->th, 0, 0, TEXTURE_HOVERED, 0);

    /// prepare lmap mvp matrix
    float dlm = -((float)lmap->size)/2; /// := base delta for logical map
    float dxlm = 64.0f/w * 2 * (dlm);   /// := delta x for logical map
    float dylm = 64.0f/h * 2 * (dlm);
    float lmposx = dxlm + (+0.5f) - dx; /// := map position x
    float lmposy = dylm + (-0.5f) - dy; /// := map position y

    matrix_identity(gw_game->lmf.mvp);
    matrix_multiply(gw_game->lmf.mvp, gw_game->lmf.mvp, projection);
    matrix_translate(gw_game->lmf.mvp, lmposx, lmposy, 0.0f);

    ht_init(&gw_game->lmht, lmap->elements_max);
    /// Нужно: вычислить число связей
    // ht_init(&gw_game->lmlht, );
    lmap->ht = &gw_game->lmht;

    elems_assign_callback(input, elem_input_update_gfx_);
    elems_assign_callback(output, elem_output_update_gfx_);
    elems_assign_callback(logical, elem_logical_update_gfx_);
    elems_assign_data(lmap->ht);

    gw_game_lmap_init_(lmap);
    tframe_init_default(&gw_game->lmf.bg, lmap->size, lmap->size);
    tilex_init(&lmap->ts, 0, 0, TEXTURE_SELECTED, 0);
    tilex_init(&lmap->th, 0, 0, TEXTURE_HOVERED, 0);

    gw_sqnm_init(&gw_game->gw_sqnm, game->sqnm);
    matrix_identity(gw_game->gw_sqnm.mvp);
    matrix_multiply(gw_game->gw_sqnm.mvp, gw_game->gw_sqnm.mvp, projection);

    gw_sqns_list_init(&gw_game->gw_sqns_list, game);
}

static float position_correction__(float x)
{
    return fmod(x/2, 2)/x;
}

static void gw_game_map_init_(map_t* map)
{
    htilex_t* ht = (htilex_t*)map->ht;

    for (size_t i = 1; i < map->objs_count; i++)
    {
        obj_base_t* obj = map->objs_list[i];
        ht_tilex_append_ex(ht, &obj->tind, (float)obj->x, (float)obj->y, obj->texture, 0);

        // switch (obj->type) {
        //     case OBJ_TYPE_DOOR:
        //         ht_tilex_append_ex(ht, NULL, (float)obj->x, (float)obj->y, 1, 0);
        //         break;
        //
        //     // case OBJ_TYPE_LEVER:
        //     //     // ht_tilex_append_ex(ht, NULL, (float)obj->x, (float)obj->y, 1, 0);
        //     //     break;
        //     default:
        //         ht_tilex_append_ex(ht, &obj->tind, (float)obj->x, (float)obj->y, obj->texture, 0);
        //         break;
        // }

    }
}

static void gw_game_lmap_init_(logical_map_t* lmap)
{
    htilex_t* ht = (htilex_t*)lmap->ht;

    for (size_t i = 1; i < lmap->elements_count; i++)
    {
        elem_base_t* elem = lmap->elements_list[i];
        ht_tilex_append_ex(ht, &elem->tind, (float)elem->x, (float)elem->y, elem->texture, 0);
    }
}

static void obj_lever_update_gfx_(obj_lever_t* lever, void* data)
{
    htilex_t* ht = (htilex_t*)data;
    ht_tilex_update(ht, lever->tind, (float)lever->x, (float)lever->y,
        lever->texture, 0);
}

static void obj_door_update_gfx_(obj_door_t* door, void* data)
{
    htilex_t* ht = (htilex_t*)data;
    ht_tilex_update(ht, door->tind, (float)door->x, (float)door->y,
        door->texture, 0);
}

static void obj_star_update_gfx_(obj_star_t* star, void* data)
{
    htilex_t* ht = (htilex_t*)data;
    ht_tilex_update(ht, star->tind, (float)star->x, (float)star->y,
        star->texture, 0);
}

static void elem_input_update_gfx_(elem_input_t* input, void* data)
{
    htilex_t* ht = (htilex_t*)data;

    input->texture |= (TEXTURE_ELEM_IO_LIGHT * input->value) << TEXTURE_LAYER_SHIFT;
    ht_tilex_update(ht, input->tind, (float)input->x, (float)input->y,
        input->texture, 0);
}

static void elem_output_update_gfx_(elem_output_t* output, void* data)
{
    htilex_t* ht = (htilex_t*)data;

    if (output->value) output->texture |= (TEXTURE_ELEM_IO_LIGHT << TEXTURE_LAYER_SHIFT);
    else output->texture &= ~(TEXTURE_ELEM_IO_LIGHT << TEXTURE_LAYER_SHIFT);

    ht_tilex_update(ht, output->tind, (float)output->x, (float)output->y,
        output->texture, 0);
}

static void elem_logical_update_gfx_(elem_logical_t* logical, void* data)
{
    htilex_t* ht = (htilex_t*)data;
    ht_tilex_update(ht, logical->tind, (float)logical->x, (float)logical->y,
        logical->texture, 0);
}
