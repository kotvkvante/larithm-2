#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/graphics.h"
#include "client/graphics/sbox.h"

// static float vertices[] =
// {
// 	-0.5f, -0.5f,
// 	 0.5f, -0.5f,
// 	 0.5f,  0.5f,
// 	-0.5f,  0.5f,
// };

static float vertices[] =
{
	-1.0f, -1.0f,
	 1.0f, -1.0f,
	 1.0f,  1.0f,
	-1.0f,  1.0f,
};

// static float vertices[] =
// {
// 	0.0f, 0.0f,
// 	1.0f, 0.0f,
// 	1.0f, 1.0f,
// 	0.0f, 1.0f,
// };

// static float vertices[] =
// {
// 	0.0f, 0.0f,
// 	255.0f, 0.0f,
// 	255.0f, 255.0f,
// 	0.0f, 255.0f,
// };

void sbox_init(gfx_t* gfx)
{
	sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);
    glGenVertexArrays(1, &sdata->sb_vao);
	glBindVertexArray(sdata->sb_vao);

    glGenBuffers(1, &sdata->sb_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, sdata->sb_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
}
