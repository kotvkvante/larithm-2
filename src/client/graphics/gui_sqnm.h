#ifndef GUI_SQNM_H
#define GUI_SQNM_H

#include <stddef.h>

typedef struct client_t client_t;
typedef struct action_t action_t;

size_t gui_sqnm_start(client_t* clt, action_t* a);
size_t gui_sqnm_plmove(client_t* clt, action_t* a);
size_t gui_sqnm_interract(client_t* clt, action_t* a);
size_t gui_sqnm_plin(client_t* clt, action_t* a);
size_t gui_sqnm_plout(client_t* clt, action_t* a);
size_t gui_sqnm_end(client_t* clt, action_t* a);

#endif /* end of include guard: GUI_SQNM_H */
