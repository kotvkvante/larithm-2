#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "math/matrix.h"

#include "client/client.h"
#include "common/objs/base.h"
#include "common/game/game.h"
#include "common/map/map.h"
#include "common/tile/tilex.h"
#include "client/graphics/texture.h"
#include "client/graphics/graphics.h"
#include "client/graphics/picker.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"

#include "client/graphics/gwrap/gw_field.h"
#include "gfx.h"

extern SHADER_DECLARE(shader_tilexu);
extern SHADER_DECLARE(shader_tile_color);

static void gfx_draw_map_prep_color(gfx_t* gfx, float* mvp)
{
    LA2_UNUSED(gfx);
    glUniformMatrix4fv(shader_tile_color.SU(mvp), 1, GL_FALSE, mvp);
}

static void gfx_draw_map_prep(gfx_t* gfx, float* mvp)
{
    LA2_UNUSED(gfx);
    glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
}

void gfx_draw_map(gfx_t* gfx, map_t* map, gw_field_t* f)
{
    gfx_draw_map_prep(gfx, f->mvp);
    gfx_draw_map_bg(gfx, &f->bg);
    gfx_draw_map_objs(gfx, (htilex_t*)map->ht);

    if (map->tile_hovered) gfx_draw_tilexu(gfx, &map->th);
    if (map->tile_selected) gfx_draw_tilexu(gfx, &map->ts);
}

void gfx_draw_map_color(gfx_t* gfx, map_t* map, gw_field_t* f)
{
    gfx_draw_map_prep_color(gfx, f->mvp);
    gfx_draw_map_bg_color(gfx, map);
}

void gfx_draw_map_objs(gfx_t* gfx, htilex_t* ht)
{
    for (size_t i = 0; i < ht->tilexs_cnt; i++)
        gfx_draw_tilexu(gfx, &ht->tilexs_list[i]);
}

void gfx_draw_map_bg(gfx_t* gfx, tframe_t* bg)
{
    gfx_draw_tframe_txu(gfx, bg);
}

void gfx_draw_map_bg_color(gfx_t* gfx, map_t* map)
{
    for (size_t i = 0; i < map->size; i++)
    for (size_t j = 0; j < map->size; j++)
    {
        gfx_draw_tile_color(gfx, PICKER_MAP_TILE, i, j, (float)i, (float)j);
    }
}
