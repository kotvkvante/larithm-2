#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "client/graphics/camera.h"

extern GLuint texture_id;
extern SHADER_DECLARE(shader_tile_color);

void gfx_draw_tile_color_start(gfx_t* gfx, float tile_size)
{
    glBindVertexArray(gfx->vao_empty);
    shader_tile_color_use();
    glUniform1f(shader_tile_color.unif_tile_size, tile_size);
}


void gfx_draw_tile_color(gfx_t* gfx, int r, int g, int b, float xoffset, float yoffset)
{
    LA2_UNUSED(gfx);

    glUniform2f(shader_tile_color.unif_tile_offset, xoffset, yoffset);
    glUniform3i(shader_tile_color.unif_tile_color, r, g, b);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void gfx_draw_tile_color_end()
{
    // gfx_use_default_shader();
}
