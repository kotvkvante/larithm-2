#include "common/objs/base.h"
#include "common/game/game.h"
#include "common/level/levels_list.h"
#include "common/map/map.h"
#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/texture.h"
#include "client/graphics/camera.h"
#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "client/graphics/picker.h"

#include "math/matrix.h"

#include "gfx.h"

#define DELTAX 1.5f

extern SHADER_DECLARE(shader_tile);
extern SHADER_DECLARE(shader_tile_color);

static float* mvp = NULL;

/// Важно: эта функция зависит от `gfx_lvlist_draw_color` через mvp
void gfx_lvlist_draw(gfx_t* gfx, lvlist_t* lvlist)
{
    gfx_draw_tile_start(gfx, GAME_TILE_SIZE);

    glUniformMatrix4fv(shader_tile.unif_mvp, 1, GL_FALSE, mvp);

    for (size_t i = 0; i < lvlist->lvlems_count; i++) {
        gfx_draw_tile(gfx, TEXTURE_LVLIST_ELEMENT, (float)i + DELTAX, 0);
    }

    if (lvlist->lvlem_hovered)
    {
        gfx_draw_tile(gfx, TEXTURE_HOVERED, (float)lvlist->hi + DELTAX, 0);
    }

    if (lvlist->lvlem_selected)
    {
        gfx_draw_tile(gfx, TEXTURE_SELECTED, (float)lvlist->si + DELTAX, 0);
    }

    gfx_draw_tile_end();
}

void gfx_lvlist_draw_color(gfx_t* gfx, lvlist_t* lvlist)
{
    gfx_draw_tile_color_start(gfx, GAME_TILE_SIZE);

    camera_t* camera = gfx_get_camera(gfx);
    mvp = camera_get_projection(camera);
    glUniformMatrix4fv(shader_tile_color.unif_mvp, 1, GL_FALSE, mvp);

    for (size_t i = 0; i < lvlist->lvlems_count; i++) {
        gfx_draw_tile_color(gfx, PICKER_LEVELS_LIST, (float)i, 0,
            (float)i + DELTAX, 0.0f);
    }

    gfx_draw_tile_color_end();
}
