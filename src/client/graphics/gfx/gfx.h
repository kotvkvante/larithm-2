#ifndef GFX_H
#define GFX_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct gfx_t gfx_t;
typedef struct game_t game_t;
typedef struct map_t map_t;
typedef struct logical_map_t logical_map_t;

typedef struct gw_game_t gw_game_t;
typedef struct htilex_t htilex_t;
typedef struct tframe_t tframe_t;
typedef struct gw_field_t gw_field_t;
typedef struct gw_sqns_list_t gw_sqns_list_t;

// game
void gfx_game_init(gfx_t* gfx, gw_game_t* gw_game);
void gfx_game_draw(gfx_t* gfx, gw_game_t* gw_game);
void gfx_game_draw_color(gfx_t* gfx, gw_game_t* gw_game);

// tile [texture]
void gfx_draw_tile_start(gfx_t* gfx, float tile_size);
void gfx_draw_tile(gfx_t* gfx, int texture, float xoffset, float yoffset);
void gfx_draw_tile_end();

// tile color
void gfx_draw_tile_color_start(gfx_t* gfx, float tile_size);
void gfx_draw_tile_color(gfx_t* gfx, int r, int g, int b, float xoffset, float yoffset);
void gfx_draw_tile_color_end();

// map draw
void gfx_draw_map(gfx_t* gfx, map_t* map, gw_field_t* f);
void gfx_draw_map_objs(gfx_t* gfx, htilex_t* ht);
// void gfx_draw_map_objs(gfx_t* gfx, map_t* map);
void gfx_draw_map_bg(gfx_t* gfx, tframe_t* map);

void gfx_draw_map_color(gfx_t* gfx, map_t* map, gw_field_t* f);
void gfx_draw_map_bg_color(gfx_t* gfx, map_t* map);

// void gfx_draw_map_objects_color(gfx_t* gfx, map_t* map);
// void gfx_draw_map_background(gfx_t* gfx, map_t* map);
// void gfx_draw_map_background_color(gfx_t* gfx, map_t* map);
// void gfx_draw_map_border(gfx_t* gfx, map_t* map);

// logical map draw
void gfx_draw_lmap(gfx_t* gfx, logical_map_t* logical_map, gw_field_t* f);
void gfx_draw_lmap_color(gfx_t* gfx, logical_map_t* lmap, gw_field_t* f);
void gfx_draw_lmap_elems(gfx_t* gfx, htilex_t* ht);
// void gfx_draw_lmap_objects_color(gfx_t* gfx, logical_map_t* logical_map);
void gfx_draw_lmap_bg(gfx_t* gfx, tframe_t* bg);
// void gfx_draw_lmap_background(gfx_t* gfx, logical_map_t* logical_map);
void gfx_draw_lmap_background_color(gfx_t* gfx, logical_map_t* logical_map);
void gfx_draw_lmap_border(gfx_t* gfx, logical_map_t* logical_map);

// levels list draw
typedef struct lvlist_t lvlist_t;
typedef struct gfx_lvlist_t gfx_lvlist_t;

void gfx_lvlist_draw(gfx_t* gfx, lvlist_t* lvlist);
void gfx_lvlist_draw_color(gfx_t* gfx, lvlist_t* lvlist);
void gfx_lvlist_init(gfx_t* gfx, lvlist_t* lvlist);

// player
typedef struct player_t player_t;
// void gfx_player_init(gfx_t* gfx, player_t* p);
void gfx_player_draw(gfx_t* gfx, player_t* p);
// void gfx_players_list_draw(gfx_t* gfx);
void gfx_players_list_draw(gfx_t* gfx, game_t* game);

// sequence
typedef struct sqn_t sqn_t;
// void gfx_sqn_draw(gfx_t* gfx, sqn_t* sqn);
// void gfx_sqn_draw(gfx_t* gfx, htilex_t* ht);
void gfx_sqn_draw(gfx_t* gfx, htilex_t* ht, size_t n);
void gfx_sqns_list_draw(gfx_t* gfx, gw_sqns_list_t* gw_sqns_list);

/* sqn menu */
typedef struct gw_sqnm_t gw_sqnm_t;
void gfx_sqnm_draw(gfx_t* gfx, gw_sqnm_t* gw_sqnm);
void gfx_sqnm_draw_color(gfx_t* gfx, gw_sqnm_t* gw_sqnm);

/* nineslice */
typedef struct nineslice_t nineslice_t;
void gfx_draw_nineslice(gfx_t* gfx, nineslice_t* ns);

/* tframe */
void gfx_draw_tframe(gfx_t* gfx, tframe_t* tf);
void gfx_draw1_tframe(gfx_t* gfx, tframe_t* tf);
void gfx_draw_tframe_xy(gfx_t* gfx, tframe_t* tf, float x, float y);
void gfx_draw_tframe_txu(gfx_t* gfx, tframe_t* tf);

/* sbox */
void gfx_draw_sbox(gfx_t* gfx, int w, int h);

/* tilex */
// void gfx_draw_tilex(gfx_t* gfx, gw_tilex_t* t);

/* tilexu */
typedef struct tilex_t tilex_t;
void gfx_draw_tilexu_start(gfx_t* gfx);
void gfx_draw_tilexu(gfx_t* gfx, tilex_t* t);

/* test */
void gfx_draw_test(gfx_t* gfx, gw_game_t* gw_game);
void gfx_draw_test_line(gfx_t* gfx, gw_game_t* gw_game);
void gfx_draw_test_links(gfx_t* gfx, gw_game_t* gw_game);


#endif /* end of include guard: GFX_H */
