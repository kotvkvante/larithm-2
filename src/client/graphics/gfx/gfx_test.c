#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "client/client.h"
#include "client/graphics/graphics.h"
#include "client/graphics/gfx/gfx.h"
#include "client/graphics/gwrap/gw_game.h"
#include "client/graphics/shader.h"
#include "client/graphics/texture.h"

#include "common/game/game.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"
#include "common/map/graph.h"

extern SHADER_DECLARE(shader_tilexu);
extern SHADER_DECLARE(shader_line);

void gfx_draw_test(gfx_t* gfx, gw_game_t* gw_game)
{
	glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, gw_game->lmf.mvp);

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 1.0,
		.y = 1.0,
		.texture = TEXTURE_ELEM_LINK_POINT,
		.transform = 0
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 3.0,
		.y = 0.0,
		.texture = TEXTURE_ELEM_LINK_POINT,
		.transform = 2
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 2.5,
		.y = 0.0,
		.texture = TEXTURE_ELEM_LINK_CORNER,
		.transform = 1
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 2.5,
		.y = 0.5,
		.texture = TEXTURE_ELEM_LINK_CORNER,
		.transform = 3
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 2.0,
		.y = 0.5,
		.texture = TEXTURE_ELEM_LINK_LINE,
		.transform = 0
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 1.5,
		.y = 0.5,
		.texture = TEXTURE_ELEM_LINK_LINE,
		.transform = 0
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 1.25,
		.y = 0.5,
		.texture = TEXTURE_ELEM_LINK_CORNER,
		.transform = 1
	});

	gfx_draw_tilexu(gfx, &(tilex_t) {
		.x = 1.25,
		.y = 0.75,
		.texture = TEXTURE_ELEM_LINK_POINT,
		.transform = 1
	});
}

void gfx_draw_test_line(gfx_t* gfx, gw_game_t* gw_game)
{
	shader_line_use();
	glBindVertexArray(gfx->vao_line);
	glUniformMatrix4fv(shader_line.SU(mvp), 1, GL_FALSE, gw_game->mf.mvp);

	// glLineWidth(1.0f);
	glLineWidth(4.0f);
    glDrawArrays(GL_LINES, 0, 2);

	glBindVertexArray(gfx->vao_empty);
}

void gfx_draw_test_links(gfx_t* gfx, gw_game_t* gw_game)
{
	game_t* game = gw_game->game;
	logical_map_t* lmap = game_get_lmap(game);
	graph_t* g = &lmap->g;

	shader_line_use();
	glBindVertexArray(gfx->vao_line);
	glUniformMatrix4fv(shader_line.SU(mvp), 1, GL_FALSE, gw_game->lmf.mvp);

	edgenode_t* p;
	for (size_t i = 0; i < (size_t)g->nvertices; i++)
	{
		p = g->edges[i];
		elem_base_t* elem1 = lmap->elements_list[i];
		while(p != NULL) {
			// printf("\t [%ld]", i);
			// printf("\t [%d]\n", p->y);

			elem_base_t* elem2 = lmap->elements_list[p->y];
			float data[10] = {
				(float)elem1->x * 64.0f + 32.0f, (float)elem1->y * 64.0f + 32.0f,
				0.0f, 0.0f, 1.0f,
				(float)elem2->x * 64.0f + 32.0f, (float)elem2->y * 64.0f + 32.0f,
				0.0f, 0.0f, 1.0f,
			};
			// float data[4] = {
			// 	0.0f, 0.0f,
			// 	1.0f, 1.0f,
			// };

			glBindBuffer(GL_ARRAY_BUFFER, gfx->vbo_line);
			glBufferSubData(GL_ARRAY_BUFFER, 0, 10 * sizeof(float), data);

			// glLineWidth(1.0f);
			glLineWidth(4.0f);
			glDrawArrays(GL_LINES, 0, 2);

			p = p->next;
		}



	}


	glBindVertexArray(gfx->vao_empty);
}
