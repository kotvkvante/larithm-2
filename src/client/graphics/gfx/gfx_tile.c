#include "../graphics.h"
#include "../shader.h"
#include "../camera.h"

extern GLuint texture_id;
extern shader_tile_t shader_tile;

void gfx_draw_tile_start(gfx_t* gfx, float tile_size)
{
    glBindVertexArray(gfx->vao_empty);
    shader_tile_use();

    glUniform1f(shader_tile.unif_tile_size, tile_size);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_id);
}


void gfx_draw_tile(gfx_t* gfx, int texture, float xoffset, float yoffset)
{
    LA2_UNUSED(gfx);
    glUniform2f(shader_tile.unif_tile_offset,  xoffset, yoffset);
    glUniform1i(shader_tile.unif_tile_texture, texture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


void gfx_draw_tile_end()
{
    gfx_use_default_shader();
}
