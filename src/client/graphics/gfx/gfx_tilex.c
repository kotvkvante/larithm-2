#include "math/matrix.h"

#include "client/client.h"
#include "client/state/state.h"

#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"
#include "client/graphics/gfx/gfx.h"

extern SHADER_DECLARE(shader_tilex);

// void gfx_draw_tilex(gfx_t* gfx, gw_tilex_t* t)
// {
// 	static float mvp[16];
// 	matrix_identity(mvp);
//
// 	sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);
//
//     glBindVertexArray(sdata->tx_vao);
//
// 	shader_tilex_use();
//
// 	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
// 	glUniform2f(shader_tilex.unif_wh,
// 		64.0f/gfx->client->window_width,
// 		64.0f/gfx->client->window_height);
//
// 	camera_t* camera = gfx_get_camera(gfx);
//
// 	matrix_multiply(mvp, mvp, camera_get_projection(camera));
// 	glUniformMatrix4fv(shader_tilex.unif_mvp, 1, GL_FALSE, mvp);
// 	glDrawArrays(GL_POINTS, 0, 4);
// }
