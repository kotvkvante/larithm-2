#include "math/matrix.h"

#include "client/client.h"
#include "client/state/state.h"

#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"
#include "client/graphics/gfx/gfx.h"
#include "common/tile/tilex.h"

extern SHADER_DECLARE(shader_tilexu);

void gfx_draw_tilexu_start(gfx_t* gfx)
{
	glBindVertexArray(gfx->vao_empty);
	shader_tilexu_use();
	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
    glUniform2f(shader_tilexu.SU(wh), gfx->client->window_width,
                                      gfx->client->window_height);
	glUniform1i(shader_tilexu.SU(style), TXU_NOSTYLE);
}

void LT_gfx_draw_tilexu(gfx_t* gfx, tilex_t* tx)
{
	float mvp[16];
	matrix_identity(mvp);
	glBindVertexArray(gfx->vao_empty);
	camera_t* camera = gfx_get_camera(gfx);
	matrix_multiply(mvp, mvp, camera_get_projection(camera));

	shader_tilexu_use();

	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);

	glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
	glUniform2f(shader_tilexu.SU(position), tx->x, tx->y);
	glUniform2f(shader_tilexu.SU(wh),
		gfx->client->window_width,
		gfx->client->window_height);
	glUniform1i(shader_tilexu.SU(texture), tx->texture);
	glUniform1i(shader_tilexu.SU(transform), tx->transform);

	glDrawArrays(GL_POINTS, 0, 1);
}

void gfx_draw_tilexu(gfx_t* gfx, tilex_t* tx)
{
	LA2_UNUSED(gfx);
    glUniform2f(shader_tilexu.SU(position), tx->x, tx->y);
	glUniform1i(shader_tilexu.SU(texture), tx->texture);
    glUniform1i(shader_tilexu.SU(transform), tx->transform);
    glDrawArrays(GL_POINTS, 0, 1);
}
