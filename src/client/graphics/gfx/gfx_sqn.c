#include "gfx.h"

#include "math/matrix.h"

#include "client/client.h"

// #include "client/state/state_game.h"
// #include "client/state/state.h"

#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "client/graphics/camera.h"
#include "client/graphics/texture.h"
#include "client/graphics/picker.h"
#include "client/graphics/gwrap/gw_sqnm.h"
#include "client/graphics/gwrap/gw_sqns_list.h"

#include "common/game/game.h"
#include "common/player/player.h"
#include "common/player/sequence.h"
#include "common/player/sqnm.h"

// extern SHADER_DECLARE(shader_tile);
extern SHADER_DECLARE(shader_tile_color);
extern SHADER_DECLARE(shader_tilexu);

extern int SQN_TEXTURES[SQN_COUNT];

// static int BACKGROUND = 47;

// void gfx_sqn_draw(gfx_t* gfx, sqn_t* sqn)
// {
//     if (sqn == NULL) return;
//
//     tilex_t tx = {
//         .x = 0.0f, .y = 0.0f,
//         .texture = 0,
//         .transform = 0,
//     };
//
//     glUniform1i(shader_tilexu.SU(style), TXU_GRAYSCALE);
//     for (size_t i = 0; i < sqn->act_next; i++) {
//         action_t* act = &sqn->acts_list[i];
//         tx.x = (float)i;
//         tx.texture = SQN_TEXTURES[act->action];
//         gfx_draw_tilexu(gfx, &tx);
//     }
//
//     glUniform1i(shader_tilexu.SU(style), TXU_NOSTYLE);
//     for (size_t i = sqn->act_next; i < sqn->acts_count; i++)
//     {
//         action_t* act = &sqn->acts_list[i];
//         tx.x = (float)i;
//         tx.texture = SQN_TEXTURES[act->action];
//         gfx_draw_tilexu(gfx, &tx);
//     }
//
//     tx.texture = TEXTURE_SEQUENCE;
//     tx.x = ((float)sqn->act_next)-0.5;
//     gfx_draw_tilexu(gfx, &tx);
// }

// void gfx_sqns_draw(gfx_t* gfx, sqnm_t* sm, sqn_t* sqns, gw_tfgroup* tfg, size_t cnt)
// {
// 	float mvp[16];
// 	camera_t* camera = gfx_get_camera(gfx);
// 	float* projection = camera_get_projection(camera);
//
//     // float dlm = -((float)lmap->size)/2; /// := base delta for logical map
//     // float dxlm = 64.0f/w * 2 * (dlm);   /// := delta x for logical map
//     // float dylm = 64.0f/h * 2 * (dlm);
//     // float lmposx = dxlm + (-0.5f); // - dx; /// := map position x
//     // float lmposy = dylm + (+0.5f); // - dy; /// := map position y
//     float lmposx = (-0.5f); // - dx; /// := map position x
//     float lmposy = (+0.5f); // - dy; /// := map position y
//
//     matrix_identity(mvp);
//     matrix_multiply(mvp, mvp, projection);
//     matrix_translate(mvp, lmposx, lmposy, 0.0f);
//
//     glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
//
// 	gfx_draw_tframe();
//
// }

// void gfx_sqn_draw(gfx_t* gfx, htilex_t* ht)
// {
//     for (size_t i = 0; i < ht->tilexs_cnt; i++)
//         gfx_draw_tilexu(gfx, &ht->tilexs_list[i]);
// }

// void gfx_sqns_draw(gfx_t* gfx, size_t n, htilex_t* sqns_list)
// {
//     for (size_t i = 0; i < n; i++) {
//         htilex_t* ht = sqns_list[i];
//         gfx_sqn_draw(gfx, sqn);
//     }
// }

void gfx_sqns_list_draw(gfx_t* gfx, gw_sqns_list_t* gw_sqns_list)
{
    float mvp[16];
    camera_t* camera = gfx_get_camera(gfx);

    tilex_t tx = {
        .x = 0.0f, .y = 0.0f,
        .texture = TEXTURE_SELECTED,
        .transform = 0,
    };

    /// Иконки перед sequence
    matrix_identity(mvp);
    matrix_multiply(mvp, mvp,
        camera_get_projection(camera));
    matrix_translate(mvp, -0.15f, 0.0f, 0.0f);

    for (size_t i = 0; i < gw_sqns_list->cnt; i++)
    {
        glUniform1i(shader_tilexu.SU(style), TXU_NOSTYLE);

        matrix_translate(mvp, 0.0f, 0.25f, 0.0f);
        glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
        gw_sqns_list->defbg.w = 1;
        gfx_draw_tframe_txu(gfx, &gw_sqns_list->defbg);

        /// Текстуры игроков расположены со сдвигом равным номеру игрока
        tx.texture = TEXTURE_SEQUENCE_PLAYER_1 + i;
        gfx_draw_tilexu(gfx, &tx);
    }

    matrix_identity(mvp);
    matrix_multiply(mvp, mvp,
        camera_get_projection(camera));
    matrix_translate(mvp, -0.15f, (*gw_sqns_list->cur + 1) * 0.25f, 0.0f);

    glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
    tx.texture = TEXTURE_SELECTED;
    gfx_draw_tilexu(gfx, &tx);

    /// sequence
    matrix_identity(mvp);
    matrix_multiply(mvp, mvp,
        camera_get_projection(camera));
    for (size_t i = 0; i < gw_sqns_list->cnt; i++)
    {
        glUniform1i(shader_tilexu.SU(style), TXU_NOSTYLE);

        sqn_t* sqn = &gw_sqns_list->sqns_list[i];
        matrix_translate(mvp, 0.0f, 0.25f, 0.0f);
        glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
        gw_sqns_list->defbg.w = (sqn->acts_count == 0) ? 1: sqn->acts_count;
        gfx_draw_tframe_txu(gfx, &gw_sqns_list->defbg);


        gfx_sqn_draw(gfx, &gw_sqns_list->hts_list[i], sqn->act_next);
    }
}

void gfx_sqn_draw(gfx_t* gfx, htilex_t* ht, size_t n)
{
    size_t i = 0;


    // uint32_t a = TXU_APPLYCOLOR | (255 << 8) | (127 << 16) | (63 << 24); /// Вариант с цветом
    // glUniform1i(shader_tilexu.SU(style), a);
    glUniform1i(shader_tilexu.SU(style), TXU_GRAYSCALE);
    for ( ; i < n; i++)
        gfx_draw_tilexu(gfx, &ht->tilexs_list[i]);

    glUniform1i(shader_tilexu.SU(style), TXU_NOSTYLE);
    for ( ; i < ht->tilexs_cnt; i++)
        gfx_draw_tilexu(gfx, &ht->tilexs_list[i]);

    tilex_t tx = {
        .x = ((float)n)-0.5f, .y = 0.0f,
        .texture = TEXTURE_SEQUENCE_CURSOR,
        .transform = 0,
    };
    gfx_draw_tilexu(gfx, &tx);
}


void gfx_sqnm_draw(gfx_t* gfx, gw_sqnm_t* gw_sqnm)
{
    glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, gw_sqnm->mvp);

    gfx_draw_tframe_txu(gfx, &gw_sqnm->bg);

    for (size_t i = 0; i < gw_sqnm->ht.tilexs_cnt; i++)
    {
        tilex_t* tx = &gw_sqnm->ht.tilexs_list[i];
        gfx_draw_tilexu(gfx, tx);
    }

	if (gw_sqnm->sqnm->hvrd) gfx_draw_tilexu(gfx, &gw_sqnm->th);
    if (gw_sqnm->sqnm->slcd) gfx_draw_tilexu(gfx, &gw_sqnm->ts);
}

void gfx_sqnm_draw_color(gfx_t* gfx, gw_sqnm_t* gw_sqnm)
{
	// static float mvp[16] = { 0 };
	// sd_game_t* data = (sd_game_t*)(gfx->client->sdata);
    // game_t* game = data->game;

    // gw_game_t*     gw_game = &((sd_game_t*)(gfx->client->sdata))->gw_game;
    // gw_sqnm_t*     gw_sqnm = &gw_game->gw_sqnm;
    // matrix_identity(mvp);
    // matrix_multiply(mvp,
    //     data->sqn_origin,
    //     camera_get_projection(camera));
    // matrix_translate(mvp, 0.0f, 0.05f, 0.5f);
    glUniformMatrix4fv(shader_tile_color.unif_mvp, 1, GL_FALSE, gw_sqnm->mvp);
    // glUniformMatrix4fv(shader_tile_color.unif_mvp, 1, GL_FALSE, mvp);

	sqnm_t* s = gw_sqnm->sqnm;
    for (size_t i = 1; i < s->cnt; i++)
    {
		// gfx_draw_tile(gfx, SQN_TEXTURES[act]+((a_plmove_t*)sqn->acts_list[i].data)->d, (float)i, 0);

		gfx_draw_tile_color(gfx, PICKER_SQN_MENU_LIST, (float)i, 0, (float)(i-1), 0.0f);

		// gfx_draw_tile(gfx, SQN_TEXTURES[i], (float)i, 0);
    }
}
