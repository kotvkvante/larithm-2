#include "math/matrix.h"
#include "common/objs/base.h"
#include "common/game/game.h"
#include "common/player/player.h"
#include "common/map/logical_map.h"
#include "common/map/map.h"
#include "client/client.h"

// #include "client/state/state_game.h"
#include "client/state/state.h"

#include "client/graphics/texture.h"
#include "client/graphics/camera.h"
#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "gfx.h"

extern SHADER_DECLARE(shader_tile);

// void depr_gfx_game_draw(gfx_t* gfx)
// {
//     game_t* game = ((sd_game_t*)(gfx->client->sdata))->game;
//     logical_map_t* lmap = game_get_lmap(game);
//     map_t*         map  = game_get_map(game);
//
//     gfx_draw_map_background(gfx, map);
//
//     gfx_draw_tile_start(gfx, GAME_TILE_SIZE);
//
//     gfx_draw_map(gfx, map);
//     gfx_players_list_draw(gfx);
//
//     gfx_draw_lmap(gfx, lmap);
//
//     gfx_sqn_list_draw(gfx);
//     gfx_sqn_menu_draw(gfx);
//
//     gfx_draw_tile_end();
// }

void gfx_game_draw(gfx_t* gfx, gw_game_t* gw_game)
{
    game_t*        game    = gw_game->game;
    logical_map_t* lmap    = game_get_lmap(game);
    map_t*         map     = game_get_map(game);

    gfx_draw_tilexu_start(gfx);
    //

    gfx_draw_map(gfx, map, &gw_game->mf);
    gfx_players_list_draw(gfx, gw_game->game);
    gfx_draw_lmap(gfx, lmap, &gw_game->lmf);
    gfx_sqnm_draw(gfx, &gw_game->gw_sqnm);
    gfx_sqns_list_draw(gfx, &gw_game->gw_sqns_list);

    gfx_draw_test(gfx, gw_game);
    gfx_draw_test_links(gfx, gw_game);
    //
    //
    // gfx_draw_test_line(gfx, gw_game);
    // gfx_sqns_draw(gfx, &gw_game->sqns_list);
    // gfx_sqn_draw(gfx, &gw_game->game->players_list[0]->sqn);
    // gfx_sqn_draw(gfx, gw_game->game->players_list[1]->sqn);
    //
    // gfx_sqn_menu_draw(gfx);
    gfx_draw_tile_end();
}


void gfx_game_draw_color(gfx_t* gfx, gw_game_t* gw_game)
{
    game_t* game        = gw_game->game;
    map_t*  map         = game_get_map(game);
    logical_map_t* lmap = game_get_lmap(game);

    gfx_draw_tile_color_start(gfx, GAME_TILE_SIZE);

    // Draw map background
    gfx_draw_map_color(gfx, map, &gw_game->mf);
    gfx_draw_lmap_color(gfx, lmap, &gw_game->lmf);

    gfx_sqnm_draw_color(gfx, &gw_game->gw_sqnm);

    gfx_draw_tile_color_end();
}
