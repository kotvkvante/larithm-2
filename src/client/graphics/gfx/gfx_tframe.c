#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"
#include "client/graphics/gfx/gfx.h"
#include "client/graphics/tframe.h"
#include "common/tile/tilex.h"
#include "math/matrix.h"
#include <math.h>

extern SHADER_DECLARE(shader_tframe);
extern SHADER_DECLARE(shader_texarr);
extern SHADER_DECLARE(shader_tilexu);

extern GLuint texture_id;


static float mvp[16];

void gfx_draw_tframe(gfx_t* gfx, tframe_t* tf)
{
	// sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);

	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
	glBindVertexArray(gfx->vao_empty);

	shader_tframe_use();
	camera_t* camera = gfx_get_camera(gfx);
	matrix_identity(mvp);
	matrix_translate(mvp, -0.25, 0.0, 0.0);
	matrix_multiply(mvp, mvp, camera_get_projection(camera));

	glUniformMatrix4fv(shader_tframe.unif_mvp, 1, GL_FALSE, mvp);

	glUniform1i(shader_tframe.unif_corner, tf->corner);
	glUniform1i(shader_tframe.unif_border, tf->border);
	glUniform1i(shader_tframe.unif_background, tf->background);
	glUniform2f(shader_tframe.unif_rect,
		64.0f/gfx->client->window_width,
		64.0f/gfx->client->window_height);

	glUniform1i(shader_tframe.unif_width,  1 + tf->w);
	glUniform1i(shader_tframe.unif_height, 1 + tf->h);
	// glUniform1f(shader_tframe.unif_width, 2.0f);
	glDrawArrays(GL_POINTS, 0, 1);
}

void gfx_draw_tframe_xy(gfx_t* gfx, tframe_t* tf, float x, float y)
{
	// sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);

	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
	glBindVertexArray(gfx->vao_empty);


	shader_tframe_use();
	camera_t* camera = gfx_get_camera(gfx);
	matrix_identity(mvp);
	matrix_translate(mvp, x, y, 0.0);
	matrix_multiply(mvp, mvp, camera_get_projection(camera));

	glUniformMatrix4fv(shader_tframe.unif_mvp, 1, GL_FALSE, mvp);

	glUniform1i(shader_tframe.unif_corner, tf->corner);
	glUniform1i(shader_tframe.unif_border, tf->border);
	glUniform1i(shader_tframe.unif_background, tf->background);
	glUniform2f(shader_tframe.unif_rect,
		64.0f/gfx->client->window_width,
		64.0f/gfx->client->window_height);

	glUniform1i(shader_tframe.unif_width,  1 + tf->w);
	glUniform1i(shader_tframe.unif_height, 1 + tf->h);
	// glUniform1i(shader_tframe.unif_atlas, gfx->texarr);
	// glUniform1f(shader_tframe.unif_width, 2.0f);
	glDrawArrays(GL_POINTS, 0, 1);
}

void gfx_draw1_tframe(gfx_t* gfx, tframe_t* tf)
{
	LA2_UNUSED(tf);

	// sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);
	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
    glBindVertexArray(gfx->vao);

	camera_t* camera = gfx_get_camera(gfx);

	shader_texarr_use();
	float mvp[16];
	matrix_identity(mvp);
	matrix_scale(mvp, 64.0f, 64.0f, 1.0f);

	matrix_multiply(mvp, camera_get_projection(camera), mvp);

	glUniformMatrix4fv(shader_texarr.unif_mvp, 1, GL_FALSE, mvp);
	glUniform1i(shader_texarr.unif_layer, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void gfx_draw_tframe_txu(gfx_t* gfx, tframe_t* tf)
{
	if ((tf->w == 0) || (tf->h == 0)) return;

	tilex_t tx = { .x = 0, .y = 0,
				   .texture = tf->background,
				   .transform = 0 };

	for (size_t i = 0; i < tf->w; i++) {
		for (size_t j = 0; j < tf->h; j++) {
			tx.x = (float)i;
			tx.y = (float)j;
			gfx_draw_tilexu(gfx, &tx);
		}
	}

	tx.texture = tf->border;
	for (size_t i = 0; i < tf->w - 1 ; i++) {
		tx.x = (float)i + 0.5f;
		tx.y = tf->h - 0.5f;
		gfx_draw_tilexu(gfx, &tx);

		tilex_mirrorx(&tx);
		tx.y = 0 - 0.5f;
		gfx_draw_tilexu(gfx, &tx);
		tilex_mirrorx(&tx);
	}

	tx.transform = ROTATE90;
	for (size_t i = 0; i < tf->h - 1; i++)
	{
		tx.x = tf->w - 0.5f;
		tx.y = (float)i + 0.5f;
		gfx_draw_tilexu(gfx, &tx);

		tilex_mirrory(&tx);
		tx.x = 0 - 0.5f;
		gfx_draw_tilexu(gfx, &tx);
		tilex_mirrory(&tx);
	}

	tx.transform = 0;
	tx.texture = tf->corner;
	tx.x = -0.5f;
	tx.y = tf->h - 0.5f;
	gfx_draw_tilexu(gfx, &tx);

	tilex_mirrory(&tx);
	tx.x = tf->w - 0.5f;
	gfx_draw_tilexu(gfx, &tx);

	// tilex_mirrorx(&tx);
	tx.transform = MIRRORXY;
	// tilex_mirrory(&tx);
	tx.y = -0.5f;
	gfx_draw_tilexu(gfx, &tx);
	//
	tilex_mirrory(&tx);
	tx.x = -0.5f;
	tx.y = -0.5f;
	gfx_draw_tilexu(gfx, &tx);
}
