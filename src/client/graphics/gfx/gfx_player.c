#include <stdlib.h>
#include <stdio.h>

#include "common/game/game.h"
#include "common/player/player.h"

#include "client/client.h"
#include "client/state/state.h"

#include "client/graphics/texture.h"
#include "client/graphics/graphics.h"
#include "client/graphics/shader.h"
#include "client/graphics/gfx/gfx.h"

void gfx_player_draw(gfx_t* gfx, player_t* p)
{
    gfx_draw_tilexu(gfx, &p->t);
}

void gfx_players_list_draw(gfx_t* gfx, game_t* game)
{
    /// No need to setup matrices because this
    /// function is called after gfx_draw_map.
    for (size_t i = 0; i < game->players_cnt; i++)
    {
        player_t* p  = &game->players_list[i];
        gfx_player_draw(gfx, p);
    }
}
