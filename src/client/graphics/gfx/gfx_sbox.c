#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"
#include "client/graphics/gfx/gfx.h"
#include "math/matrix.h"
#include <math.h>

extern SHADER_DECLARE(shader_sbox);
extern GLuint texture_id;

void gfx_draw_sbox(gfx_t* gfx, int w, int h)
{
	sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glBindVertexArray(sdata->sb_vao);

	float model[16], mvp[16];
	matrix_identity(model);
	// matrix_scale(model, 200.0f, 400.0f, 1.0f);

	shader_sbox_use();
	camera_t* camera = gfx_get_camera(gfx);

	matrix_multiply(mvp, model, camera_get_projection(camera));

	glUniformMatrix4fv(shader_sbox.unif_mvp, 1, GL_FALSE, mvp);
	glUniform2f(shader_sbox.unif_rect,
		((float)h)/gfx->client->window_width,
		((float)w)/gfx->client->window_height);

	glDrawArrays(GL_POINTS, 0, 1);
}
