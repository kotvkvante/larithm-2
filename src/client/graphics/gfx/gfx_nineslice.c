#include "math/matrix.h"

#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/shader.h"
#include "client/graphics/nineslice.h"

extern SHADER_DECLARE(shader_nineslice);
extern GLuint texture_id;

void gfx_draw_nineslice(gfx_t* gfx, nineslice_t* ns)
{
	float mvp[16];
	camera_t* camera = gfx_get_camera(gfx);

	matrix_identity(mvp);
	matrix_scale(mvp, 2.25f, 4.5f, 1.0f);
	matrix_multiply(mvp, mvp, camera_get_projection(camera));

	shader_nineslice_use();
	glBindVertexArray(gfx->vao);

    glBindTexture(GL_TEXTURE_2D, texture_id);

	// glUniform1f(shader_nineslice.unif_tile_size, 32.0f);
    // glUniform2f(shader_nineslice.unif_tile_offset, 0, 0);
    // glUniform1i(shader_nineslice.unif_tile_texture, 0);

	// glUniformMatrix4fv(shader_nineslice.unif_mvp, 1, GL_FALSE, mvp);
	glUniform2f(shader_nineslice.unif_u_dimensions, 0.09f, 0.16f);
	glUniform2f(shader_nineslice.unif_u_border, 1/4.0f, 1/4.0f);



    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	// printf(">> `gfx_draw_nineslice`\n");
}
