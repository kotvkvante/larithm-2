#include <stdlib.h>
#include <stdio.h>

#include "math/matrix.h"

#include "common/elems/base.h"
#include "common/game/game.h"
#include "common/map/logical_map.h"

#include "client/client.h"
#include "client/state/state.h"

#include "../texture.h"
#include "../graphics.h"
#include "../picker.h"
#include "../camera.h"
#include "../shader.h"
#include "gfx.h"

extern SHADER_DECLARE(shader_tile_color);
extern SHADER_DECLARE(shader_tilexu);

static void gfx_draw_lmap_prep_color(gfx_t* gfx, float* mvp)
{
    glUniformMatrix4fv(shader_tile_color.SU(mvp), 1, GL_FALSE, mvp);
}

static void gfx_draw_lmap_prep(gfx_t* gfx, float* mvp)
{
    glUniformMatrix4fv(shader_tilexu.SU(mvp), 1, GL_FALSE, mvp);
}

void gfx_draw_lmap(gfx_t* gfx, logical_map_t* lmap, gw_field_t* f)
{
    gfx_draw_lmap_prep(gfx, f->mvp);
    gfx_draw_lmap_bg(gfx, &f->bg);
    gfx_draw_lmap_elems(gfx, (htilex_t*)lmap->ht);

    if (lmap->tile_hovered)  gfx_draw_tilexu(gfx, &lmap->th);
    if (lmap->tile_selected) gfx_draw_tilexu(gfx, &lmap->ts);
}

void gfx_draw_lmap_color(gfx_t* gfx, logical_map_t* lmap, gw_field_t* f)
{
    gfx_draw_lmap_prep_color(gfx, f->mvp);
    gfx_draw_lmap_background_color(gfx, lmap);
}

void gfx_draw_lmap_elems(gfx_t* gfx, htilex_t* ht)
{
    for (size_t i = 0; i < ht->tilexs_cnt; i++)
        gfx_draw_tilexu(gfx, &ht->tilexs_list[i]);
}

void gfx_draw_lmap_bg(gfx_t* gfx, tframe_t* bg)
{
    gfx_draw_tframe_txu(gfx, bg);
}

void gfx_draw_lmap_background_color(gfx_t* gfx, logical_map_t* lmap)
{
    for (size_t i = 0; i < lmap->size; i++)
    for (size_t j = 0; j < lmap->size; j++)
    {
        gfx_draw_tile_color(gfx, PICKER_LOGICAL_MAP_TILE, i, j, (float)i, (float)j);
    }
}
