#ifndef NINESLICE_H
#define NINESLICE_H

typedef struct nineslice_t
{
	size_t texture; /// texture id
	size_t b; /// border
	size_t w; /// width

} nineslice_t;


#endif /* end of include guard: NINESLICE_H */
