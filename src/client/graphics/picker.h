#ifndef PICKER_H
#define PICKER_H

typedef struct picker_t picker_t;

typedef enum {
    PICKER_NONE = 0,
    PICKER_MAP_TILE,
    PICKER_LOGICAL_MAP_TILE,
    PICKER_LEVELS_LIST,
    PICKER_SQN_MENU_LIST,
} picker_id_t;


void picker_init(int width, int height);
void picker_resize(int width, int height);

int picker_begin();
void picker_read_pixel(unsigned int x, unsigned int y, unsigned int rgb[3]);


void picker_read_pixel_int(unsigned int x, unsigned int y);
int picker_get_hovered_ij(int x, int y, int* i, int* j);

void picker_end();

#endif /* end of include guard: PICKER_H */
