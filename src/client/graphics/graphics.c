#include <stdlib.h>
#include <stdio.h>

#include "client/client.h"

#include "common/map/map.h"
#include "common/objs/base.h"
#include "math/matrix.h"

#include "graphics.h"
// #include "client/graphics/texture.h"
#include "client/graphics/texarr.h"
#include "camera.h"
#include "picker.h"
#include "shader.h"

#include "gfx/gfx.h"

extern GLuint texture_id;

gfx_t gfx;

float vertices[] = {
    0.5f,  0.5f, 0.0f, 1.0f, 1.0f,   // top right
    0.5f, -0.5f, 0.0f, 1.0f, 0.0f,   // bottom right
   -0.5f, -0.5f, 0.0f, 0.0f, 0.0f,   // bottom left
   -0.5f,  0.5f, 0.0f, 0.0f, 1.0f    // top left
};

float vertices1[] = {
    100.5f,  100.5f, 0.0f, 1.0f, 1.0f,   // top right
    100.5f, -100.5f, 0.0f, 1.0f, 0.0f,   // bottom right
   -100.5f, -100.5f, 0.0f, 0.0f, 0.0f,   // bottom left
   -100.5f,  100.5f, 0.0f, 0.0f, 1.0f    // top left
};

float vertices2[] = {
    100.0f, 100.0f, 0.0f, 1.0f, 1.0f,
    100.0f, 10.0f,  0.0f, 1.0f, 0.0f,
    10.0f,  10.0f,  0.0f, 0.0f, 0.0f,
    10.0f,  100.0f, 0.0f, 0.0f, 1.0f,
};

float vertices3[] = {
    WINDOW_WIDTH, WINDOW_HEIGHT, 0.0f, 1.0f, 1.0f,
    WINDOW_WIDTH, 0,  0.0f, 1.0f, 0.0f,
    0,  0,  0.0f, 0.0f, 0.0f,
    0,  WINDOW_HEIGHT, 0.0f, 0.0f, 1.0f,
};

// float vertices[] = {
//     32.0f, 32.0f, 0.0f, 1.0f, 1.0f,
//     32.0f, 0,  0.0f, 1.0f, 0.0f,
//     0,  0,  0.0f, 0.0f, 0.0f,
//     0,  32.0f, 0.0f, 0.0f, 1.0f,
// };

// float vertices[] = {
//     100.0f, 32.0f, 0.0f, 1.0f, 1.0f,
//     100.0f, 0,  0.0f, 1.0f, 0.0f,
//     0,  0,  0.0f, 0.0f, 0.0f,
//     0,  32.0f, 0.0f, 0.0f, 1.0f,
// };

unsigned int indices[] = {
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
};

void gfx_init(client_t* clt)
{
    gfx.client = clt;
    gfx.camera = camera_new();
    camera_init(gfx.camera, clt->window_width, clt->window_height);

    shader_tile_init();
    shader_tile_color_init();
    shader_nineslice_init();
    shader_tframe_init();
    shader_sbox_init();
    shader_texarr_init();
    shader_tilex_init();
    shader_tilexu_init();
    shader_line_init();

    texture_init();
    texarr_init(&gfx);
    picker_init(clt->window_width, clt->window_height);

    gfx_init_default_shader();
    // gfx_init_buffers();
    gfx_init_vao_empty();
    gfx_init_vao_main();
    gfx_init_vao_line();

    clt->gfx = &gfx;
}

void gfx_init_vao_empty()
{
    glCreateVertexArrays(1, &gfx.vao_empty);

}

void gfx_init_vao_main()
{
    // glCreateVertexArrays(1, &gfx.vao);
    glGenVertexArrays(1, &gfx.vao);
    glBindVertexArray(gfx.vao);

    glGenBuffers(1, &gfx.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, gfx.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &gfx.ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gfx.ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}

float vertexData[] = {
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
};

void gfx_init_vao_line()
{
    glCreateVertexArrays(1, &gfx.vao_line);
    glBindVertexArray(gfx.vao_line);

    glGenBuffers(1, &gfx.vbo_line);
    glBindBuffer(GL_ARRAY_BUFFER, gfx.vbo_line);
    glBufferData(GL_ARRAY_BUFFER, 10 * sizeof(float), vertexData, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
}

camera_t* gfx_get_camera(gfx_t* gfx)
{
    return gfx->camera;
}


void gfx_init_default_shader()
{
    GLint status;
    GLint length;

    const char* vs =
        "#version 330 core\n"
        "layout (location = 0) in vec3 vertex_position;\n"
        "layout (location = 1) in vec2 vertex_texcoord;\n"
        "uniform mat4 mvp;\n"
        "out vec2 texcoord;\n"
        "void main()\n"
        "{\n"
        "   texcoord.x = vertex_texcoord.x / 4;\n"
        "   texcoord.y = vertex_texcoord.y / 4;\n"
        "   gl_Position = mvp * vec4(vertex_position, 1.0);\n"
        "}\n";
    const char* fs =
        "#version 330 core\n"
        "in vec2 texcoord;\n"
        "uniform sampler2D texture_map;\n"
        "out vec4 output_color;\n"
        "void main()\n"
        "{\n"
        "   output_color = texture(texture_map, texcoord);\n"
        "}";

    gfx.prog = glCreateProgram();
    gfx.vs   = glCreateShader(GL_VERTEX_SHADER);
    gfx.fs   = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(gfx.vs, 1, &vs, 0);
    glShaderSource(gfx.fs, 1, &fs, 0);

    glCompileShader(gfx.vs);
    glCompileShader(gfx.fs);

    glGetShaderiv(gfx.vs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLchar* log;
        glGetShaderiv(gfx.vs, GL_INFO_LOG_LENGTH, &length);
        log = malloc(sizeof(GLchar) * length);

        glGetShaderInfoLog(gfx.vs, length, NULL, log);
        fprintf(stdout, "[GL]: failed to compile vertex shader: %s", log);

        glDeleteShader(gfx.vs);
        free(log);
        return;
    }

    glGetShaderiv(gfx.fs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLchar* log;
        glGetShaderiv(gfx.fs, GL_INFO_LOG_LENGTH, &length);
        log = malloc(sizeof(GLchar) * length);

        glGetShaderInfoLog(gfx.fs, length, NULL, log);
        fprintf(stdout, "[GL]: failed to compile fragment shader:\n %s", log);

        glDeleteShader(gfx.fs);
        free(log);
        return;
    }

    glAttachShader(gfx.prog, gfx.vs);
    glAttachShader(gfx.prog, gfx.fs);
    glLinkProgram(gfx.prog);
    glGetProgramiv(gfx.prog, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        fprintf(stdout, "[GL]: failed to link program.\n");
        return;
    }

    gfx.attr_vertex_position = glGetAttribLocation(gfx.prog, "vertex_position");
    gfx.unif_texture_map = glGetUniformLocation(gfx.prog, "texture_map");
    gfx.unif_mvp = glGetUniformLocation(gfx.prog, "mvp");
}

void gfx_use_default_shader()
{
    glUseProgram(gfx.prog);
}
