#include <stdio.h>

#include "stb_image.h"

#include "client/graphics/graphics.h"
#include "client/graphics/texture.h"
#include "client/graphics/texarr.h"

#define ATLAS_PATH "assets/textures/tilemap20.png"

GLuint texarr;
// static unsigned char data_converted[32 * 32 * 4 * 64] = { 255 };

void texarr_init(gfx_t* gfx)
{
    int width, height, channels;

	/////////////////////////////////////////////////////////////////
	// 1. Activate the texture unit we'll work with.
	/////////////////////////////////////////////////////////////////

	// int texture_unit = 5;  // For example.
	// glActiveTexture(GL_TEXTURE0 + texture_unit);

	/////////////////////////////////////////////////////////////////
	// 2. Load pixel data and hand it over to OpenGL into a 2d array.
	/////////////////////////////////////////////////////////////////

	unsigned char *data = NULL;
    data = stbi_load(ATLAS_PATH, &width, &height, &channels, 0);
	if (data == NULL) printf("[W] > Unable to load: `%s`.\n", ATLAS_PATH);
    unsigned char *data_converted = malloc(sizeof(unsigned char) * 32 * 32 * 4 * 64);
    if (data_converted == NULL) { printf("[w] Failed to malloc.\n"); return; }
    // for (size_t i = 0; i < width * height * channels; i++) {
	// 	data_converted[i] = 0;
	// }

	size_t p = 0;
	size_t n = 0;
    for (size_t y = 0; y < 8; y++) {
        for (size_t x = 0; x < 8; x++) {
            for (size_t j = 32 * y; j < (y + 1) * 32; j++) {
                for (size_t i = 32 * x; i < (x + 1) * 32; i++) {
                    for (size_t m = 0; m < 4; m++) {
                        // printf("> i, j := `%ld`\n", i * 4 + 4 * 32 * j + m);
                        // n = (t * 32) * 4 + (i) * 4 + 4 * 256 * (j) * 4 + m + f * 256 * 4;
                        n = i * 4 + j * 256 * 4 + m;
                        if (n >= 32 * 32 * 4 * 64) printf("[W] `!`\n");
                        // printf("> n = %ld\n", n);
                        data_converted[p] = data[n];
                        p++;
                    }
                }
            }
        }
    }

	// for (; n < 32 * 32 * 4 * 64; n++) {
	// 	data_converted[p] = 255;
	// }


	printf("[i] texarr: w=%d h=%d c=%d\n", width, height, channels);

	glGenTextures(1, &gfx->texarr);
	glBindTexture(GL_TEXTURE_2D_ARRAY, gfx->texarr);
    // printf("[i] Is texture `%d`\n", glIsTexture(gfx->texarr));
	// The `my_gl_format` represents your cpu-side channel layout.
	// Both GL_RGBA and GL_BGRA are common. See the "format" section
	// of this page: https://www.opengl.org/wiki/GLAPI/glTexImage3D

	// glTexImage3D(GL_TEXTURE_2D_ARRAY,
	//              1,                // mipmap level
	//              GL_RGBA8,         // gpu texel format
	//              32,               // width
	//              32,               // height
	//              64,               // depth
	//              0,                // border
	//              GL_RGBA,      	   // cpu pixel format
	//              GL_UNSIGNED_BYTE, // cpu pixel coord type
	//              data_converted);  // pixel data

    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 32, 32, 64);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					0,
					0,
					0,
					0,
					32,
					32,
					64,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					data_converted);

	/////////////////////////////////////////////////////////////////
	// 3. Mipmap and set up parameters for your texture.
	/////////////////////////////////////////////////////////////////

	// glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

	// These parameters have been chosen for Apanga, where I want a
	// pixelated appearance to the textures as they're magnified.

	// glTexParameteri(GL_TEXTURE_2D_ARRAY,
	//                 GL_TEXTURE_MIN_FILTER,
	//                 GL_NEAREST_MIPMAP_LINEAR);
	// glTexParameteri(GL_TEXTURE_2D_ARRAY,
	//                 GL_TEXTURE_MAG_FILTER,
	//                 GL_NEAREST);
	// // glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 1);
	// glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	// glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);


	/////////////////////////////////////////////////////////////////
	// 4. Connect this texture to our shader program.
	/////////////////////////////////////////////////////////////////

	// This assumes we've already set up the `program` shader.
	// GLuint sampler_loc = glGetUniformLocation(program, "my_sampler");
	// glUniform1i(sampler_loc, texture_unit);
    stbi_image_free(data);
    free(data_converted);
}

void texarr_init1()
{
	GLsizei width = 2;
	GLsizei height = 2;
	GLsizei layerCount = 2;
	GLsizei mipLevelCount = 1;

	// Read you texels here. In the current example, we have 2*2*2 = 8 texels, with each texel being 4 GLubytes.
	GLubyte texels[32] =
	{
	     // Texels for first image.
	     0,   0,   0,   255,
	     255, 0,   0,   255,
	     0,   255, 0,   255,
	     0,   0,   255, 255,
	     // Texels for second image.
	     255, 255, 255, 255,
	     255, 255,   0, 255,
	     0,   255, 255, 255,
	     255, 0,   255, 255,
	};

	glGenTextures(1,&texarr);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texarr);
	// Allocate the storage.
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGBA8, width, height, layerCount);
	// Upload pixel data.
	// The first 0 refers to the mipmap level (level 0, since there's only 1)
	// The following 2 zeroes refers to the x and y offsets in case you only want to specify a subrectangle.
	// The final 0 refers to the layer index offset (we start from index 0 and have 2 levels).
	// Altogether you can specify a 3D box subset of the overall texture, but only one mip level at a time.
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					0,
					0,
					0,
					0,
					width,
					height,
					layerCount,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					texels);

	// Always set reasonable texture parameters
	glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
}

// #version 330 core
//
// // Other global-level declarations.
//
// // The name of this variable must match the "my_sampler" string
// // given to `glGetUniformLocation` in the setup code above.
// uniform sampler2DArray my_sampler;
//
// void main() {
//   // Determine x, y, z used below.
//   vec4 texel = texture(my_sampler, vec3(x, y, z));
//   // Use texel and other magicks to compute the fragement color.
// }
