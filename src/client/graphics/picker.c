#include <stdlib.h>
#include <stdio.h>

#include "client/graphics/graphics.h"
#include "client/graphics/picker.h"

typedef struct picker_t
{
    GLuint fbo;
    GLuint texture;
    int w, h;
} picker_t;

picker_t picker;

void picker_init(int width, int height)
{
    picker.w = width;
    picker.h = height;

    glGenFramebuffers(1, &picker.fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, picker.fbo);

    // Create the texture object for the primitive information buffer
    glGenTextures(1, &picker.texture);
    glBindTexture(GL_TEXTURE_2D, picker.texture);
    // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, WINDOW_WIDTH, WINDOW_HEIGHT,
    //            0, GL_RGB, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32UI, picker.w, picker.h,
               0, GL_RGBA_INTEGER, GL_INT, NULL);
    // glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32I, width, height, depth, 0, GL_RGBA_INTEGER, GL_INT, NULL);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
               picker.texture, 0);

    // Create the texture object for the depth buffer
    // glGenTextures(1, picker.depth_texture);
    // glBindTexture(GL_TEXTURE_2D, picker.depth_texture);
    // glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, WINDOW_WIDTH, WINDOW_HEIGHT,
    //            0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    // glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
    //            picker.depth_texture, 0);

    // Disable reading to avoid problems with older GPUs
    // glReadBuffer(GL_NONE);

    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    // Verify that the FBO is correct
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (status != GL_FRAMEBUFFER_COMPLETE) {
       printf("FrameBuffer error, status: 0x%x\n", status);
       // return False;
    }

    // Restore the default framebuffer
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // return GLCheckError();
}

void picker_resize(int width, int height)
{
    picker.w = width;
    picker.h = height;

    glBindTexture(GL_TEXTURE_2D, picker.texture);
    /// Ошибка?: Повторный вызов `glTexImage2D` освобождает память
    /// предыдущей текстуры?
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32UI, width, height,
               0, GL_RGBA_INTEGER, GL_INT, NULL);
}

int picker_begin()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, picker.fbo);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    return true;
}


void picker_end()
{
    // LA2_UNUSED(picker);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void picker_read_pixel(unsigned int x, unsigned int y, unsigned int rgb[3])
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, picker.fbo);
    // glReadBuffer(GL_COLOR_ATTACHMENT0);
    glReadPixels(x, y, 1, 1, GL_RGB_INTEGER, GL_UNSIGNED_INT, rgb);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void picker_read_pixel_int(unsigned int x, unsigned int y)
{
    unsigned int rgb[3];

    glBindFramebuffer(GL_READ_FRAMEBUFFER, picker.fbo);
    // glReadBuffer(GL_COLOR_ATTACHMENT0);

    glReadPixels(x, y, 1, 1, GL_RGB_INTEGER, GL_UNSIGNED_INT, rgb);
    // printf("> Color (%d, %d): %d %d \n", x, y, rgb[0], rgb[1], rgb[2]);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

// int picker_get_id(unsigned int x, unsigned int y)
// {
//     unsigned int rgb[3];
//
//     glBindFramebuffer(GL_READ_FRAMEBUFFER, picker.fbo);
//     // glReadBuffer(GL_COLOR_ATTACHMENT0);
//
//     glReadPixels(x, y, 1, 1, GL_RGB_INTEGER, GL_UNSIGNED_INT, &rgb);
//     // printf("> Color (%d, %d): %d %d \n", x, y, rgb[0], rgb[1]);
//     glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
//
//     printf("?> %d \n", rgb[0]);
//     if (rgb[0] == PICKER_MAP_OBJ)
//         return rgb[1];
//     return 0;
// }

int picker_get_hovered_ij(int x, int y, int* i, int* j)
{
    if ((x >= picker.w) || (y >= picker.h) || (x < 0) || (y < 0))
        return PICKER_NONE;

    unsigned int rgb[3];
    picker_read_pixel(x, y, rgb);
    if (rgb[0] != PICKER_NONE)
    {
        *i = rgb[1];
        *j = rgb[2];
    }

    // printf(" >> %d \n", rgb[0]);
    return rgb[0]; // PICKER_NONE, PICKER_MAP_TILE, PICKER_LOGICAL_MAP_TILE, ...
}
