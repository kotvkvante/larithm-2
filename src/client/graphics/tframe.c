#include "client/client.h"
#include "client/state/state.h"
#include "client/graphics/graphics.h"
#include "client/graphics/tframe.h"
#include "client/graphics/texture.h"

// static float vertices[] = {
// 	/// right top corner
// 	0.5f, 0.5f, 1.0f, 1.0f, /// 0
// 	1.0f, 0.5f, 0.0f, 1.0f, /// 1
// 	1.0f, 1.0f, 0.0f, 0.0f, /// 2
// 	0.5f, 1.0f, 1.0f, 0.0f, /// 3
// 	/// left top corner
// 	-1.0f, 0.5f, 1.0f, 0.0f, /// 4
// 	-0.5f, 0.5f, 1.0f, 1.0f, /// 5
// 	-0.5f, 1.0f, 0.0f, 1.0f, /// 6
// 	-1.0f, 1.0f, 0.0f, 0.0f, /// 7
// 	/// left bottom corner
// 	-1.0f, -1.0f, 0.0f, 0.0f,
// 	-1.0f, -0.5f, 0.0f, 1.0f,
// 	-0.5f, -0.5f, 1.0f, 1.0f,
// 	-0.5f, -1.0f, 1.0f, 0.0f,
// 	/// right bottom corner
// 	0.5f, -1.0f, 0.0f, 1.0f,
// 	1.0f, -1.0f, 0.0f, 0.0f,
// 	1.0f, -0.5f, 1.0f, 0.0f,
// 	0.5f, -0.5f, 1.0f, 1.0f,
// 	/// right edge
// 	0.5f, -0.5f, 0.0f, 1.0f,
// 	1.0f, -0.5f, 0.0f, 0.0f,
// 	1.0f,  0.5f, 1.0f, 0.0f,
// 	0.5f,  0.5f, 1.0f, 1.0f,
// 	/// top edge
//
// };
//
// static unsigned int indices[] = {
// 	/// right top corner
// 	0,   1,  2,
// 	0,   2,  3,
// 	/// left top corner
// 	4,   5,  6,
// 	4,   6,  7,
// 	/// left bottom corner
// 	8,   9, 10,
// 	8,  10, 11,
// 	/// right bottom corner
// 	12, 13, 14,
// 	12, 14, 15,
// 	/// right edge
// 	16, 17, 18,
// 	16, 18, 19,
// 	/// top edge
// };

// void tframe_init(gfx_t* gfx)
// {
// 	// sd_game_t* sdata = (sd_game_t*)(gfx->client->sdata);
//
//     // glGenVertexArrays(1, &sdata->tf_vao);
//     // glBindVertexArray(sdata->tf_vao);
// 	//
//     // glGenBuffers(1, &sdata->tf_vbo);
//     // glBindBuffer(GL_ARRAY_BUFFER, sdata->tf_vbo);
//     // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
// 	//
//     // glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
//     // glEnableVertexAttribArray(0);
// 	//
//     // glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
//     // glEnableVertexAttribArray(1);
// 	//
//     // glGenBuffers(1, &sdata->tf_ebo);
// 	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sdata->tf_ebo);
//     // glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
// 	// printf(">> %ld \n", sizeof(indices));
// }

void tframe_init_default(tframe_t* tf, size_t w, size_t h)
{
	tf->w = w;
	tf->h = h;

	tf->corner = TEXTURE_TFRAME_CORNER;
	tf->border = TEXTURE_TFRAME_BORDER;
	tf->background = TEXTURE_TFRAME_BACKGROUND;
}

void tframe_init(tframe_t* tf, size_t w, size_t h, size_t c, size_t b, size_t bg)
{
	tf->w = w;
	tf->h = h;
	tf->corner = c;
	tf->border = b;
	tf->background = bg;
}
