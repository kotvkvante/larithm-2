#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "shader_template.h"

/// shader tile for rendering textured tiles
SHADER_DEFAULT_TYPE(shader_tile)
    SHADER_TYPE_UNIFORM(tile_size);
    SHADER_TYPE_UNIFORM(tile_offset);
    SHADER_TYPE_UNIFORM(tile_texture);
    SHADER_TYPE_UNIFORM(texture_map);
    SHADER_TYPE_UNIFORM(mvp);
SHADER_TYPE_END(shader_tile);
SHADER_INIT_DECLARE(shader_tile);
SHADER_USE_DECLARE(shader_tile);

/// shader tile color for rendering solid color tiles (for picker)
SHADER_DEFAULT_TYPE(shader_tile_color)
    SHADER_TYPE_UNIFORM(tile_size);
    SHADER_TYPE_UNIFORM(tile_offset);
    SHADER_TYPE_UNIFORM(tile_color);
    SHADER_TYPE_UNIFORM(mvp);
SHADER_TYPE_END(shader_tile_color);
SHADER_INIT_DECLARE(shader_tile_color);
SHADER_USE_DECLARE(shader_tile_color);

/// shader for nineslice textures
SHADER_DEFAULT_TYPE(shader_nineslice)
    // SHADER_TYPE_UNIFORM(tile_size);
    // SHADER_TYPE_UNIFORM(tile_offset);
    // SHADER_TYPE_UNIFORM(mvp);
    // SHADER_TYPE_UNIFORM(tile_texture);
    // SHADER_TYPE_UNIFORM(texture_map);
    SHADER_TYPE_UNIFORM(u_dimensions);
    SHADER_TYPE_UNIFORM(u_border);
SHADER_TYPE_END(shader_nineslice);
SHADER_INIT_DECLARE(shader_nineslice);
SHADER_USE_DECLARE(shader_nineslice);

/// shader for tframe [tile frame]
SHADER_DEFAULT_TYPE(shader_tframe)
    SHADER_FIELD(GL_GEOMETRY_SHADER);
    SHADER_TYPE_UNIFORM(mvp);
    SHADER_TYPE_UNIFORM(rect);
    SHADER_TYPE_UNIFORM(corner);
    SHADER_TYPE_UNIFORM(border);
    SHADER_TYPE_UNIFORM(background);
    SHADER_TYPE_UNIFORM(width);
    SHADER_TYPE_UNIFORM(height);
    SHADER_TYPE_UNIFORM(atlas);
SHADER_TYPE_END(shader_tframe);
SHADER_INIT_DECLARE(shader_tframe);
SHADER_USE_DECLARE(shader_tframe);

/// shader for sbox
SHADER_DEFAULT_TYPE(shader_sbox)
    SHADER_FIELD(GL_GEOMETRY_SHADER);
    SHADER_TYPE_UNIFORM(mvp);
    SHADER_TYPE_UNIFORM(rect);
    // SHADER_TYPE_UNIFORM(w);
    // SHADER_TYPE_UNIFORM(h);
SHADER_TYPE_END(shader_sbox);
SHADER_INIT_DECLARE(shader_sbox);
SHADER_USE_DECLARE(shader_sbox);

/// shader for texarr := `texture array`
SHADER_DEFAULT_TYPE(shader_texarr)
    SHADER_TYPE_UNIFORM(layer);
    SHADER_TYPE_UNIFORM(mvp);
SHADER_TYPE_END(shader_texarr);
SHADER_INIT_DECLARE(shader_texarr);
SHADER_USE_DECLARE(shader_texarr);

/// shader for tilex := `tile extended`
SHADER_DEFAULT_TYPE(shader_tilex)
    SHADER_FIELD(GL_GEOMETRY_SHADER);
    SHADER_TYPE_UNIFORM(wh);
    SHADER_TYPE_UNIFORM(mvp);
SHADER_TYPE_END(shader_tilex);
SHADER_INIT_DECLARE(shader_tilex);
SHADER_USE_DECLARE(shader_tilex);

#define TXU_NOSTYLE    (0)        /// 0000
#define TXU_GRAYSCALE  (1 << 0)   /// 1000
#define TXU_APPLYRED   (1 << 1)	  /// 0100
#define TXU_APPLYCOLOR (1 << 2)	  /// 0010 0000 0000 0000

/// shader for tilexu := `tile extended uniform`
SHADER_DEFAULT_TYPE(shader_tilexu)
    SHADER_FIELD(GL_GEOMETRY_SHADER);
    /// vertex shader
    SHADER_TYPE_UNIFORM(mvp);
    SHADER_TYPE_UNIFORM(position);
    /// geometry shader
    SHADER_TYPE_UNIFORM(wh);
    SHADER_TYPE_UNIFORM(texture);
    SHADER_TYPE_UNIFORM(transform);
    /// fragment shader
    SHADER_TYPE_UNIFORM(style);
SHADER_TYPE_END(shader_tilexu);
SHADER_INIT_DECLARE(shader_tilexu);
SHADER_USE_DECLARE(shader_tilexu);

/// shader for line
SHADER_DEFAULT_TYPE(shader_line)
    /// vertex shader
    SHADER_TYPE_UNIFORM(mvp);
SHADER_TYPE_END(shader_line);

SHADER_INIT_DECLARE(shader_line);
SHADER_USE_DECLARE(shader_line);

#endif /* end of include guard: SHADER_H */
