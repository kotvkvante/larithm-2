#ifndef GRAPHICS_H
#define GRAPHICS_H

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 800

#define GAME_TILE_SIZE 64.0f

#include <stdbool.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utils/utils.h"

typedef struct client_t client_t;
typedef struct gfx_t gfx_t;
typedef struct map_t map_t;
typedef struct camera_t camera_t;
typedef struct game_t game_t;
typedef struct lvlist_t lvlist_t;

typedef struct gfx_t
{
    client_t* client;
    GLuint vs;
    GLuint fs;
    GLuint prog;
    GLint attr_vertex_position;
    GLint unif_texture_map;
    GLint unif_mvp;

    GLuint vao_empty;

    // main vao
    GLuint vao;
    GLuint vbo;
    GLuint ebo;

    GLuint vbo_line;
    GLuint vao_line;

    camera_t* camera;

    GLuint texarr;  /// 2d array texture for atlas
} gfx_t;

void gfx_init(client_t* client);
void gfx_init_buffers();

void gfx_draw(client_t* client);

camera_t* gfx_get_camera(gfx_t* gfx);

void gfx_init_vao_empty();
void gfx_init_vao_main();
void gfx_init_vao_line();


void gfx_init_default_shader();
void gfx_use_default_shader();

#endif /* end of include guard: GRAPHICS_H */
