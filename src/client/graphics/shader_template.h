#ifndef SHADER_TEMPLATE_H
#define SHADER_TEMPLATE_H

#define SHADER_FIELD(shader_type) \
    GLuint shader_type##_st

#define SHADER_TYPE(shader_name) \
typedef struct shader_name##_t \
{ \
    GLuint id;

#define SHADER_EMPTY_TYPE(shader_name) \
    SHADER_TYPE(shader_name) \


#define SHADER_DEFAULT_TYPE(shader_name) \
    SHADER_TYPE(shader_name) \
    SHADER_FIELD(GL_VERTEX_SHADER); \
    SHADER_FIELD(GL_FRAGMENT_SHADER);

#define SU(uniform_name) unif_##uniform_name
/* #define ATTR(attr_name) attr_##attr_name */

#define SHADER_TYPE_UNIFORM(uniform_name) \
    GLint SU(uniform_name)

/* #define SHADER_TYPE_ATTRIBUTE(attribute_name) \
     GLint ATTR(attribute_name) */

#define SHADER_TYPE_END(shader_name) \
} shader_name##_t

#define SHADER_DECLARE(shader_name) \
    shader_name##_t shader_name

#define SHADER_INIT_DECLARE(shader_name) \
void shader_name##_init()

#define SHADER_USE_DECLARE(shader_name) \
void shader_name##_use()

#define SHADER_SOURCE(shader_name, shader_type, filename) \
    do { \
    char* spath = "assets/shaders/" filename; /* shader path */ \
    char* ssource = shader_read_file(spath);  /* shader source */ \
    shader_name.shader_type##_st = glCreateShader(shader_type); \
    glShaderSource(shader_name.shader_type##_st, 1, (const GLchar* const*)(&ssource), 0); \
    glCompileShader(shader_name.shader_type##_st); \
    SHADER_CHECK_COMPILE_STATUS(shader_name.shader_type##_st, spath); \
    glAttachShader(shader_name.id, shader_name.shader_type##_st); \
    free(ssource); \
    } while (0)

#define SHADER_INIT(shader_name) \
SHADER_INIT_DECLARE(shader_name) \
{ \
    GLint status; \
    shader_name.id = glCreateProgram();
    /* char* vs_path = "assets/shaders/" vert_filename; \
    char* fs_path = "assets/shaders/" frag_filename; \
    char* vs_source = shader_read_file(vs_path); \
    char* fs_source = shader_read_file(fs_path); \
    shader_name.vs = glCreateShader(GL_VERTEX_SHADER); \
    shader_name.fs = glCreateShader(GL_FRAGMENT_SHADER); \
    glShaderSource(shader_name.vs, 1, (const GLchar* const*)(&vs_source), 0); \
    glShaderSource(shader_name.fs, 1, (const GLchar* const*)(&fs_source), 0); \
    glCompileShader(shader_name.vs); \
    glCompileShader(shader_name.fs); \
    SHADER_CHECK_COMPILE_STATUS(shader_name.vs, vs_path); \
    SHADER_CHECK_COMPILE_STATUS(shader_name.fs, fs_path); \
    glAttachShader(shader_name.id, shader_name.vs); \
    glAttachShader(shader_name.id, shader_name.fs); */


#define SHADER_LINK(shader_name) \
    do { \
    glLinkProgram(shader_name.id); \
    SHADER_CHECK_LINK_STATUS(shader_name.id); \
    } while (0)


#define SHADER_INIT_UNIFORM(shader_name, uniform_name) \
    shader_name.unif_##uniform_name = glGetUniformLocation(shader_name.id, #uniform_name ); \
    if (shader_name.unif_##uniform_name < 0) \
    { printf("[W] Failed to get uniform location in `%s`: %s\n", #shader_name, #uniform_name); }

#define SHADER_INIT_END(shader_name) \
    printf("[i] Shader init complite: %s\n", #shader_name); \
}

#define SHADER_USE(shader_name) \
void shader_name##_use() \
{ glUseProgram(shader_name.id); }


#define SHADER_CHECK_COMPILE_STATUS(s, filepath) \
    glGetShaderiv(s, GL_COMPILE_STATUS, &status); \
    if (status != GL_TRUE) \
    { \
        shader_print_log(s, filepath); \
        exit(-1); \
    }

#define SHADER_CHECK_LINK_STATUS(id) \
    glGetProgramiv(id, GL_LINK_STATUS, &status); \
    if (status != GL_TRUE) \
    { \
        fprintf(stdout, "[GL]: failed to link program.\n"); \
        exit(-1); \
    }

#endif /* end of include guard: SHADER_TEMPLATE_H */
