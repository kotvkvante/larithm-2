#ifndef LA2_TFRAME_H
#define LA2_TFRAME_H

#include <stdlib.h>

/// tile frame
typedef struct tframe_t
{
	int corner;
	int border;
	int background;
	size_t w, h;
} tframe_t;

void tframe_init(tframe_t* tf, size_t w, size_t h, size_t c, size_t b, size_t bg);
void tframe_init_default(tframe_t* tf, size_t w, size_t h);

#endif /* end of include guard: LA2_TFRAME_H */
