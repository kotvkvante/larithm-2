#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

// #define WINDOW_WIDTH 1200
// #define WINDOW_HEIGHT 800

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_KEYSTATE_BASED_INPUT
#include "nuklear.h"
#include "client/nk_glfw_gl4.h"

#include "client/client.h"
#include "client/callbacks.h"
#include "client/state/main/state.h"
#include "client/state/choose_level/state.h"
#include "client/state/game/state.h"

#include "client/graphics/graphics.h"
#include "common/level/levels_list.h"

static void clt_init_glfw(client_t* clt);
static void clt_init_nk(client_t* clt);

void clt_parse_args(client_t* clt, int argc, char const *argv[])
{
    int opt;
    while ((opt = getopt(argc, argv, "dlw")) != -1) {
        switch (opt) {
        case 'd':
            printf("[i] Client Debug Mode.\n");
            clt->debug = true;
            break;
        case 'l': printf("l\n"); break;
        case 'w': printf("w\n"); break;
        default:
            fprintf(stderr, "Usage: %s [-dlw] [file...]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }
}

void clt_init(client_t* clt)
{
    clt_init_glfw(clt);
    clt_init_nk(clt);
    gfx_init(clt);

    if (clt->debug)
    {
        stt_choose_level_entry(clt);
        stt_choose_level_pass_data(clt);
        stt_choose_level_leave(clt);

        stt_game_entry(clt);
    }
    else
    {
        stt_main_entry(clt);
    }
}

static void clt_init_glfw(client_t* clt)
{
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
    {
        // ERROR
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    printf("[i] mode w, h = %d %d.\n", mode->width, mode->height);
    // int width = mode->width - 400;


    clt->window = glfwCreateWindow(mode->width, mode->height, "larithm 2", NULL, NULL);
    // clt->window = glfwCreateWindow(800, 1200, "larithm 2", NULL, NULL);
    // clt->window = glfwCreateWindow(mode->width, mode->height, "larithm 2", glfwGetPrimaryMonitor(), NULL);
    glfwMakeContextCurrent(clt->window);

    // clt->window_width  = mode->width;
    // clt->window_height = mode->height;
    glfwGetWindowSize(clt->window, &clt->window_width, &clt->window_height);
    printf("[i] window w, h = %d %d\n", clt->window_width, clt->window_height);

    glViewport(0, 0, clt->window_width, clt->window_height);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "Failed to setup GLEW\n");
        exit(1);
    }
}

static void clt_init_nk(client_t* clt)
{
    // Нужно сделать собственную обработку обратных вызовов.
    clt->ctx = nk_glfw3_init(clt->window,
        NK_GLFW3_INSTALL_CALLBACKS, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);

    glfwSetWindowSizeCallback(clt->window, window_size_callback);
    glfwSetWindowUserPointer(clt->window, (void*)clt);

    {
        struct nk_font_atlas *atlas;
        nk_glfw3_font_stash_begin(&atlas);
        struct nk_font *font = nk_font_atlas_add_from_file(atlas, "assets/fonts/consolas.ttf", 20, 0);
        nk_glfw3_font_stash_end();
        nk_style_set_font(clt->ctx, &font->handle);
    }
}

void clt_handle_events(client_t* clt)
{
    clt->handle_events(clt);
}

void clt_update(client_t* clt)
{
    clt->update(clt);
}

void clt_draw(client_t* clt)
{
    clt->draw(clt);
}

void clt_stop(client_t* clt)
{
    glfwSetWindowShouldClose(clt->window, true);
}

bool clt_should_stop(client_t* clt)
{
    return glfwWindowShouldClose(clt->window);
}

void clt_set_window_size(client_t* clt, int w, int h)
{
    clt->window_width  = w;
    clt->window_height = h;
}

void clt_close(client_t* clt)
{
    LA2_UNUSED(clt);
    //
}
