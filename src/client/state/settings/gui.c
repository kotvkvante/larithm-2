#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STATE_SETTINGS
#include "client/state/state.h"
#include "client/state/settings/state.h"
#include "client/state/settings/gui.h"
#include "client/state/main/state.h"

#include "client/graphics/gui_panel.h"

void stt_settings_gui(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;

    if (nk_begin(ctx, "Settings", nk_rect(20, 20, 250, 300),
        LG_DEFAULT_WINDOW))
    {
        nk_layout_row_dynamic(ctx, 30, 2);
        if (nk_button_label(ctx, "Music Off"))
        {

        }
        if (nk_button_label(ctx, "Music On"))
        {

        }
        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "Disable Animations"))
        {

        }
        if (nk_button_label(ctx, "Back"))
        {
            stt_settings_leave(clt);
            stt_main_entry(clt);
        }

    } nk_end(ctx);
}
