#ifndef STATE_SETTINGS_H
#define STATE_SETTINGS_H

typedef struct client_t client_t;

void stt_settings_entry(client_t* clt);
void stt_settings_leave(client_t* clt);

#endif /* end of include guard: STATE_SETTINGS_H */
