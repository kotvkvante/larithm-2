#include <stdlib.h>
#include <stdio.h>

// #define NK_INCLUDE_FIXED_TYPES
// #define NK_INCLUDE_STANDARD_IO
// #define NK_INCLUDE_STANDARD_VARARGS
// #define NK_INCLUDE_DEFAULT_ALLOCATOR
// #define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
// #define NK_INCLUDE_FONT_BAKING
// #define NK_INCLUDE_DEFAULT_FONT
// #define NK_KEYSTATE_BASED_INPUT
// #include "nuklear.h"
#include "client/nk_glfw_gl4.h"

#include "client/client.h"
#define STT_DEFAULT_DECL
#include "client/state/state.h"
#include "client/state/settings/state.h"
#include "client/state/settings/gui.h"


#include "client/graphics/gui.h"
#include "client/graphics/graphics.h"

void stt_settings_entry(client_t* clt)
{
    clt->draw = draw_;
    clt->update = update_;
    clt->handle_events = handle_events_;
}

static void draw_(client_t* clt)
{
    glEnable(GL_BLEND);
    // glfwGetWindowSize(win, &width, &height);
    glViewport(0, 0, clt->window_width, clt->window_height);
    glClearColor(0.10f, 0.18f, 0.24f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    nk_glfw3_render(NK_ANTI_ALIASING_ON);

    glfwSwapBuffers(clt->window);
}

static void update_(client_t* clt)
{
    nk_glfw3_new_frame();
    stt_settings_gui(clt);
}

static void handle_events_(client_t* clt)
{
    LA2_UNUSED(clt);
    glfwPollEvents();
}

void stt_settings_leave(client_t* clt)
{
    LA2_UNUSED(clt);
}
