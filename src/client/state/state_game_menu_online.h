#ifndef STATE_GAME_MENU_ONLINE_H
#define STATE_GAME_MENU_ONLINE_H

typedef struct client_t client_t;

void stt_choose_level_online_entry(client_t* clt);
void stt_choose_level_online_leave(client_t* clt);

#endif /* end of include guard: STATE_GAME_MENU_ONLINE_H */
