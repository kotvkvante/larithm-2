#include <stdio.h>

#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STATE_GAME
#include "client/state/state.h"
#include "client/state/game/state.h"
#include "client/state/game/gui.h"

#include "client/state/main/state.h"
#include "client/state/choose_level/state.h"

#include "client/graphics/gui_panel.h"

#include "common/level/levels_list.h"
#include "common/game/game.h"
#include "common/map/logical_map.h"
#include "common/map/map.h"
#include "common/player/player.h"
#include "common/player/sqnm.h"
#include "common/objs/base.h"
#include "common/objs/lever.h"


static void panel_game(client_t* clt);
static void panel_menu(client_t* clt);
static void panel_tile(client_t* clt);
static void panel_debug(client_t* clt);
static void panel_sqnm(client_t* clt);
static bool gui_game_leave_confirm(client_t* clt);


void gui_game(client_t* clt)
{
    sd_game_t* sdata = (sd_game_t*)clt->sdata;

    if (sdata->leave_confirm) {
        if (gui_game_leave_confirm(clt)) return;
    }
    panel_game(clt);
    panel_menu(clt);
    panel_tile(clt);
    panel_debug(clt);
    panel_sqnm(clt);
}


static void panel_game(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_game_t* sdata = (sd_game_t*)clt->sdata;
    game_t* game = sdata->game;
    map_t* map = game_get_map(game);

    if (nk_begin(ctx, "Game", LG_GAME_PANEL, LG_DEFAULT_WINDOW))
    {
        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "Compute"))
        {
            game_compute(game);
            if (game_check_win(game))
            {
                printf("You Win!\n");
            }
            else
            {
                printf("Hmm something is wrong, try again.\n");
                game_compute_reverse(game);
            }
        }

        if (nk_button_label(ctx, "Next"))
        {
            if (!game_step_next(game)) printf("\a");
            for (size_t i = 0; i < game->players_cnt; i++) {
                player_t* p = &game->players_list[i];
                sqn_print(player_get_sqn(p));
            }
        }

        if (nk_button_label(ctx, "Prev"))
        {
            if (!game_step_prev(game)) printf("\a");
            for (size_t i = 1; i < game->players_cnt; i++) {
                player_t* p = &game->players_list[i];
                sqn_print(player_get_sqn(p));
            }
        }

        nk_value_int(ctx, "Score", map->score);
        nk_value_int(ctx, "Score Goal", map->score_goal);

    } nk_end(ctx);
}

static void panel_menu(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_game_t* sdata = (sd_game_t*)clt->sdata;

    struct nk_style *s = &ctx->style;
    nk_style_push_color(ctx, &s->window.background, nk_rgba(1,1,1,1));
    nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_color(nk_rgba(1,1,1,1)));

	if (nk_begin(ctx, "Menu", sdata->lg_menu, NK_WINDOW_NO_SCROLLBAR))
    {
        nk_layout_row_dynamic(ctx, 50, 1);
        if (nk_button_label(ctx, "Menu"))
        {
            sdata->leave_confirm = true;
        }
    }
    nk_end(ctx);

    nk_style_pop_color(ctx);
    nk_style_pop_style_item(ctx);
}

static void panel_tile(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_game_t* sdata = (sd_game_t*)clt->sdata;
    game_t* game = sdata->game;
	map_t* map = game_get_map(game);
    logical_map_t* lmap = game_get_lmap(game);

	if (nk_begin(ctx, "Tile Info", sdata->lg_tileinfo, LG_DEFAULT_WINDOW))
	{
		static char buf1[128] = "";
		static char buf2[128] = "";
		static char buf3[128] = "";
		static char buf4[128] = "";

        map_get_stile_info(map, buf1);
		map_get_htile_info(map, buf2);
		lmap_get_stile_info(lmap, buf3);
		lmap_get_htile_info(lmap, buf4);

		nk_layout_row_dynamic(ctx, 50, 1);
		nk_label_wrap(ctx, buf1);
		nk_label_wrap(ctx, buf2);
		nk_label_wrap(ctx, buf3);
        nk_label_wrap(ctx, buf4);
	} nk_end(ctx);
}

static void panel_debug(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_game_t* sdata = (sd_game_t*)clt->sdata;
    game_t* game = sdata->game;
	map_t*  map  = game_get_map(game);
    logical_map_t* lmap = game_get_lmap(game);

    if (nk_begin(ctx, "Debug", sdata->gpanel_debug,
                 LG_DEFAULT_WINDOW | NK_WINDOW_MINIMIZABLE))
    {
        static char text[2][16];
        static int text_len[2];

        nk_layout_row_dynamic(ctx, 30, 2);

        nk_label(ctx, "Pixel X:", NK_TEXT_LEFT);
        nk_edit_string(ctx, NK_EDIT_FIELD, text[0], &text_len[0], 16, nk_filter_decimal);
        nk_label(ctx, "Pixel Y:", NK_TEXT_LEFT);
        nk_edit_string(ctx, NK_EDIT_FIELD, text[1], &text_len[1], 16, nk_filter_decimal);


        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "Info players"))
        {
            for (size_t i = 0; i < game->players_cnt; i++)
            {
                sqn_t* s = player_get_sqn(&game->players_list[i]);
                sqn_print(s);
            }
        }

        if (nk_button_label(ctx, "Turn Lever"))
        {
            obj_base_t* obj = map_get_sobj(map);
            if (obj != MAP_NULL_OBJ)
                if (map_is_tile_interactable(map, obj->x, obj->y))
                {
                    map_tile_interact(map, obj->x, obj->y);
                    game_set_need_sync(game);
                }
        }

        if (nk_button_label(ctx, "Info lever"))
        {
            obj_base_t* obj = map_get_sobj(map);
            if (obj != MAP_NULL_OBJ)
                if (obj->type == OBJ_TYPE_LEVER)
                    obj_lever_print_info((obj_lever_t*)obj);
        }

        if (nk_button_label(ctx, "Info Elem"))
        {
            elem_base_t* elem = lmap_get_selected_elem(lmap);
            if (elem != LOGICAL_MAP_NULL_ELEM)
            {
                size_t index = lmap_get_elem_index(lmap, elem);
                printf(">[%ld] `%s`.\n",index, elem_get_name(elem));

                edgenode_t* node = lmap->g.edges[index];
                while (node != NULL)
                {
                    elem_base_t* elem = lmap->elements_list[node->y];
                    printf("\t [%d] -> `%s`.\n", node->y, elem_get_name(elem));
                    node = node->next;
                }
            }
        }

        if (nk_button_label(ctx, "Exit"))
        {
            glfwSetWindowShouldClose(clt->window, True);
        }


    } nk_end(ctx);
}

static void panel_sqnm(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    game_t* game = ((sd_game_t*)clt->sdata)->game;
    player_t*  p = game_get_current_player(game);
    sqn_t*   sqn = player_get_sqn(p);
    sqnm_t*   sm = game->sqnm;

    if (nk_begin(ctx, "Sqn Menu", LG_SQNM_PANEL,
                 LG_DEFAULT_WINDOW | NK_WINDOW_MINIMIZABLE))
    {
        size_t size = 0;
        action_t* a = sqnm_get_slcd_act(sm);
        if (a) size = sm->handler(clt, a);
        else nk_widget_disable_begin(ctx);

        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "button"))
        {
            sqn_insert_data_safe(sqn, a->action, sqn->act_next, a->data, size);
            sqn->act_next++; /// correction
            sqn_print(sqn);
        }

        nk_widget_disable_end(ctx);

        // nk_layout_row_dynamic(ctx, 30, 1);

    } nk_end(ctx);
}

static bool gui_game_leave_confirm(client_t* clt)
{
    #define BUTTON_HEIGHT 30
    #define VSPACE_SKIP 100

    bool res = false;
    struct nk_context* ctx = clt->ctx;
    sd_game_t* sdata = (sd_game_t*)clt->sdata;

    struct nk_style *s = &ctx->style;
    nk_style_push_color(ctx, &s->window.background, nk_rgba(1,1,1,1));
    nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_color(nk_rgba(1,1,1,1)));

    if (nk_begin(ctx, "Leave ?",
        // NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_BORDER))
        // nk_rect(0, 0, 800, 800),
        nk_rect(0, 0, clt->window_width, clt->window_height),
        0))
    {
        /* 0.2 are a space skip on button's left and right, 0.6 - size of the button */
        static const float ratio[] = {0.42f, 0.16f, 0.42f};  /* 0.2 + 0.6 + 0.2 = 1 */

        /* Just make vertical skip with calculated height of static row  */
        nk_layout_row_static(ctx, (clt->window_height - (BUTTON_HEIGHT+VSPACE_SKIP)*2 )/2, 15, 2);
        nk_layout_row(ctx, NK_DYNAMIC, BUTTON_HEIGHT, 3, ratio);


        nk_spacing(ctx, 1); /* skip 0.2 left */
        nk_label(ctx, "Leave Level?", NK_TEXT_CENTERED);
        nk_spacing(ctx, 1); /* skip 0.2 left */

        nk_spacing(ctx, 1); /* skip 0.2 left */
        if (nk_button_label(ctx, "Yes"))
        {
            stt_game_leave(clt);
            stt_main_entry(clt);
            res = true;
        }
        nk_spacing(ctx, 1); /* skip 0.2 left */
        nk_spacing(ctx, 1); /* skip 0.2 right */
        if (nk_button_label(ctx, "No"))
        {
            sdata->leave_confirm = false;
        }
        nk_spacing(ctx, 1); /* skip 0.2 right */

    } nk_end(ctx);

    nk_style_pop_color(ctx);
    nk_style_pop_style_item(ctx);

    return res;
}
