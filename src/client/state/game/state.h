#ifndef STATE_GAME_H
#define STATE_GAME_H

typedef struct client_t client_t;

void stt_game_entry(client_t* clt);
void stt_game_leave(client_t* clt);

#endif /* end of include guard: STATE_GAME_H */
