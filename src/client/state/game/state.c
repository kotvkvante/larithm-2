#include <stdlib.h>
#include <stdio.h>

// #define NK_INCLUDE_FIXED_TYPES
// #define NK_INCLUDE_STANDARD_IO
// #define NK_INCLUDE_STANDARD_VARARGS
// #define NK_INCLUDE_DEFAULT_ALLOCATOR
// #define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
// #define NK_INCLUDE_FONT_BAKING
// #define NK_INCLUDE_DEFAULT_FONT
// #define NK_KEYSTATE_BASED_INPUT
// #include "nuklear.h"
#include "client/nk_glfw_gl4.h"


#include "client/client.h"
#define STT_DEFAULT_DECL
#define STATE_GAME
#include "client/state/state.h"
#include "client/state/game/state.h"
#include "client/state/game/gui.h"

#include "client/graphics/graphics.h"
#include "client/graphics/gfx/gfx.h"
#include "client/graphics/picker.h"
#include "client/graphics/gui.h"
#include "client/graphics/tframe.h"
#include "client/graphics/sbox.h"
#include "client/graphics/gwrap/gw_sqnm.h"

#include "common/level/levels_list.h"
#include "common/game/game.h"
#include "common/map/logical_map.h"
#include "common/map/map.h"
#include "common/player/player.h"
#include "common/player/sqnm.h"

static sd_game_t sdata = {
    0,
    .debug1 = 0,
};

static void key_callback_(GLFWwindow* window, int key, int scancode, int action, int mods);

/// Нужно: Нужна ли эта функция?
static void sdata_init_default_()
{
    sdata.leave_confirm = false;
    sdata.game = NULL;
}

void stt_game_entry(client_t* clt)
{
    lvlem_t* lvlem = (lvlem_t*)clt->pdata;
    if (lvlem == NULL)
    {
        printf("> [E] No level passed...\n");
        exit(-1);
    };

    clt->draw = draw_;
    clt->handle_events = handle_events_;
    clt->update = update_;

    sdata_init_default_();
    clt->sdata = &sdata;

    game_t* game = game_new();
        game_init_from_file(game, lvlem->fname);
    sdata.game = game;

    sdata.gw_game.game = game;
    gw_game_init(clt->gfx, &sdata.gw_game);

    sdata.lg_menu = (struct nk_rect){
        .h = 70,
        .w = 70,
        .x = clt->window_width - 70 - 20,
        .y = 20
    };

    sdata.lg_tileinfo = (struct nk_rect) {
        .h = 250,
        .w = 200,
        .x = clt->window_width - 200 - 20,
        .y = clt->window_height - 250 - 20,
    };

    sdata.gpanel_debug = (struct nk_rect) {
        .h = 300,
        .w = 200,
        .x = sdata.lg_tileinfo.x,
        .y = sdata.lg_tileinfo.y - 250 - 20,
    };

    // sbox_init(clt->gfx);

    // gw_tilex_init(clt->gfx);
    glfwSetKeyCallback(clt->window, key_callback_);
}


static void key_callback_(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    LA2_UNUSED(window);
    LA2_UNUSED(scancode);
    LA2_UNUSED(mods);

    game_t* game = sdata.game;

    if (action == GLFW_PRESS)
    {
        switch (key)
        {
            case GLFW_KEY_I:
            {
                sdata.debug1++;
            } break;

            case GLFW_KEY_O:
            {
                sdata.debug1--;
            } break;

            case GLFW_KEY_LEFT:
            {
                player_t* cp = game_get_current_player(game);
                sqn_t* csqn = player_get_sqn(cp);
                sqn_dec_safe(csqn);
            } break;

            case GLFW_KEY_RIGHT:
            {
                player_t* cp = game_get_current_player(game);
                sqn_t* csqn = player_get_sqn(cp);
                sqn_inc_safe(csqn);
            } break;

            case GLFW_KEY_UP:
                game_prev_player(game);
                break;

            case GLFW_KEY_DOWN:
                game_next_player(game);
                break;

            case GLFW_KEY_BACKSPACE:
            {
                player_t* cp = game_get_current_player(game);
                sqn_t* csqn = player_get_sqn(cp);
                sqn_remove(csqn, csqn->act_next-1);
                sqn_dec_safe(csqn);
            } break;

            case GLFW_KEY_DELETE:
            {
                player_t* cp = game_get_current_player(game);
                sqn_t* csqn = player_get_sqn(cp);
                sqn_remove(csqn, csqn->act_next);
            } break;
//            case GLFW_KEY_ESCAPE:
//                clt_stop(clt);
//                break;
        }
    }

    return;
}


static void handle_events_(client_t* clt)
{
    LA2_UNUSED(clt);
    glfwPollEvents();
}

static void gui_(client_t* clt)
{
    nk_glfw3_new_frame();
    gui_game(clt);
}

static void update_(client_t* clt)
{
    gui_(clt);

    sd_game_t* sdata = ((sd_game_t*)clt->sdata);
    /// no interraction with game..
    if (sdata->leave_confirm) return;

    int i, j;
    double x, y;
    GLFWwindow*    win  = clt->window;
    game_t*        game = sdata->game;
    logical_map_t* lmap = game_get_lmap(game);
    map_t*         map  = game_get_map(game);
    gw_game_t*  gw_game = &sdata->gw_game;
    gw_sqnm_t*  gw_sqnm = &gw_game->gw_sqnm;

    if (game_get_need_sync(game)) game_sync_maps(game);

    bool leftpress = (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
    bool leftclick = nk_input_is_mouse_released(&clt->ctx->input, NK_BUTTON_LEFT);
    bool escpress  = (glfwGetKey(win, GLFW_KEY_ESCAPE) == GLFW_PRESS);

    glfwGetCursorPos(clt->window, &x, &y);

    int picker_id = picker_get_hovered_ij((int)x, clt->window_height - (int)y, &i, &j);

    switch (picker_id)
    {
        case PICKER_NONE:
            if (escpress) {
                map_unset_stile(map);
                lmap_unset_stile(lmap);
                gw_sqnm_unset_slcd(gw_sqnm);
            }
            map_unset_htile(map);
            lmap_unset_htile(lmap);
            gw_sqnm_unset_hvrd(gw_sqnm);
            break;

        case PICKER_MAP_TILE:
            if (leftpress) map_set_stile(map, i, j);
            if (escpress)  map_unset_stile(map);
            map_set_htile(map, i, j);
            break;

        case PICKER_LOGICAL_MAP_TILE:
            if (leftpress) lmap_set_stile(lmap, i, j);
            if (escpress)  lmap_unset_stile(lmap);
            lmap_set_htile(lmap, i, j);
            break;

        case PICKER_SQN_MENU_LIST:
            if (leftclick) gw_sqnm_set_slcd(gw_sqnm, i);
            if (escpress)  gw_sqnm_unset_slcd(gw_sqnm);
            gw_sqnm_set_hvrd(gw_sqnm, i);
            break;
        default:
            printf("Untracked option: %s\n", __func__);
            break;
    }
}

static void draw_(client_t* clt)
{
    gfx_t* gfx = clt->gfx;
    gw_game_t* gw_game = &sdata.gw_game;

    glViewport(0, 0, clt->window_width, clt->window_height);
    glClearColor(0.8f, 0.18f, 0.24f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    /// larithm2 rendering
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // glBlendEquation(GL_FUNC_ADD);
    glEnable(GL_BLEND);

    if(picker_begin())
    {
        gfx_game_draw_color(clt->gfx, gw_game);
    } picker_end();

    gfx_game_draw(gfx, gw_game);

    /// nk gui rendering
    nk_glfw3_render(NK_ANTI_ALIASING_ON);

    glfwSwapBuffers(clt->window);
}

void stt_game_leave(client_t* clt)
{
    game_free(sdata.game);
    free(sdata.game);

    /// Обнулить указатели на обратные вызовы,
    /// назначенные инициализации state
    glfwSetKeyCallback(clt->window, NULL);
}
