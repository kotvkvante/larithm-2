#include <stdlib.h>
#include <stdio.h>

#include "client/nk_glfw_gl4.h"

#define STT_DEFAULT_DECL
#include "client/state/state.h"
#include "client/client.h"
#include "client/state/main/state.h"
#include "client/state/main/gui.h"

#include "client/graphics/graphics.h"
#include "client/graphics/gui.h"

void stt_main_entry(client_t* clt)
{
    clt->draw = draw_;
    clt->handle_events = handle_events_;
    clt->update = update_;
}

static void handle_events_(client_t* clt)
{
    LA2_UNUSED(clt);

    glfwPollEvents();
}

static void update_(client_t* clt)
{
    nk_glfw3_new_frame();
    gui_panel_main(clt);
}

static void draw_(client_t* clt)
{
    glEnable(GL_BLEND);
    glViewport(0, 0, clt->window_width, clt->window_height);
    glClearColor(0.10f, 0.18f, 0.24f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    nk_glfw3_render(NK_ANTI_ALIASING_ON);
    glfwSwapBuffers(clt->window);
}

void stt_main_leave(client_t* clt)
{
    LA2_UNUSED(clt);
}
