#ifndef STATE_MAIN_GUI_H
#define STATE_MAIN_GUI_H

typedef struct client_t client_t;

void gui_panel_main(client_t* clt);

#endif /* end of include guard: STATE_MAIN_GUI_H */
