#ifndef STATE_MAIN_H
#define STATE_MAIN_H

typedef struct client_t client_t;

void stt_main_entry(client_t* clt);
void stt_main_leave(client_t* clt);

#endif /* end of include guard: STATE_MAIN_H */
