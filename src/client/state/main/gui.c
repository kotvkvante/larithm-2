#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STATE_MAIN
#include "client/state/state.h"
#include "client/state/main/state.h"
#include "client/state/main/gui.h"
#include "client/state/choose_level/state.h"
#include "client/state/community_levels/state.h"
#include "client/state/settings/state.h"

#include "client/graphics/gui_panel.h"


void gui_panel_main(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;

    if (nk_begin(ctx, "Larithm 2", nk_rect(LG_OFFSET, 250, 300), LG_DEFAULT_WINDOW))
    {
        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "Singleplayer"))
        {
            stt_choose_level_entry(clt);
        }

        if (nk_button_label(ctx, "Multiplayer"))
        {
            // stt_choose_level_online_entry(clt);
        }

        if (nk_button_label(ctx, "Map Creator"))
        {

        }

        if (nk_button_label(ctx, "My Levels"))
        {

        }

        if (nk_button_label(ctx, "Community Levels"))
        {
            stt_community_levels_entry(clt);
        }

        if (nk_button_label(ctx, "Settings"))
        {
            stt_settings_entry(clt);
        }

        if (nk_button_label(ctx, "Exit"))
        {
            clt_stop(clt);
        }

    } nk_end(ctx);

}
