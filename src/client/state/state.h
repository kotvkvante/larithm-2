#ifndef STATE_H
#define STATE_H

#include <stdbool.h>

#include "nuklear.h"

// #include "client/state/state_choose_level.h"
// #include "client/state/state_choose_level_online.h"
// #include "client/state/state_main_menu.h"
// #include "client/state/state_settings_menu.h"

#include "client/graphics/gwrap/gw_game.h"

typedef struct client_t client_t;
typedef struct lvlist_t lvlist_t;
typedef struct tframe_t tframe_t;
typedef struct tilex_t tilex_t;

#if defined(STATE_GAME)
typedef struct sd_game_t {
    bool leave_confirm;
    struct nk_rect lg_menu;
    struct nk_rect lg_tileinfo;
    struct nk_rect gpanel_debug;

    game_t* game;
    gw_game_t gw_game;

    /// Graphics data
    /// Common graphics data
    // htilex_t *htilex;

    /// Game graphics data
    /// float map_mvp[16]; <- moved to gw_map.h
    // gw_map_t gw_map;
    GLuint tx_vao; /// tilex
    GLuint tx_vbo;

    // float lmap_mvp[16];
    float sqn_origin[16];

    // tframe_t mapbg;  // tframe for map background
    // tframe_t lmapbg; // tframe for logical map background
    // tframe_t* sqnbg_list; // tframes for sequences background
    //                       // list length is equal to game->players_cnt

    /// Debug data
    int debug1;

    /// tframe test
    GLuint tf_vao;
    GLuint tf_vbo;
    GLuint tf_ebo;

    /// sbox test
    GLuint sb_vao;
    GLuint sb_vbo;

} sd_game_t;
#endif /* STATE_GAME */

#if defined(STATE_CHOOSE_LEVEL)
typedef struct sd_choose_level_t {
    bool initialized;
    lvlist_t* lvlist; /// current lvlist
    lvlist_t* lvlist_default;
    lvlist_t* lvlist_test;
    lvlist_t* lvlist_community;
} sd_choose_level_t;
#endif /* STATE_SETUP_GAME */

#if defined(STATE_COMMUNITY_LEVELS)
typedef struct sd_community_levels_t {
    lvlist_t* lvlist;
} sd_community_levels_t;
#endif /* STATE_COMMUNITY_LEVELS_H */


// void state_entry(client_t* clt, state_id_t id);

#if defined(STT_DEFAULT_DECL)
static void handle_events_(client_t* clt);
static void update_(client_t* clt);
static void draw_(client_t* clt);
#endif

#endif /* end of include guard: STATE_H */
