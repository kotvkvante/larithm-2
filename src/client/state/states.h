#ifndef STATE_ID_H
#define STATE_ID_H

typedef enum state_id_t {
    STT_MAIN = 0,
    STT_CHOOSE_LEVEL,
    STT_GAME,
    /// STT_SETUP_MAP_CREATOR, ?
    STT_MAP_CREATOR,
    STT_COMMUNITY_GAMESAVES,
    STT_SETTINGS,
    STT_COUNT,
} state_id_t;

#endif /* end of include guard: STATE_ID_H */
