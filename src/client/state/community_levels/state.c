#include <stdlib.h>
#include <stdio.h>

#include "common/level/levels_list.h"
#include "utils/utils.h"

#define STT_DEFAULT_DECL
#include "client/nk_glfw_gl4.h"
// #include "client/graphics/gui_panel.h"
#include "client/client.h"

#define STATE_COMMUNITY_LEVELS
#include "client/state/state.h"

#include "client/state/community_levels/state.h"
#include "client/state/community_levels/gui.h"

#include "client/graphics/graphics.h"
#include "client/graphics/picker.h"
#include "client/graphics/gfx/gfx.h"
#include "client/graphics/gwrap/gw_lvlist.h"


static void stt_community_levels_load_levels(client_t* clt);

static sd_community_levels_t sdata = {
	.lvlist = NULL,
};

void stt_community_levels_entry(client_t* clt)
{
    clt->draw = draw_;
    clt->update = update_;
    clt->handle_events = handle_events_;
    clt->sdata = &sdata;

    if (sdata.lvlist == NULL)
    {
        sdata.lvlist = lvlist_new();
    }

    // lvlist_init_from_file(sdata.lvlist, "./assets/lvls/lvls.txt");
    // lvlist_print(sdata.lvlist);

    // glfwSetKeyCallback(clt->window, key_callback_);
}

static void draw_(client_t* clt)
{
    glfwGetWindowSize(clt->window, &clt->window_width, &clt->window_height);
    glViewport(0, 0, clt->window_width, clt->window_height);
    glClearColor(0.10f, 0.18f, 0.24f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // nk gui rendering
    nk_glfw3_render(NK_ANTI_ALIASING_ON);

    // larithm2 stuff rendering
    glEnable(GL_BLEND);
    if(picker_begin())
    {
        gfx_lvlist_draw_color(clt->gfx, sdata.lvlist);
    } picker_end();

    gfx_lvlist_draw(clt->gfx, sdata.lvlist);

    glfwSwapBuffers(clt->window);
}

static void update_(client_t* clt)
{
    lvlist_t* lvlist = sdata.lvlist;
    double x, y;

    // nk gui stuff
    nk_glfw3_new_frame();
	gui_panel_community_levels(clt);

    // larithm2 stuff
    glfwGetCursorPos(clt->window, &x, &y);
	gw_lvlist_update(clt, lvlist, x, y);

    // if (glfwGetKey(clt->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    // {
    //     lvlist_unset_selected(lvlist);
    // }
}

static void handle_events_(client_t* clt)
{
    LA2_UNUSED(clt);
    glfwPollEvents();
}

static void stt_community_levels_load_levels(client_t* clt)
{
	LA2_UNUSED(clt);
	LA2_NOTIMPL();
	// COMMUNITY_LEVELS_SOURCE_LINK
}


void stt_community_levels_leave(client_t* clt)
{
    LA2_UNUSED(clt);
    //  ..
}
