#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STATE_COMMUNITY_LEVELS
#include "client/state/state.h"
#include "client/state/common.h"
#include "client/state/community_levels/state.h"
#include "client/state/community_levels/gui.h"
#include "client/graphics/gui_panel.h"
#include "client/state/main/state.h"

#include "common/level/levels_list.h"
#include "network/common/level/nw_levels_list.h"
#include "network/common/level/nw_level.h"

void gui_panel_community_levels(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_community_levels_t* sdata = (sd_community_levels_t*)clt->sdata;

	if (nk_begin(COMMUNITY_LEVELS_GUI_PANEL))
	{
        nk_layout_row_dynamic(ctx, 30, 1);

	    if (nk_button_label(ctx, "Load List of Levels"))
		{
			nw_lvlist_save_to_file("assets/lvls/tmp/tmp.la2lj");
			lvlist_init_from_file(sdata->lvlist, "assets/lvls/tmp/tmp.la2lj");
		}

        if (nk_button_label(ctx, "Load Selected Level"))
        {
            lvlem_t* lvlem = lvlist_get_selected(sdata->lvlist);
            if (lvlem != NULL) {
                nw_api_lvl_download(lvlem);
                // lvlist_append_to_file(lvlem);
                lvlem_append_to_file_JSON(lvlem, "assets/lvls/list_of_levels.la2lj");
            }
        }

        nk_button_back_to_main(stt_community_levels_leave);
	} nk_end(ctx);
}
