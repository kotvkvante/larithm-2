#ifndef STATE_COMMUNITY_LEVELS_H
#define STATE_COMMUNITY_LEVELS_H

#define COMMUNITY_LEVELS_SOURCE_LINK "localhost"


typedef struct client_t client_t;

void stt_community_levels_entry(client_t* clt);
void stt_community_levels_leave(client_t* clt);


#endif /* end of include guard: STATE_COMMUNITY_LEVELS_H */
