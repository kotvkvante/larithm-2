#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STATE_CHOOSE_LEVEL
#include "client/state/state.h"
#include "client/state/common.h"
#include "client/state/choose_level/state.h"
#include "client/state/choose_level/gui.h"
#include "client/state/main/state.h"
#include "client/state/game/state.h"

#include "client/graphics/gui_panel.h"

#include "common/level/levels_list.h"

void gui_panel_choose_level(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    lvlist_t* lvlist = stt_choose_level_get_lvlist(clt);

    if (nk_begin(CHOOSE_LEVEL_GUI_PANEL))
    {
        bool is_selected = lvlist_is_selected(lvlist);

        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "Go!"))
        {
            if (is_selected)
            {
				stt_choose_level_pass_data(clt);
                stt_choose_level_leave(clt);
                stt_game_entry(clt);
            }
        }

		nk_button_back_to_main(stt_choose_level_leave);
        // if (nk_button_label(ctx, "Back"))
        // {
        //     stt_choose_level_leave(clt);
        //     stt_main_entry(clt);
        // }

        char* p = "";
        char* d = "";

        if (is_selected)
        {
            lvlem_t* el = lvlist_get_selected(lvlist);
            if (el->fname != NULL) p = el->fname;
            if (el->desc != NULL) d = el->desc;
        }
        nk_layout_row_dynamic(ctx, 100, 1);
        nk_label_wrap(ctx, p);
        nk_label_wrap(ctx, d);


    } nk_end(ctx);
}

void gui_panel_choose_level_source(client_t* clt)
{
    struct nk_context* ctx = clt->ctx;
    sd_choose_level_t* sdata = (sd_choose_level_t*)clt->sdata;

    if (nk_begin(CHOOSE_LEVEL_GUI_PANEL_SOURCE))
    {
        nk_layout_row_dynamic(ctx, 30, 1);
        if (nk_button_label(ctx, "default")) {
            sdata->lvlist = sdata->lvlist_default;
        }
        if (nk_button_label(ctx, "community")) {
            sdata->lvlist = sdata->lvlist_community;
        }
        if (nk_button_label(ctx, "test")) {
            sdata->lvlist = sdata->lvlist_test;
        }
    } nk_end(ctx);


}
