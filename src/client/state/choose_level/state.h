#ifndef STATE_CHOOSE_LEVEL_H
#define STATE_CHOOSE_LEVEL_H

typedef struct client_t client_t;
typedef struct lvlist_t lvlist_t;

#define LVLIST_FILEPATH_DEFAULT "./assets/lvls/default.la2lj"
#define LVLIST_FILEPATH_TEST "./assets/lvls/test1.txt"
#define LVLIST_FILEPATH_COMMUNITY "./assets/lvls/community.la2lj"


#define CHOOSE_LEVEL_GUI_PANEL_NAME "Choose Level"
#define CHOOSE_LEVEL_GUI_PANEL_SIZE nk_rect(LG_OFFSET, 250, 300)
#define CHOOSE_LEVEL_GUI_PANEL_WIN LG_DEFAULT_WINDOW
#define CHOOSE_LEVEL_GUI_PANEL 		\
	ctx, 							\
	CHOOSE_LEVEL_GUI_PANEL_NAME,	\
	CHOOSE_LEVEL_GUI_PANEL_SIZE,	\
	CHOOSE_LEVEL_GUI_PANEL_WIN

#define CHOOSE_LEVEL_GUI_PANEL_SOURCE_NAME "Level Source"
#define CHOOSE_LEVEL_GUI_PANEL_SOURCE_SIZE nk_rect(LG_XOFFSET, LG_YOFFSET + 300 + LG_YOFFSET, 250, 300)
#define CHOOSE_LEVEL_GUI_PANEL_SOURCE_WIN LG_DEFAULT_WINDOW
#define CHOOSE_LEVEL_GUI_PANEL_SOURCE 		\
	ctx, 									\
	CHOOSE_LEVEL_GUI_PANEL_SOURCE_NAME,		\
	CHOOSE_LEVEL_GUI_PANEL_SOURCE_SIZE,		\
	CHOOSE_LEVEL_GUI_PANEL_SOURCE_WIN

void stt_choose_level_entry(client_t* clt);
void stt_choose_level_leave(client_t* clt);

void stt_choose_level_pass_data(client_t* clt);
lvlist_t* stt_choose_level_get_lvlist(client_t* clt);

#endif /* end of include guard: STATE_CHOOSE_LEVEL_H */
