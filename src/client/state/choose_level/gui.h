#ifndef STATE_CHOOSE_LEVEL_GUI_H
#define STATE_CHOOSE_LEVEL_GUI_H

typedef struct client_t client_t;

void gui_panel_choose_level(client_t* clt);
void gui_panel_choose_level_source(client_t* clt);

#endif /* end of include guard: STATE_CHOOSE_LEVEL_GUI_H */
