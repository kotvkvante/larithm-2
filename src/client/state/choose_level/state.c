#include <stdlib.h>
#include <stdio.h>

#include "client/nk_glfw_gl4.h"
#include "client/client.h"

#define STT_DEFAULT_DECL
#define STATE_CHOOSE_LEVEL
#include "client/state/state.h"
#include "client/state/choose_level/state.h"
#include "client/state/choose_level/gui.h"

#include "client/graphics/graphics.h"
#include "client/graphics/picker.h"
#include "client/graphics/gfx/gfx.h"
#include "client/graphics/gwrap/gw_lvlist.h"

#include "common/level/levels_list.h"

static void key_callback_(GLFWwindow* window, int key, int scancode, int action, int mods);

static sd_choose_level_t sdata = {
    .initialized = false,
};

void stt_choose_level_entry(client_t* clt)
{
    clt->draw = draw_;
    clt->update = update_;
    clt->handle_events = handle_events_;
    clt->sdata = &sdata;

    if (!sdata.initialized)
    {
        sdata.lvlist_default   = lvlist_new();
        sdata.lvlist_test      = lvlist_new();
        sdata.lvlist_community = lvlist_new();

        if (clt->debug)
        {
            sdata.lvlist = sdata.lvlist_test;
            lvlist_set_selected(sdata.lvlist, 0);
        }
        else
        {
            sdata.lvlist = sdata.lvlist_default;
        }


        sdata.initialized = true;
    }

    lvlist_init_from_file(sdata.lvlist_default, LVLIST_FILEPATH_DEFAULT);
    lvlist_init_from_file(sdata.lvlist_test, LVLIST_FILEPATH_TEST);
    lvlist_init_from_file(sdata.lvlist_community, LVLIST_FILEPATH_COMMUNITY);

    glfwSetKeyCallback(clt->window, key_callback_);
}

lvlist_t* stt_choose_level_get_lvlist(client_t* clt)
{
    LA2_UNUSED(clt);
    return sdata.lvlist;
}

// lvlem_t* stt_choose_level_get_selected(client_t* clt)
// {
//     LA2_UNUSED(clt);
//     if (sdata.lvlist != NULL) {
//         return lvlist_get_selected(sdata.lvlist);
//     }
//     return NULL;
// }

void stt_choose_level_pass_data(client_t* clt)
{
    LA2_UNUSED(clt);
    clt->pdata = (void*)lvlist_get_selected(sdata.lvlist);
}

void stt_choose_level_leave(client_t* clt)
{
    LA2_UNUSED(clt);
    /// Нужно: переиспользование выделенных ресурсов?
}

static void key_callback_(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    LA2_UNUSED(window);
    LA2_UNUSED(scancode);
    LA2_UNUSED(mods);

    client_t* clt = (client_t*)glfwGetWindowUserPointer(window);

    if (action == GLFW_PRESS)
    {
        switch (key)
        {
            case GLFW_KEY_ESCAPE:
                clt_stop(clt);
                break;
        }
    }

    return;
}

static void draw_(client_t* clt)
{
    glfwGetWindowSize(clt->window, &clt->window_width, &clt->window_height);
    glViewport(0, 0, clt->window_width, clt->window_height);
    glClearColor(0.10f, 0.18f, 0.24f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // nk gui rendering
    nk_glfw3_render(NK_ANTI_ALIASING_ON);

    // larithm2 stuff rendering
    glEnable(GL_BLEND);
    if(picker_begin())
    {
        gfx_lvlist_draw_color(clt->gfx, sdata.lvlist);
    } picker_end();

    gfx_lvlist_draw(clt->gfx, sdata.lvlist);

    glfwSwapBuffers(clt->window);
}

static void update_(client_t* clt)
{
    lvlist_t* lvlist = sdata.lvlist;
    double x, y;

    // nk gui stuff
    nk_glfw3_new_frame();
    gui_panel_choose_level(clt);
    gui_panel_choose_level_source(clt);

    // larithm2 stuff
    glfwGetCursorPos(clt->window, &x, &y);
    gw_lvlist_update(clt, lvlist, x, y);

    // if (glfwGetKey(clt->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    // {
    //     lvlist_unset_selected(lvlist);
    // }
}

static void handle_events_(client_t* clt)
{
    LA2_UNUSED(clt);
    glfwPollEvents();
}
