#ifndef STATE_COMMON_H
#define STATE_COMMON_H

#define nk_button_back_to_main_ex(f, s) 	\
    if (nk_button_label(ctx, "Back")) 		\
	{										\
		s;									\
	    f(clt);								\
        stt_main_entry(clt);				\
	}

#define nk_button_back_to_main(f) 			\
	nk_button_back_to_main_ex(f, )

#endif /* end of include guard: STATE_COMMON_H */
