#ifndef CLIENT_H
#define CLIENT_H

#include <stdbool.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "client/state/states.h"
// #include "client/state/state_main_menu.h"
// #include "client/state/state_game.h"
// #include "client/state/state_choose_level.h"
// #include "client/state/state.h"
// #include "client/state/state_choose_level_online.h"

typedef struct client_t client_t;
typedef struct gfx_t gfx_t;
typedef struct lvlist_t lvlist_t;
typedef struct game_t game_t;

typedef void (*handle_events_f)(client_t*);
typedef void (*update_f)(client_t*);
typedef void (*draw_f)(client_t*);

typedef struct client_t
{
    bool debug;

    int window_height;
    int window_width;

    gfx_t* gfx;
    GLFWwindow* window;
    struct nk_context* ctx;

    // lvlist_t* lvlist; // part of direct client state?
    // clt_state_t state_id;

    draw_f draw;
    update_f update;
    handle_events_f handle_events;

    state_id_t id;
    void* sdata; /// state data
    void* pdata; /// passed data: pointer to data passed
                 /// to current state from previous state

} client_t;

void clt_parse_args(client_t* clt, int argc, char const *argv[]);
void clt_init(client_t* clt);
bool clt_should_stop(client_t* clt);
void clt_stop(client_t* clt);

void clt_handle_events(client_t* clt);
void clt_update(client_t* clt);
void clt_draw(client_t* clt);
void clt_set_window_size(client_t* clt, int w, int h);

/// Нужно: close или free ?
void clt_close(client_t* clt);

#endif /* end of include guard: CLIENT_H */
