#ifndef CLT_CALLBACKS_H
#define CLT_CALLBACKS_H

typedef struct client_t client_t;
typedef struct camera_t camera_t;
typedef struct GLFWwindow GLFWwindow;

void error_callback(int e,  const char* d);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void window_size_callback(GLFWwindow* window, int width, int height);

#endif /* end of include guard: CLT_CALLBACKS_H */
