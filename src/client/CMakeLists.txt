# Client
target_compile_options(laclient PRIVATE -Wall -Wextra -pedantic )

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)

target_sources(laclient PUBLIC
    main.c
    client.c
    callbacks.c
    nk_glfw_gl4.c

    state/state.c

    state/main/state.c
    state/main/gui.c
    state/choose_level/state.c
    state/choose_level/gui.c
    state/game/state.c
    state/game/gui.c
    state/settings/state.c
    state/settings/gui.c

    state/community_levels/state.c
    state/community_levels/gui.c

    graphics/graphics.c
    graphics/gfx/gfx_tile.c
    graphics/gfx/gfx_tile_color.c
    graphics/gfx/gfx_map.c
    graphics/gfx/gfx_lmap.c
    graphics/gfx/gfx_game.c
    graphics/gfx/gfx_lvlist.c
    graphics/gfx/gfx_player.c
    graphics/gfx/gfx_sqn.c
    graphics/gfx/gfx_nineslice.c
    graphics/gfx/gfx_tframe.c
    # graphics/gfx/gfx_sbox.c
    graphics/gfx/gfx_tilex.c
    graphics/gfx/gfx_tilexu.c
    graphics/gfx/gfx_test.c
    # graphics/gwrap/gw_tilex.c
    graphics/gwrap/gw_game.c
    graphics/gwrap/gw_sqnm.c
    graphics/gwrap/gw_sqns_list.c
    graphics/gwrap/gw_lvlist.c
    # graphics/gwrap/gw_map.c
    graphics/shader.c
    graphics/gui.c
    graphics/gui_sqnm.c
    graphics/camera.c
    graphics/texture.c
    graphics/picker.c
    graphics/tframe.c
    # graphics/sbox.c
    graphics/texarr.c
)

target_include_directories(laclient PUBLIC
    "${CMAKE_SOURCE_DIR}/src"
)

# Client glfw3

# set(GLEW_USE_STATIC_LIBS TRUE)
# set(GLEW_LIB GLEW::glew_s)
# set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

# Client glad

target_link_libraries(laclient
    m
    glfw
    OpenGL::GL
    OpenGL::GLU
    GLEW::GLEW
    lamath
    larithm2
    lautils
    lanetwork
)

# Client nuklear

target_include_directories(laclient PUBLIC
    extern/Nuklear
    extern
)
