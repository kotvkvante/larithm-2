#include <stdlib.h>
#include <stdio.h>

#include "client/client.h"
#include "client/callbacks.h"
#include "client/graphics/graphics.h"
#include "client/graphics/camera.h"
#include "client/graphics/picker.h"

#include "utils/utils.h"

void error_callback(int e, const char *d)
{
    printf("Error > [%d]: %s\n", e, d);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    LA2_UNUSED(window);
    LA2_UNUSED(key);
    LA2_UNUSED(scancode);
    LA2_UNUSED(action);
    LA2_UNUSED(mods);
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    client_t* clt = (client_t*)glfwGetWindowUserPointer(window);
    camera_t* camera = gfx_get_camera(clt->gfx);

    clt_set_window_size(clt, width, height);

    /// Наверное: нужно сделать функцию gfx_update_winsize(), которая
    /// будет обрабатывать все функции модуля графики связанные
    /// с изменением размера экрана.
    camera_update(camera, width, height);
    picker_resize(width, height);
}
