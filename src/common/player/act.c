#include "common/player/sequence.h"
#include "common/player/act.h"

char* action_info[SQN_COUNT] = {
    [SQN_NONE]  = "None action",
    [SQN_START] = "Start action",
    [SQN_PLMOVE] = "Player move action",
    [SQN_PLINTERACT] = "Player interact action",
    [SQN_PLOUT  ] = "Player out",
    [SQN_PLIN   ] = "Player in",
    [SQN_END] = "Start action",
};

char* action_info_short[SQN_COUNT] = {
    [SQN_NONE   ] = " ",
    [SQN_START  ] = "start",
    [SQN_PLMOVE ] = "player move",
    [SQN_PLINTERACT] = "player interact",
    [SQN_PLOUT  ] = "player out",
    [SQN_PLIN   ] = "player in",
    [SQN_END    ] = "end",
};
