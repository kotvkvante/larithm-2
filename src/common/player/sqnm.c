#include <stdio.h>

#include "common/player/sequence.h"
#include "common/player/sqnm.h"
#include "common/tile/htilex.h"
#include "common/tile/tilex.h"

extern int SQN_TEXTURES[SQN_COUNT];

sqnm_t* sqnm_new()
{
	return malloc(sizeof(sqnm_t));
}

void sqnm_init(sqnm_t* sm, size_t count)
{
	sm->cnt = count;
}

void sqnm_init_default(sqnm_t* sm)
{
	sm->slcd = 0;
	sm->hvrd = 0;
	sm->handler = NULL;
	sm->cnt = SQN_COUNT;

	sm->acts_list = malloc(sizeof(action_t) * sm->cnt);
	sm->acts_list[SQN_NONE		] = (action_t){ SQN_NONE, NULL, 0};

	static a_plin_t start_data_ = {.i = 0};
	sm->acts_list[SQN_START		] = (action_t){ SQN_START, &start_data_, sizeof(a_plin_t)};

	static a_plmove_t plmove_data_ = {.d = RIGHT};
	sm->acts_list[SQN_PLMOVE	] = (action_t){ SQN_PLMOVE, &plmove_data_, sizeof(plmove_data_)};

	sm->acts_list[SQN_PLINTERACT] = (action_t){ SQN_PLINTERACT, NULL, 0};

	static a_plout_t plout_data_ = {.o = 0};
	sm->acts_list[SQN_PLOUT		] = (action_t){ SQN_PLOUT, &plout_data_, sizeof(a_plout_t)};

	static a_plin_t plin_data_ = {.i = 0};
	sm->acts_list[SQN_PLIN		] = (action_t){ SQN_PLIN, &plin_data_, sizeof(a_plin_t)};

	static a_plout_t end_data_ = {.o = 0};
	sm->acts_list[SQN_END		] = (action_t){ SQN_END, &end_data_, sizeof(a_plout_t)};
}

void sqnm_print(sqnm_t* sm)
{
	for (size_t i = 0; i < sm->cnt; i++) {
		printf("[%d %p]", sm->acts_list[i].action, sm->acts_list[i].data);
	}
	printf("\n");
}

action_t* sqnm_get_slcd_act(sqnm_t* sm)
{
	if (sm->slcd == 0) return NULL;
	return &sm->acts_list[sm->slcd];
}

void sqnm_set_selected(sqnm_t* sm, size_t s)
{
	sm->slcd = s;
}

void sqnm_unset_selected(sqnm_t* sm)
{
	sm->slcd = 0;
}

void sqnm_set_hovered(sqnm_t* sm, size_t h)
{
	sm->hvrd = h;
}

void sqnm_unset_hovered(sqnm_t* sm)
{
	sm->hvrd = 0;
}

char* sqnm_get_info(sqnm_t* sm, size_t i)
{
	action_e a = sm->acts_list[i].action;
	return action_info[a];
}

char* sqnm_get_sinfo(sqnm_t* sm, size_t i)
{
	action_e a = sm->acts_list[i].action;
	return action_info_short[a];
}

char* sqnm_get_slcd_info(sqnm_t* sm)
{
	action_e a = sm->acts_list[sm->slcd].action;
	return action_info[a];
}

char* sqnm_get_slcd_sinfo(sqnm_t* sm)
{
	action_e a = sm->acts_list[sm->slcd].action;
	return action_info_short[a];
}
