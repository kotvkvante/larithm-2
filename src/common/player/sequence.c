#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common/player/sequence.h"
#include "utils/utils.h"

sqn_ftable_t sqn_ftable = {
    .ctx = NULL,
    .inc = NULL,
    .dec = NULL,
    .remove = NULL,
    .insert = NULL,
};

sqn_t* sqn_new()
{
    return malloc(sizeof(sqn_t));
}

sqn_t* sqns_list_new(size_t n)
{
    return malloc(sizeof(sqn_t) * n);
}

void sqn_init_default(sqn_t* sqn)
{
    sqn_init_max(sqn, SQN_ACTS_MAX);
}

void sqn_init_max(sqn_t* sqn, size_t acts_max)
{
    sqn->act_next = 0;
    sqn->acts_count = 0;
    sqn->acts_max = acts_max;
    sqn->acts_list = malloc(sizeof(action_t) * sqn->acts_max);
}

void sqn_init_test1(sqn_t* sqn)
{
    sqn_init_default(sqn);
    sqn_append(sqn, SQN_START);
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){RIGHT});
    sqn_append(sqn, SQN_PLINTERACT);
    sqn_append_data(sqn, SQN_PLOUT, sizeof(a_plout_t), &(a_plout_t){.o = 2});
    sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 1});
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){UP});
    // sqn_append_data(sqn, SQN_PLOUT, sizeof(a_plout_t), &(a_plout_t){.o = 1});
    // sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 1});
    sqn_append_data(sqn, SQN_PLMOVE,  sizeof(a_plmove_t), &(a_plmove_t){LEFT});
    sqn_append(sqn, SQN_END);
    // sqn_print(sqn1);
}

void sqn_init_test2(sqn_t* sqn)
{
    sqn_init_default(sqn);
    sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 2});
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){DOWN});
    sqn_append_data(sqn, SQN_PLOUT, sizeof(a_plout_t), &(a_plout_t){.o = 2});
    sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 2});
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){DOWN});
    sqn_append_data(sqn, SQN_END,  sizeof(a_plout_t), &(a_plout_t){.o = 0});
    // sqn_print(sqn2);
}

void sqn_init_test3(sqn_t* sqn)
{
    sqn_init_default(sqn);

    sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 0});
    // sqn_append_data(sqn, SQN_START, sizeof(a_plin_t), &(a_plin_t){.i = 0});
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){DOWN});
    sqn_append_data(sqn, SQN_PLOUT, sizeof(a_plout_t), &(a_plout_t){.o = 1});
    sqn_append_data(sqn, SQN_PLIN,  sizeof(a_plin_t),  &(a_plin_t){.i = 1});
    sqn_append_data(sqn, SQN_PLMOVE, sizeof(a_plmove_t), &(a_plmove_t){DOWN});
    sqn_append_data(sqn, SQN_END,  sizeof(a_plout_t), &(a_plout_t){.o = 1});
}

void sqn_init_static(sqn_t* sqn)
{
    sqn->act_next = 0;
    sqn->acts_count = SQN_COUNT + SQN_DCOUNT - 2; /// -1 for SQN_NONE
    sqn->acts_max = sqn->acts_count;
    sqn->acts_list = malloc(sizeof(action_t) * sqn->acts_max);

    size_t k = 0;
    a_plmove_t* pm = malloc(sizeof(a_plmove_t) * 1);
    pm[0].d = RIGHT;

    sqn->acts_list[k++] = (action_t){ .action = SQN_START, .data = NULL};
    sqn->acts_list[k++] = (action_t){ .action = SQN_PLMOVE, .data = &pm[0]};
    sqn->acts_list[k++] = (action_t){ .action = SQN_PLINTERACT, .data = NULL};
    sqn->acts_list[k++] = (action_t){ .action = SQN_PLOUT, .data = NULL};
    sqn->acts_list[k++] = (action_t){ .action = SQN_PLIN, .data = NULL};
    sqn->acts_list[k++] = (action_t){ .action = SQN_END, .data = NULL};
    /// ? SQN_COUNT? ???
    // sqn->acts_list[k++] = (action_t){ .action = SQN_COUNT, .data = NULL};
}

bool sqn_next(sqn_t* sqn, action_t** a)
{
    // https://stackoverflow.com/questions/8233161/compare-int-and-unsigned-int
    // if (sqn->act_next >= 0 && ((unsigned int)sqn->act_next) == sqn->acts_count)
    //     return false;

    if (sqn->act_next == sqn->acts_count) return false;

    *a = &(sqn->acts_list[sqn->act_next]);
    sqn->act_next++;
    return true;
}

bool sqn_prev(sqn_t* sqn, action_t** a)
{
    if (sqn->act_next == 0) return false;

    sqn->act_next--;
    *a = &(sqn->acts_list[sqn->act_next]);
    return true;
}

void sqn_inc(sqn_t* sqn)
{
    sqn->act_next++;
}

void sqn_dec(sqn_t* sqn)
{
    sqn->act_next--;
}

void sqn_inc_safe(sqn_t* sqn)
{
    if (sqn == NULL) return;
    if (sqn->act_next == sqn->acts_count) return;
    sqn_inc(sqn);
    // if (sqn_ftable.inc != NULL) sqn_ftable.inc(sqn, sqn_ftable.ctx);
}

void sqn_dec_safe(sqn_t* sqn)
{
    if (sqn == NULL) return;
    if (sqn->act_next == 0) return;
    sqn_dec(sqn);
    // if (sqn_ftable.dec != NULL) sqn_ftable.dec(sqn, sqn_ftable.ctx);
}

void sqn_append(sqn_t* sqn, action_e a)
{
    // Нужно добавить проверки переполности
    sqn->acts_list[sqn->acts_count].action = a;
    sqn->acts_list[sqn->acts_count].data = NULL;
    sqn->acts_list[sqn->acts_count].size = 0;
    sqn->acts_count++;
}

void sqn_append_data(sqn_t* sqn, action_e a, size_t size, void* data)
{
    sqn->acts_list[sqn->acts_count].action = a;
    sqn->acts_list[sqn->acts_count].size = size;
    sqn->acts_list[sqn->acts_count].data = malloc(size);
    memcpy(sqn->acts_list[sqn->acts_count].data, data, size);

    sqn->acts_count++;
}

void sqn_remove(sqn_t* sqn, size_t n)
{
    if (n >= sqn->acts_count) return;
    // if (n == sqn->acts_count - 1) {
    //     sqn->acts_count--;
    //     if (sqn_ftable.remove_last != NULL) sqn_ftable.remove_last(sqn, sqn_ftable.ctx);
    //     return;
    // }

    for (size_t i = n; i < sqn->acts_count; i++) {
        sqn->acts_list[i] = sqn->acts_list[i+1];
    }
    sqn->acts_count--;
    if (sqn_ftable.remove != NULL) sqn_ftable.remove(sqn, n, sqn_ftable.ctx);
}

// void sqn_insert(sqn_t* sqn, action_e a, size_t n)
// {
//     if (n >= sqn->acts_count) return;
//
//     for (size_t j = sqn->acts_count; j > n; j--) {
//         sqn->acts_list[j] = sqn->acts_list[j-1];
//     }
//
//     sqn->acts_list[n].action = a;
//     sqn->acts_list[n].data = NULL;
//     sqn->acts_count++;
// }

// void sqn_insert_data(sqn_t* sqn, action_e a, size_t i, size_t n, void* data)
// {
//     if (i > sqn->acts_max-1) return;
//     // if (i == sqn->acts_count-1) {
//     //     sqn->acts_count--;
//     //     return;
//     // }
//
//     for (size_t j = sqn->acts_count; j > i; j--) {
//         sqn->acts_list[j] = sqn->acts_list[j-1];
//     }
//
//     sqn->acts_list[i].action = a;
//     sqn->acts_list[i].data = malloc(n);
//     memcpy(sqn->acts_list[i].data, data, n);
//     // sqn->acts_list[sqn->acts_count].data = malloc(n);
//     // memcpy(sqn->acts_list[sqn->acts_count].data, data, n);
//
//     sqn->acts_count++;
//
// }

/// ?
void sqn_insert_data_safe(sqn_t* sqn, action_e a, size_t n, void* data, size_t size)
{
    if (n > sqn->acts_max-1) return;
    // if (i == sqn->acts_count-1) {
    //     sqn->acts_count--;
    //     return;
    // }

    for (size_t j = sqn->acts_count; j > n; j--) {
        sqn->acts_list[j] = sqn->acts_list[j-1];
    }

    sqn->acts_list[n].action = a;
    if ((size != 0) && (data != NULL))
    {
        sqn->acts_list[n].data = malloc(size);
        sqn->acts_list[n].size = size;
        memcpy(sqn->acts_list[n].data, data, size);

        // printf("[i] Inserted %d\n", ((a_plmove_t*)data)->d);
    }
    else sqn->acts_list[n].data = NULL;

    sqn->acts_count++;

    if (sqn_ftable.insert != NULL) sqn_ftable.insert(sqn, n, sqn_ftable.ctx);
}

/// Копирует act
void sqn_insert_act_safe(sqn_t* sqn, size_t n, action_t* act)
{
    sqn_insert_data_safe(sqn, act->action, n, act->data, act->size);
}

void sqn_append_act_safe(sqn_t* sqn, action_t* act)
{
    sqn_insert_data_safe(sqn, act->action, sqn->acts_count, act->data, act->size);
}

void sqn_action_info(action_t* a)
{
    printf("[i] action: %s", action_info[a->action]);
}

void sqn_print(sqn_t* sqn)
{
    const char t1[] = " %d";
    const char t2[] = "|%d";
    const char* t = t1;
    printf("[i] sqn %p [%2.ld, %2.1ld]: ", (void*)sqn, sqn->acts_count, sqn->act_next);
    for (size_t i = 0; i < sqn->acts_count; i++) {
        if (i == sqn->act_next) t = t2;
        else t = t1;
        printf(t, sqn->acts_list[i].action);
    }

    if (sqn->act_next == sqn->acts_count)
        fputs("|", stdout);

    printf("\n");
}
