#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <stdlib.h>
#include <stdbool.h>
#include "common/player/act.h"

#define SQN_ACTS_MAX 16

typedef struct sqn_t sqn_t;
typedef void (*gw_sqn_f)(sqn_t* sqn, void* ctx);
typedef void (*gw_sqn_n_f)(sqn_t* sqn, size_t n, void* ctx);
typedef struct sqn_ftable_t
{
    void* ctx;
    gw_sqn_f inc;
    gw_sqn_f dec;
    gw_sqn_n_f remove;
    gw_sqn_n_f insert;
} sqn_ftable_t;

/// Все виды действий (actions)
typedef struct a_plin_t {
    size_t i;
} a_plin_t;

typedef struct a_plout_t {
    size_t o;
} a_plout_t;

typedef struct a_plmove_t {
    direction_e d;
} a_plmove_t;

typedef struct action_t {
    action_e action;
    void* data;
    size_t size; /// data size
} action_t;

typedef struct sqn_t {
    action_t* acts_list;
    size_t acts_max;
    size_t acts_count;
    size_t act_next;

    void* gd; /// graphics data
} sqn_t;

sqn_t* sqn_new();
sqn_t* sqns_list_new(size_t n);

void sqn_init_default(sqn_t* sqn);
void sqn_init_max(sqn_t* sqn, size_t acts_max);
void sqn_init_static(sqn_t* sqn);
void sqn_init_test1(sqn_t* sqn);
void sqn_init_test2(sqn_t* sqn);
void sqn_init_test3(sqn_t* sqn);

bool sqn_next(sqn_t* sqn, action_t** a);
bool sqn_prev(sqn_t* sqn, action_t** a);

void sqn_inc_safe(sqn_t* sqn);
void sqn_dec_safe(sqn_t* sqn);
void sqn_inc(sqn_t* sqn);
void sqn_dec(sqn_t* sqn);

void sqn_append(sqn_t* sqn, action_e a);
void sqn_append_data(sqn_t* sqn, action_e a, size_t size, void* data);
void sqn_remove(sqn_t* sqn, size_t n);

void sqn_insert(sqn_t* sqn, action_e a, size_t n);
// Нужно: переписать функции _data и _data_safe,
// чтобы соответствовали названию
void sqn_insert_data(sqn_t* sqn, action_e a, size_t i, size_t n, void* data);
void sqn_insert_data_safe(sqn_t* sqn, action_e a, size_t n, void* data, size_t size);

void sqn_insert_act_safe(sqn_t* sqn, size_t n, action_t* act);
void sqn_append_act_safe(sqn_t* sqn, action_t* act);

void sqn_action_info(action_t* a);
void sqn_print(sqn_t* sqn);

#endif /* end of include guard: SEQUENCE_H */
