#include <stdlib.h>
#include <stdio.h>

#include "client/graphics/texture.h"
#include "common/player/player.h"

player_t* player_new()
{
    return malloc(sizeof(player_t));
}

player_t* players_list_new(size_t n)
{
    return malloc(sizeof(player_t) * n);
}

void player_init(player_t* p, size_t pid, int x, int y)
{
    p->x = x;
    p->y = y;
    p->pid = pid;

    tilex_init(&p->t, (float)x, (float)y, TEXTURE_MAP_POINTER_PLAYER_1 + pid, 0);
}

void player_update_tilex(player_t* p)
{
    tilex_set_xy(&p->t, (float)p->x, (float)p->y);
}

int player_get_x(player_t* p)
{
    return p->x;
}

int player_get_y(player_t* p)
{
    return p->y;
}

void player_set_sqn(player_t* p, sqn_t* sqn)
{
    p->sqn = sqn;
}

sqn_t* player_get_sqn(player_t* p)
{
    return p->sqn;
}

void player_move(player_t* p, direction_e d)
{
    switch (d) {
        case LEFT:
            player_move_left(p);
            goto update;
        case RIGHT:
            player_move_right(p);
            goto update;
        case DOWN:
            player_move_down(p);
            goto update;
        case UP:
            player_move_up(p);
            goto update;
    }
    return;

    update:
        player_update_tilex(p);
}

void player_calc_dest_pos(player_t* p, direction_e d, int* dest_x, int* dest_y)
{
    switch (d) {
        case LEFT:
            *dest_x = p->x--;
            *dest_y = p->y;
            return;
        case RIGHT:
            *dest_x = p->x++;
            *dest_y = p->y;
            return;
        case DOWN:
            *dest_x = p->x;
            *dest_y = p->y--;
            return;
        case UP:
            *dest_x = p->x;
            *dest_y = p->y++;
            return;
    }
}

void player_move_to(player_t* p, int x, int y)
{
    p->x = x;
    p->y = y;
}

void player_move_left(player_t* p)
{
    p->x--;
}
void player_move_right(player_t* p)
{
    p->x++;
}

void player_move_up(player_t* p)
{
    p->y++;
}

void player_move_down(player_t* p)
{
    p->y--;
}
