#ifndef ACT_H
#define ACT_H

typedef enum {
	SQN_NONE,  			// 0
	SQN_START, 			// 1
	SQN_PLMOVE,			// 2
	SQN_PLINTERACT,		// 3
	SQN_PLOUT,			// 4
	SQN_PLIN,			// 5
	SQN_END,
	// SQN_COUNT, // 7
} action_e;
#define SQN_COUNT (SQN_END + 1)

#define SQN_DCOUNT 4
typedef enum {
	RIGHT,
	DOWN,
	LEFT,
	UP,
} direction_e;

extern char* action_info[SQN_COUNT];
extern char* action_info_short[SQN_COUNT];

#endif /* end of include guard: ACT_H */
