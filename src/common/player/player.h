#ifndef PLAYER_H
#define PLAYER_H

#include "common/player/sequence.h"
#include "common/tile/tilex.h"

typedef struct level_t level_t;
typedef struct map_t map_t;
typedef struct player_t player_t;
typedef struct sqn_t sqn_t;

typedef struct player_t
{
    int x, y;
    sqn_t* sqn;
    size_t pid; /// player id. Индекс игрока в общем списке
    size_t sind; /// sqn index
    tilex_t t;
} player_t;

player_t* player_new();
player_t* players_list_new(size_t n);

void player_init(player_t* p, size_t pid, int x, int y);

void player_set_sqn(player_t* p, sqn_t* sqn);
sqn_t* player_get_sqn(player_t* p);

int player_get_x(player_t* p);
int player_get_y(player_t* p);

void player_move(player_t* p, direction_e d);
void player_move_to(player_t* p, int x, int y);
void player_move_left(player_t* p);
void player_move_right(player_t* p);
void player_move_up(player_t* p);
void player_move_down(player_t* p);

#endif /* end of include guard: PLAYER_H */
