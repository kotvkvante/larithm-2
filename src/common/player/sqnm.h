#ifndef SQNM_H
#define SQNM_H

#include <stdlib.h>
#include <stdbool.h>
#include "common/player/sequence.h"

/// Опция: sqn mask с целью задавать различные
/// sqn menu можно просто ставить в соответствии
/// каждому игроку свою sqnm
/// typedef struct smask_t {
///
/// } sqnmask_t;

typedef struct client_t client_t;
typedef size_t (*sqnm_handler_f)(client_t* clt, action_t* a);
// typedef void (*sqnm_update_tilex_f)(sqnm_t* sqnm, );

typedef struct sqnm_t {
	action_t* acts_list;
	size_t cnt;  		 /// Count

	size_t slcd; 		 /// Индекс Selected
	size_t hvrd; 		 /// Индекс Hovered

	sqnm_handler_f handler; /// Обработчик клиента для выбора
} sqnm_t;

sqnm_t* sqnm_new();
void sqnm_init(sqnm_t* sm, size_t count);
void sqnm_init_default(sqnm_t* sm);
void sqnm_print(sqnm_t* sm);

void sqnm_set_selected(sqnm_t* sm, size_t s);
void sqnm_unset_selected(sqnm_t* sm);
void sqnm_set_hovered(sqnm_t* sm, size_t h);
void sqnm_unset_hovered(sqnm_t* sm);
action_t* sqnm_get_slcd_act(sqnm_t* sm);

/// Важно: unsafe
char* sqnm_get_info(sqnm_t* sm, size_t i);
char* sqnm_get_sinfo(sqnm_t* sm, size_t i);
char* sqnm_get_slcd_info(sqnm_t* sm);
char* sqnm_get_slcd_sinfo(sqnm_t* sm);

#endif /* end of include guard: SQNM_H */
