#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>


#include "common/game/game.h"
#include "common/game/gsff.h"

#include "common/objs/base.h"
#include "common/objs/wall.h"
#include "common/objs/lever.h"
#include "common/objs/door.h"
#include "common/objs/star.h"
#include "common/map/map.h"

static bool gsff_verify_map_compr(gsff_map_raw_t* mraw);
static void gsff_init_objs(size_t cnt, obj_base_t** objs_list, char* objs_raw);
static void gsff_init_objs_inds(map_t* map, obj_base_t** objs_compr, char* inds_raw);
static void gsff_init_obj(obj_base_t** pobj, char* obj_raw);


void gsff_init_map_compr(game_t* game, gsff_map_raw_t* mraw)
{
	assert(game != NULL);
	if (!gsff_verify_map_compr(mraw)) {
		exit(-1);
	}

	gsff_map_data_compr_t* mdc = (gsff_map_data_compr_t*)mraw->data;
	map_init(game->map, (size_t)mraw->size, (size_t)mdc->objs_cnt);

	obj_base_t** objs_compr_list = malloc(sizeof(obj_base_t*) * mdc->objs_compr_cnt);

	gsff_init_objs(mdc->objs_cnt, objs_compr_list, mdc->objs_compr_raw);
	gsff_init_objs_inds(game->map, objs_compr_list, mdc->inds_raw);

	free(objs_compr_list);
}

static bool gsff_verify_map_compr(gsff_map_raw_t* mraw)
{
	assert(mraw->size > 0);

	gsff_map_data_compr_t* mdc = (gsff_map_data_compr_t*)mraw->data;
	assert(mdc->objs_compr_cnt >= 0);
	assert(mdc->objs_cnt >= 0);
	assert(mdc->objs_compr_raw != NULL);
	assert(mdc->inds_raw != NULL);

	return true;
}

void gsff_init_objs(size_t cnt, obj_base_t** objs_list, char* objs_raw)
{
	char* p; /// action pointer
	size_t i = 0;
	// printf("[i] `%s`\n", acts_compr_raw);
	char* obj_raw = strtok_r(objs_raw, ";", &p);
	// printf("[i] `%s`\n", act_raw);

	/// Нужно: Обработка неверного числа cnt
	while (obj_raw != NULL)
	{
		// act_raw = dropSpace(act_raw);
		gsff_init_obj(&objs_list[i], obj_raw);
		obj_raw = strtok_r(NULL, ";", &p);
		// printf("[i] `%s`\n", act_raw);
		i++;
	}
}

static void gsff_init_objs_inds(map_t* map, obj_base_t** objs_compr, char* inds_raw)
{
	int ind = 0, k = 0;
	// const char delims[] = ", \n";
	const char delims[] = ", ";
	char* p = strtok(inds_raw, delims);
	while (p != NULL)
	{
		// printf("---------------------------------\n");
		int res = sscanf(p, "%d", &ind);
		// if (*p == '\n') *p='N';
		// printf("\t res=`%d` s1=`%s`\n", res, p);
		p = strtok(NULL, delims);

		// if (*p == '\n') *p='N';
		if (res != 1) { continue; /* Ошибка чтения */ }
		// printf("\t s2=`%s`", p);
		// printf("-> \t %d \n", ind);

		if (ind == 0) { goto next; }
		int x = k % map->size;
		int y = k / map->size;
		map_set_obj_copy(map, objs_compr[ind], x, y);

		/// Нужно: рефакторинг?
		switch (objs_compr[ind]->type) {
			case OBJ_TYPE_STAR:
				map->score_goal++;
				break;
		}

		next:
			k++;
	}
}

static void gsff_init_obj(obj_base_t** pobj, char* obj_raw)
{
	obj_type_e obj_type;
	char* p = strtok(obj_raw, ",");

	if (!sscanf(p, "%d", (int*)&obj_type)) {
		printf("[w] Failed to scan object type: `%s`. Trying to scan next object. . .\n ", obj_raw);
		return;
	}

	if ((obj_type < 0) || (obj_type >= OBJ_TYPE_COUNT)) {
		printf("[E] Not supported object type `%d`. Trying to scan next object. . .\n", obj_type);
		return;
	}

    switch (obj_type) {
        case OBJ_TYPE_BASE: {
			*pobj = obj_base_new();
			obj_base_init(*pobj, OBJ_TYPE_BASE, 0, 0);
		} break;

        case OBJ_TYPE_WALL: {
            obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, 0, 0);
			*pobj = (obj_base_t*)wall;
        } break;

        case OBJ_TYPE_DOOR: {
            obj_door_t* door = obj_door_new();
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
            obj_door_init(door, 0, 0, value);
			*pobj = (obj_base_t*)door;
        } break;

        case OBJ_TYPE_LEVER: {
            obj_lever_t* lever = obj_lever_new();
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
            obj_lever_init(lever, 0, 0, value);
			*pobj = (obj_base_t*)lever;
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_t* star = obj_star_new();
            obj_star_init(star, 0, 0);
			*pobj = (obj_base_t*)star;
        } break;

        default: {
            printf("[w] `%s`: untracked obj type: '%d'.\n", __func__, obj_type);
        } break;
    }
}
