#ifndef GAME_H
#define GAME_H

#include <stdlib.h>
#include <stdbool.h>

#include "common/game/gsff.h"

typedef struct game_t
{
    // level_t* level;

    bool need_sync;
    map_t* map; /// Важно: почему это указатель, а не поле ???
    logical_map_t* logical_map;

    /// players
    size_t player_cur;
    size_t players_cnt;
    // player_t** players_list;
    player_t* players_list;
    sqn_t*    sqns_list;

    /// sqn menu
    sqnm_t* sqnm;
    /// mask for sqnm ?

} game_t;

game_t* game_new();

// void game_init_default(game_t* game, level_t* level);
void game_init_from_file(game_t* game, char* filepath);
void game_init_from_str(game_t* game, char* str, gsfformat_e ff);

void game_set_need_sync(game_t* game);
bool game_get_need_sync(game_t* game);
void game_sync_maps(game_t* game);


map_t* game_get_map(game_t* game);
logical_map_t* game_get_lmap(game_t* game);

player_t* game_get_current_player(game_t* game);

/// game get current player index
size_t game_get_curplayer_index(game_t* game);

bool game_step_next(game_t* game);
bool game_step_prev(game_t* game);
void game_compute(game_t* game);
void game_compute_reverse(game_t* game);
bool game_check_win(game_t* game);

bool game_switch_player(game_t* game, size_t pid);
void game_next_player(game_t* game);
void game_prev_player(game_t* game);

void game_info(game_t* game);
void game_free(game_t* game);

#endif /* end of include guard: GAME_H */
