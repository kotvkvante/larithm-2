#ifndef GSFF_H
#define GSFF_H

#include <stdbool.h>

typedef struct game_t game_t;
typedef struct map_t map_t;
typedef struct logical_map_t logical_map_t;
typedef struct obj_base_t obj_base_t;
typedef struct player_t player_t;
typedef struct sqn_t sqn_t;
typedef struct sqnm_t sqnm_t;
typedef struct action_t action_t;

typedef enum {
    /// basic plain text format based on json format
    LF_JSON,
    /// basic plain text format based on config style format
    LF_CFG,

    LF_UNDEFINED,
} gsfformat_e;

typedef struct gsff_players_raw_t {
    int cnt;
    int cur;
    char* positions;
} gsff_players_raw_t;

typedef struct gsff_sqns_raw_t {
    int is_compr;
    int acts_compr_cnt;
    char* acts_compr_raw;
    char* sqns_inds_raw;
} gsff_sqns_raw_t;

/// Map
typedef enum gsff_map_data_type_e
{
    MAP_DATA_COMPR,
    MAP_DATA_PLAIN
} gsff_map_data_type_e;

typedef struct gsff_map_raw_t {
    int size;
    char* data_type_raw;
    gsff_map_data_type_e data_type;
    void* data;
} gsff_map_raw_t;

typedef struct gsff_map_data_compr_t {
	int objs_compr_cnt;
	char* objs_compr_raw;
	int objs_cnt;
	char* inds_raw;
} gsff_map_data_compr_t;

typedef struct gsff_map_data_plain_t {
    int objs_cnt;
    char* objs_raw;
} gsff_map_data_plain_t;

/// Logical Map
typedef struct gsff_logical_map_tl_t {
    int size;
    int elems_compr_cnt;
    char* elems_compr_raw;
    int elems_cnt;
    char* inds_raw;
} gsff_logical_map_tl_t;

gsfformat_e gsff_get_fileformat(char* ext);
void gsff_init_from_str_JSON(game_t* game, char* str);
void gsff_init_from_str_JSON_v1_1(game_t* game, char* str);
void gsff_init_from_str_JSON_v1_2(game_t* game, char* str);

void gsff_init_from_str_CFG(game_t* game, char* str);

char* gsffc_convert_str(gsfformat_e src_ff, char* src_str, gsfformat_e dest_ff);
char* gsffc_convert_str_CFG_to_JSON(char* src_str);

/*
#define gsff_invalid_param(s, v, m) \
	printf("[E] " s ": %d" m "\n", v); return

#define gsff_check_greater0(v) \
	if (v < 0) \
#define gsff_check_interval(v)

#define gsff_check_players_cnt(v) \
	if (v < 1) { gsff_invalid_players_cnt(v); } while(0)

#define gsff_invalid_players_cnt(v) \
	gsff_invalid_param("Invalid players count", v, "Must be greater of equal than 1.")
*/

int gsff_parse_int(char* str, int* val);
int gsff_parse_uint(char* str, unsigned int* val);


void gsff_init_players(game_t* game, gsff_players_raw_t* players_raw);
bool gsff_verify_players(gsff_players_raw_t* players_raw);
void gsff_init_players_positions(game_t* game, char* pp_raw);


void gsff_init_sqns(game_t* game);
void gsff_init_sqns_compr(game_t* game, gsff_sqns_raw_t* sraw);
// void gsff_init_sqns_compr(game_t* game,
// 	int acts_compr_cnt, char* acts_compr_raw, char* sqns_ind_raw);
bool gsff_verify_sqns(gsff_sqns_raw_t* sraw);

void gsff_init_sqn(sqn_t* sqn, size_t cnt, char* acts_raw);
void gsff_init_act(sqn_t* sqn, char* act_raw);

// void gsff_init_map(game_t* game, gsff_map_raw_t* mraw);
void gsff_init_map(game_t* game, gsff_map_raw_t* mraw);

void gsff_init_map_compr(game_t* game, gsff_map_raw_t* mraw);
// bool gsff_verify_map_compr(gsff_map_raw_t* mraw);

void gsff_init_map_plain(game_t* game, gsff_map_raw_t* mraw);
// bool gsff_verify_map_plain(gsff_map_raw_t* mraw);


// void gsff_init_objs(size_t cnt,
//     obj_base_t** objs_list, char* objs);
// void gsff_init_obj(obj_base_t** pobj, char* obj_raw);
// void gsff_init_objs_inds(map_t* map, obj_base_t** objs_compr, char* inds);

void gsff_init_lmap(game_t* game, gsff_logical_map_tl_t* lmraw);
bool gsff_verify_lmap(gsff_logical_map_tl_t* lmraw);

#endif /* end of include guard: GSFF_H */
