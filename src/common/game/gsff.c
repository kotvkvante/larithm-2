#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "common/game/game.h"
#include "common/game/gsff.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"
#include "common/objs/base.h"
#include "common/objs/wall.h"
#include "common/objs/lever.h"
#include "common/objs/door.h"
#include "common/objs/star.h"

#include "common/player/player.h"
#include "common/player/sequence.h"

// #include "common/parser/conf_reader.h"

#include "utils/utils.h"


char* GSFF_TO_STR[] = {
	[LF_JSON] = "la2j",
	[LF_CFG]  = "la2c",
};

gsfformat_e gsff_get_fileformat(char* ext)
{
	IF_FF_RETURN_CODE(ext, LF_JSON, GSFF_TO_STR);
	IF_FF_RETURN_CODE(ext, LF_CFG, GSFF_TO_STR);

	return LF_UNDEFINED;
}


int gsff_parse_int(char* str, int* val)
{
	// str = dropSpace(str);
	return (sscanf(str, "%d", val) == 1);
}

/// Нужно: Как красиво и безопасно сканировать положительное число ??
int gsff_parse_uint(char* str, unsigned int* val)
{
	int temp;
	int res = gsff_parse_int(str, &temp);
	if (!res || (temp < 0)) { return false; }
	*val = temp;
	return true;
}

/// Вариант `gsff_parse_uint`
// int gsff_parse_uint(char* str, unsigned int* val)
// {
// 	int temp;
// 	int res = gsff_parse_int(str, &temp);
// 	if (!res || (temp < 0)) { return false; }
// 	*val = temp;
// 	return true;
// }

int gsff_parse_luint(char* str, long unsigned int* val)
{
	// str = dropSpace(str);
	return (sscanf(str, "%lu", val) == 1);
}

/// player count, player current, player positions
void gsff_init_players(game_t* game, gsff_players_raw_t* players_raw)
{
	if (!gsff_verify_players(players_raw)) {
		exit(-1);
	}

    game->player_cur   = (size_t)(players_raw->cur);
    game->players_cnt  = (size_t)(players_raw->cnt);
    game->players_list = players_list_new(game->players_cnt);

	gsff_init_players_positions(game, players_raw->positions);
}

bool gsff_verify_players(gsff_players_raw_t* players_raw)
{
	if (players_raw->cnt <= 0) {
		/// Нужно: Обработка ошибки
		printf("[E] cnt <= 0");
		return false;
	}
	if ((players_raw->cur >= players_raw->cnt)
		|| (players_raw->cur < 0)) {
		/// Нужно: Обработка ошибки
		printf("[E] (cur >= pcnt) || (cur < 0)");
		return false;
	}
	if (players_raw->positions == NULL) {
		/// Нужно: Обработка ошибки
		printf("[E] positions == NULL");
		return false;
	}
	return true;
}

void gsff_init_players_positions(game_t* game, char* pp_raw)
{
	int x, y;
	size_t i = 0;
	char* str = strtok(pp_raw, ",");

	while (str != NULL)
	{
		if (i == game->players_cnt) {
			printf("[E] Too much players positions to process: Must be exactly %ld .\n", game->players_cnt);
			return;
		}

		if (!gsff_parse_int(str, &x)) {
			goto failed_to_parse;
		}

		str = strtok(NULL, ",");
		if (!gsff_parse_int(str, &y)) {
			goto failed_to_parse;
		}

		player_init(&game->players_list[i], i, x, y);
		str = strtok(NULL, ",");
		i++;
	}

	if (i < game->players_cnt) {
		printf("[E] Not enough players positions to process: Must be exactly %ld.\n", game->players_cnt);
		return;
	}

	return;

	failed_to_parse:
		printf("[e] Failed to parse to players positions: \n\t`%s`\n", pp_raw);
}

void gsff_init_sqns(game_t* game)
{
	LA2_UNUSED(game);
	LA2_NOTIMPL();
}

// void gsff_init_sqns_compr(game_t* game, int acts_compr_cnt, char* acts_compr_raw, char* sqns_ind_raw)
void gsff_init_sqns_compr(game_t* game, gsff_sqns_raw_t* sraw)
{
	if (!gsff_verify_sqns(sraw)) {
		exit(-1);
	}

	game->sqns_list = sqns_list_new(game->players_cnt);

	sqn_t* sqn_compr = sqn_new();
	gsff_init_sqn(sqn_compr, sraw->acts_compr_cnt, sraw->acts_compr_raw);

	char* sp; /// sqn pointer
	char* sqn_raw = strtok_r(sraw->sqns_inds_raw, ",", &sp);
	size_t m = 0; /// Индекс текущей sqn

	while (sqn_raw != NULL)
	{
		size_t n;
		if (!gsff_parse_luint(sqn_raw, &n)) {
			/// Нужно: Обработка ошибки
			printf("[E] gsff_parse_luint(sqn_raw, &n)"); exit(-1);
		}

		if (m == game->players_cnt) {
			/// Нужно: Обработка ошибки
			printf("[E] m == game->players_cnt\n"); exit(-1);
		}

		// printf("[i] `%s` %ld %ld\n", sp, n, m);
		sqn_t* sqn = &game->sqns_list[m];
		player_set_sqn(&game->players_list[m], sqn);
		sqn_init_default(sqn);

		sqn_raw = strtok_r(NULL, ",", &sp);
		for (size_t j = 0; j < n && (sqn_raw != NULL); j++)
		{
			size_t k;
			if (!gsff_parse_luint(sqn_raw, &k)) {
				///
				printf("[E] gsff_parse_luint(sqn_raw, &k)"); exit(-1);
			}
			sqn_append_act_safe(sqn, &sqn_compr->acts_list[k]);
			sqn_raw = strtok_r(NULL, ",", &sp);
		}
		// sqn_raw = strtok_r(NULL, ",", &sp);

		// printf("[i] m = %ld\n", m);
		m++;
	}

	if (m < game->players_cnt) {
		///
		printf("[E] m < game->players_cnt\n"); exit(-1);
	}
}

bool gsff_verify_sqns(gsff_sqns_raw_t* sraw)
{
	if (sraw->acts_compr_cnt <= 0) {
		/// Нужно: Обработка ошибки
		printf("[E] acts_compr_cnt =< 0 \n");
		return false;
	}
	if (sraw->acts_compr_raw == NULL) {
		/// Нужно: Обработка ошибки
		printf("[E] acts_compr_raw == NULL \n");
		return false;
	}
	if (sraw->sqns_inds_raw == NULL) {
		/// Нужно: Обработка ошибки
		printf("[E] sqns_ind_raw == NULL \n");
		return false;
	}
	return true;
}


void gsff_init_sqn(sqn_t* sqn, size_t cnt, char* acts_raw)
{
	sqn_init_max(sqn, cnt);

	char* ap; /// action pointer
	size_t i = 0;
	// printf("[i] `%s`\n", acts_compr_raw);
	char* act_raw = strtok_r(acts_raw, ";", &ap);
	// printf("[i] `%s`\n", act_raw);

	/// Нужно: Обработка неверного количества сжатых action (i != acts_compr_cnt)
	while (act_raw != NULL)
	{
		// act_raw = dropSpace(act_raw);
		gsff_init_act(sqn, act_raw);
		act_raw = strtok_r(NULL, ";", &ap);
		// printf("[i] `%s`\n", act_raw);
		i++;
	}
	// sqn_print(sqn);
}

void gsff_init_act(sqn_t* sqn, char* act_raw)
{
	char* p;
	action_e action_type;

	p = strtok(act_raw, ",");
	if(sscanf(p, "%d", (int*)&action_type) < 1) {
		///
	}
	if ((action_type < 0) || (action_type > SQN_END)) {
		///
	}

	switch (action_type) {
		case SQN_NONE:
			break;
		case SQN_START:
		case SQN_END:
		case SQN_PLINTERACT:
			sqn_append(sqn, action_type);
			break;

		case SQN_PLOUT: {
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
			sqn_append_data(sqn, SQN_PLOUT,
				sizeof(a_plout_t), &(a_plout_t){.o = value});
		} break;

		case SQN_PLIN: {
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
			sqn_append_data(sqn, SQN_PLIN,
				sizeof(a_plin_t), &(a_plin_t){.i = value});
		} break;

		case SQN_PLMOVE: {
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
			sqn_append_data(sqn, SQN_PLMOVE,
				sizeof(a_plmove_t), &(a_plmove_t){.d = value});
		} break;
	}
}

void gsff_init_map(game_t* game, gsff_map_raw_t* mraw)
{
	switch (mraw->data_type) {
		case MAP_DATA_PLAIN:
			gsff_init_map_plain(game, mraw);
			break;

		case MAP_DATA_COMPR:
			gsff_init_map_compr(game, mraw);
			break;
	}
}


void gsff_init_lmap(game_t* game, gsff_logical_map_tl_t* lm_tl)
{
	assert(game != NULL);
	if (!gsff_verify_lmap(lm_tl)) {
		exit(-1);
	}

    lmap_init(game->logical_map, (size_t)lm_tl->size, (size_t)lm_tl->elems_cnt);

	// elem_base_t** elems_compr_list = malloc(sizeof(elem_base_t*) * elems_compr_cnt);
	// gsff_init_elems(elems_compr_cnt, elems_compr_list, elems_compr_raw);
	// gsff_init_objs_inds(game->map, objs_compr_list, inds_raw);

	// free(elems_compr_list);
}

bool gsff_verify_lmap(gsff_logical_map_tl_t* lm_tl)
{
	assert(lm_tl->size > 0);
	assert(lm_tl->elems_compr_cnt >= 0);
	assert(lm_tl->elems_cnt >= 0);
	assert(lm_tl->elems_compr_raw != NULL);
	assert(lm_tl->inds_raw != NULL);

	return true;
}
