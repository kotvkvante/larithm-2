#include <stdlib.h>
#include <stdio.h>

#include "common/parser/parser.h"
#include "common/parser/conf_reader.h"
#include "common/game/gsff.h"

char* GSFF_TO_STR[];

char* gsffc_convert_str(gsfformat_e src_ff, char* src_str, gsfformat_e dest_ff)
{
	char* dest_str = NULL;
	if ((src_ff == LF_CFG) && (dest_ff == LF_JSON)) {
		dest_str = gsffc_convert_str_CFG_to_JSON(src_str);
	}

	if (dest_str == NULL) {
		printf("[E] Failed to convert format `.%s` to format `.%s`: `%s`\n",
			GSFF_TO_STR[src_ff], GSFF_TO_STR[dest_ff], src_str);
		return NULL;
	}
	return dest_str;
}
#define GSFFC_SEP ", "
#define GSFFC_BEGIN(f) "\"" #f "\": { "
#define GSFFC_END() "}"
#define GSFFC_FIELD_INT(f) "\"" #f "\": %d"
#define GSFFC_FIELD_STR(f) "\"" #f "\": \"%s\""
#define GSFFC_FIELD_BOOL(f) "\"" #f "\": %s"

char* gsffc_convert_str_CFG_to_JSON(char* src_str)
{
    StackNode* sn = parse_str(src_str);

	const char templ_header[] =
	"\"header\": {"
	"\"fversion\": \"%s\", "
	"\"g_gameid\": %d, "
	"\"g_gamehash\": %d, "
	"\"desrc\": \"%s\", "
	"\"author\": \"%s\""
	"}";

	const char templ_players[] =
		GSFFC_BEGIN(players)
		GSFFC_FIELD_INT(cnt) GSFFC_SEP
		GSFFC_FIELD_INT(cur) GSFFC_SEP
		GSFFC_FIELD_STR(positions);
		GSFFC_END();

	const char templ_sequences[] =
		GSFFC_BEGIN(sequences)
		GSFFC_FIELD_BOOL(is_compr) GSFFC_SEP
		GSFFC_FIELD_INT(acts_compr_cnt) GSFFC_SEP
		GSFFC_FIELD_STR(acts_compr) GSFFC_SEP
		GSFFC_FIELD_STR(indices)
		GSFFC_END();

	// const char templ_body[] =
	// "\"players\": {"

	char result[1024];
	sprintf(result, templ_header, "1.1", 0, 0, "test convert cfg to json", "kotvkvante");
	printf("Result: `%s`\n", result);

	char result1[1024];
	/// Возможно: Использовать getConfigStr вместо getConfigInt
	sprintf(result1, templ_players,
		getConfigInt(sn, "players.cnt"),
		getConfigInt(sn, "players.cur"),
		getConfigStr(sn, "players.positions"));
	printf("Result: `%s`\n", result1);

	/// Небезопасно!
	char* opt[] = {"false", "true"};
	char result2[1024];
	sprintf(result2, templ_sequences,
		opt[getConfigInt(sn, "sqns.is_compr")],
		getConfigInt(sn, "sqns.acts_compr_cnt"),
		getConfigStr(sn, "sqns.acts_compr"),
		getConfigStr(sn, "sqns.inds"));
	printf("Result: `%s`\n", result2);


}
