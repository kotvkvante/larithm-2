#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common/game/game.h"
#include "common/game/gsff.h"
#include "common/player/player.h"

#include "common/map/map.h"
#include "common/map/graph.h"
#include "common/map/logical_map.h"

#include "common/parser/conf_reader.h"
#include "common/parser/parser.h"
#include "common/parser/stacknode.h"

#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

static void parse_objs_(char* objs, int objs_compr, char** objs_compr_list);
static void parse_objs_inds_(char* inds, int size, int objs_count, int* inds_list);
static void map_init_objs_(map_t* map, int objs_compr, char** objs_compr_list, size_t objs_count, int* inds_list);

static void parse_elems_(char* elems, int elems_compr, char** elems_compr_list);
static void parse_elems_inds_(char* inds, int size, int elems_count, int* inds_list);
static void lmap_init_elems_(logical_map_t* lmap, int elems_compr, char** elems_compr_list,
    size_t elems_count, int* inds_list);

// static void graph_init_(graph_t* g, int len, char* links);

static void init_players__(game_t* game, StackNode* sn);
static void init_sequences__(game_t* game, StackNode* sn);

static void init_map__(game_t* game, StackNode* sn);
static void init_lmap__(game_t* game, StackNode* sn);
static void init_graph__(game_t* game, StackNode* sn);

/// Важно: не безопасная функция.
void gsff_init_from_str_CFG(game_t* game, char* str)
{
    StackNode* sn = parse_str(str);

    init_players__(game, sn);
    init_sequences__(game, sn);
    init_map__(game, sn);
    init_lmap__(game, sn);
    init_graph__(game, sn);

    freeStack(sn);
}

static void init_players__(game_t* game, StackNode* sn)
{
    gsff_players_raw_t praw = {
        .cnt       = getConfigInt(sn, "players.cnt"),
        .cur       = getConfigInt(sn, "players.cur"),
        .positions = getConfigStr(sn, "players.positions"),
    };
    gsff_init_players(game, &praw);
}

static void init_sequences__(game_t* game, StackNode* sn)
{
    gsff_sqns_raw_t sraw = {
        .is_compr = getConfigInt(sn, "sqns.is_compr")
    };

    if (sraw.is_compr)
    {
        sraw.acts_compr_cnt = getConfigInt(sn, "sqns.acts_compr_cnt");
        sraw.acts_compr_raw = getConfigStr(sn, "sqns.acts_compr");
        sraw.sqns_inds_raw  = getConfigStr(sn, "sqns.inds");
        gsff_init_sqns_compr(game, &sraw);
    }
    else
    {
        LA2_NOTIMPL();
        exit(-1);
    }
}

static void init_map__(game_t* game, StackNode* sn)
{
    gsff_map_data_compr_t mdc = {
        .objs_compr_cnt = getConfigInt(sn, "map.objs_compr_cnt"),
        .objs_compr_raw = getConfigStr(sn, "map.objs"),
        .objs_cnt = getConfigInt(sn, "map.objs_cnt"),
        .inds_raw = getConfigStr(sn, "map.inds")
    };
    gsff_map_raw_t mraw = {
        .size   = getConfigInt(sn, "map.size"),
        .data_type = MAP_DATA_COMPR,
        .data = &mdc
    };

    gsff_init_map(game, &mraw);
}

static void init_lmap__(game_t* game, StackNode* sn)
{
    gsff_logical_map_tl_t lmraw = {
        .size            = getConfigInt(sn, "lmap.size"),
        .elems_compr_cnt = getConfigInt(sn, "lmap.elems_compr_cnt"),
        .elems_compr_raw = getConfigStr(sn, "lmap.elems"),
        .elems_cnt = getConfigInt(sn, "lmap.elems_cnt"),
        .inds_raw  = getConfigStr(sn, "lmap.inds")
    };

    lmap_init(game->logical_map, lmraw.size, lmraw.elems_cnt);

    char** elems_compr_list = malloc(sizeof(char*) * lmraw.elems_compr_cnt);
    int* elems_inds_list = malloc(sizeof(int) * lmraw.elems_cnt * 2); // * 2 for index and position

    parse_elems_(lmraw.elems_compr_raw, lmraw.elems_compr_cnt, elems_compr_list);
    parse_elems_inds_(lmraw.inds_raw, lmraw.size, lmraw.elems_cnt, elems_inds_list);

    lmap_init_elems_(game->logical_map,
        lmraw.elems_compr_cnt, elems_compr_list, lmraw.elems_cnt, elems_inds_list);

    free(elems_compr_list);
    free(elems_inds_list);
}

static void init_graph__(game_t* game, StackNode* sn)
{
    graph_t* g = &game->logical_map->g;
    char* str = getConfigStr(sn, "graph.graph");
    // printf("[i] %s\n", str);
    graph_read_from_str(g, true, str);
    graph_print(g);
}

static void parse_elems_(char* elems, int elems_compr, char** elems_compr_list)
{
    parse_objs_(elems, elems_compr, elems_compr_list);
}

static void parse_objs_(char* objs, int objs_compr, char** objs_compr_list)
{
    const char c = ';';
    int i = 0;

    objs = dropSpace(objs);
    objs_compr_list[i] = objs;

    while(*objs != '\0')
    {
        if (*objs == c)
        {
            i++;
            if (i == objs_compr) break;
            *objs='\0';
            objs++;
            objs = dropSpace(objs);
            objs_compr_list[i] = objs;
            continue;
        }
        objs++;
    }

    // // debug stuff
    // for (size_t i = 0; i < objs_compr; i++) {
    //     printf("%s\n", objs_compr_list[i]);
    // }
}

static void parse_elems_inds_(char* inds, int size, int elems_count, int* inds_list)
{
    parse_objs_inds_(inds, size, elems_count, inds_list);
}

static void parse_objs_inds_(char* inds, int size, int objs_count, int* inds_list)
{
    /// Нужно: фикс. При вызове этой функции `parse_elems_inds_` для `элементов`,
    /// выводятся сообщения об ошибке для `объектов`.
    int k = 0;
    for (size_t i = 0; i < size * size; i++)
    {
        int n, ind;
        inds = dropSpace(inds);

        int cnt = sscanf(inds, "%d %n", &ind, &n);
        if (ind != 0)
        {
            inds_list[2 * k] = ind;
            inds_list[2 * k + 1] = i;
            k++;
        }
        if (k == objs_count) {
            printf("[w] parse_objs_inds_: Objects count == k.\n");
            break;
        }

        if (cnt != 1) { printf("> [w] Fail to read '%s'.\n", inds); break; }
        inds+=n;
    }

    if (k < objs_count) {
        printf("> [W] Objects count > k.\n");
    }
}

static void init_obj_(map_t* map, char* strobj, int pos)
{
    obj_type_e obj_type;
    /// count of scanned symbols
    int n = 0;
    sscanf(strobj, "%d %n", (int*)&obj_type, &n);
    // printf("> [%s] %d \n", strobj, obj_type);

    int state = 0;
    if (*(strobj + 1) != ';') {
        /// +2 для `, `.
        /// Нужно: более красивое решение, в том
        /// числе для с большим числом параметров.
        strobj+=(n + 2);
        sscanf(strobj, "%d", &state);
    }
    map_init_obj(map, obj_type, pos % map->size, pos / map->size, state);
}

static void init_elem_(logical_map_t* lmap, char* strobj, int pos)
{
    elem_type_t elem_type;
    int n = 0; /// count of scanned symbols
    sscanf(strobj, "%d %n", (int*)&elem_type, &n);

    switch (elem_type) {
        case ELEM_TYPE_BASE:
        break;

        case ELEM_TYPE_INPUT:
        {
            elem_input_t* i = elem_input_new();
            elem_input_init_xy(i, pos % lmap->size, pos / lmap->size);
            logical_map_insert_elem(lmap, (elem_base_t*)i);
        }
        break;

        case ELEM_TYPE_OUTPUT:
        {
            elem_output_t* o = elem_output_new();
            elem_output_init_xy(o, pos % lmap->size, pos / lmap->size);
            logical_map_insert_elem(lmap, (elem_base_t*)o);
        }
        break;

        case ELEM_TYPE_LOGICAL:
        {
            elem_logical_t* l = elem_logical_new();
            /// +2 for comma and whitespace.
            /// Нужно: более красивое решение. Смотри `init_obj_`
            strobj+=(n + 2);
            logical_op_t op;
            sscanf(strobj, "%d", (int*)&op);

            elem_logical_init_xy(l, op, pos % lmap->size, pos / lmap->size);
            logical_map_insert_elem(lmap, (elem_base_t*)l);
        }
        break;

        default:
            printf("[W] Untracked elem type: '%d'.\n", elem_type);
        break;
    }
}

static void map_init_objs_(
    map_t* map,
    int objs_compr, char** objs_compr_list,
    size_t objs_count, int* inds_list)
{
    for (size_t i = 0; i < objs_count; i++) {
        int ind = inds_list[2 * i];
        int pos = inds_list[2 * i + 1];
        if (ind > objs_compr) {
            printf("[W] While reading: '%d' > objs_compr. \n", ind);
        }
        init_obj_(map, objs_compr_list[ind], pos);
    }
}

static void lmap_init_elems_(
    logical_map_t* lmap,
    int elems_compr, char** elems_compr_list,
    size_t elems_count, int* inds_list)
{
    for (size_t i = 0; i < elems_count; i++) {
        int ind = inds_list[2 * i];
        int pos = inds_list[2 * i + 1];
        if (ind > elems_compr) {
            printf("[W] While reading: '%d' > elems_compr. \n", ind);
        }
        init_elem_(lmap, elems_compr_list[ind], pos);
    }
}

// static void graph_init_(graph_t* g, int len, char* links)
// {
//     g->nvertices = len;
//     const char* delims = ",";
//     char* s = strtok(links, delims);
//     while (*s != '\0' || s != NULL)
//     {
//         int x, y;
//         if(sscanf(s, "%d", &x)!=1) break;
//         s = strtok(NULL, delims);
//         if(sscanf(s, "%d", &y)!=1) break;
//         printf("%d %d\n", x, y);
//         graph_insert_edge(g, x, y, true);
//         s = strtok(NULL, delims);
//     }
// }
