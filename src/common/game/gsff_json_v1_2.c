#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common/game/game.h"

#include "frozen.h"
#include "frozen_ex.h"

#include "common/objs/wall.h"
#include "common/objs/door.h"
#include "common/objs/lever.h"
#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

#include "common/map/map.h"
#include "common/map/graph.h"
#include "common/map/logical_map.h"

void gsff_init_from_str_JSON_v1_2(game_t* game, char* str)
{
    printf("[i] Init game from str json v1.2\n");

	struct json_token header = {0};
	struct json_token body = {0};

    char* fversion = NULL;
    json_scanf(str, strlen(str),
        "{"
		    "fversion: %Q,"
		    "header: %T,"
		    "body: %T,"
        "}",
        &fversion,
        &header,
        &body
    );

    /// Нужно ли: Двойная провека?
    // if (strcmp(fversion, "1.2") != 0) {
    //     printf("[w] Invalid file version. Required version == 1.2, given v%s.\n", fversion);
    //     printf("\t Trying to proccess file. . . \n");
    // }

	int g_gameid;
	int g_gamehash;
	char* description;
	char* author;
    json_scanf(header.ptr, header.len,
        "{"
			"g_gsid: %d,"
			"g_gshash: %d,"
			"description: %Q,"
			"author: %Q"
        "}",
		&g_gameid,
		&g_gamehash,
		&description,
		&author
    );

	// printf("[i] fversion = %s\n", fversion);
	// printf("[i] g_gameid = %d\n", g_gameid);
	// printf("[i] g_gamehash = %d\n", g_gamehash);
	// printf("[i] description = %s\n", description);
	// printf("[i] author = %s\n", author);

	// printf("%d `%.*s`\n", header.type, header.len, header.ptr);
	// printf("%d `%.*s`\n", body.type, body.len, body.ptr);

    struct json_token jt_players = {0};
    struct json_token jt_sequences = {0};
    struct json_token jt_map = {0};
    struct json_token jt_lmap = {0};
    struct json_token jt_graph = {0};

    json_scanf(body.ptr, body.len,
        "{"
	        "players: %T,"
	        "sequences: %T,"
	        "map: %T,"
	        "lmap: %T,"
	        "graph: %T"
        "}",
        &jt_players,
        &jt_sequences,
        &jt_map,
        &jt_lmap,
        &jt_graph
    );

	gsff_players_raw_t praw;
    json_scanf(jt_players.ptr, jt_players.len,
        "{"
	        "cnt: %d,"
	        "cur: %d,"
	        "positions: %Q"
        "}",
		&praw.cnt,
		&praw.cur,
		&praw.positions
	);

    gsff_init_players(game, &praw);

	// printf("%d `%.*s`\n", jt_sequences.type, jt_sequences.len, jt_sequences.ptr);

    gsff_sqns_raw_t sraw;
	json_scanf(jt_sequences.ptr, jt_sequences.len,
        "{"
	        "is_compr: %B,"
	        "acts_compr_cnt: %d,"
	        "acts_compr_raw: %Q,"
	        "indices: %Q"
        "}",
		&sraw.is_compr,
		&sraw.acts_compr_cnt,
		&sraw.acts_compr_raw,
		&sraw.sqns_inds_raw
	);
	// printf("%d\n", acts_compr_cnt);
	// gsff_init_sqns_compr(game, acts_compr_cnt, acts_compr_raw, sqns_inds_raw);
    if (sraw.is_compr) gsff_init_sqns_compr(game, &sraw);
    else LA2_NOTIMPL();

	/// Map
    struct json_token jt_map_data;
	gsff_map_raw_t mraw = {0};

	json_scanf(jt_map.ptr, jt_map.len,
        "{"
	        "size: %d,"
	        "data_type: %Q,"
	        "data: %T"
        "}",
		&mraw.size,
		&mraw.data_type_raw,
		&jt_map_data
    );

    if (strcmp(mraw.data_type_raw, "compressed") == 0) {
        mraw.data_type = MAP_DATA_COMPR;
        gsff_map_data_compr_t mcd; /// map compressed data
        json_scanf(jt_map_data.ptr, jt_map_data.len,
            "{"
            "objs_compr_cnt: %d,"
            "objs_compr: %Q,"
            "objs_cnt: %d,"
            "inds: %Q"
            "}",
            &mcd.objs_compr_cnt,
            &mcd.objs_compr_raw,
            &mcd.objs_cnt,
            &mcd.inds_raw
        );
        mraw.data = &mcd;
    } else if (strcmp(mraw.data_type_raw, "plain") == 0) {
        mraw.data_type = MAP_DATA_PLAIN;
        gsff_map_data_plain_t mpd; /// map compressed data
        json_scanf(jt_map_data.ptr, jt_map_data.len,
            "{"
            "objs_cnt: %d,"
            "objs: %Q"
            "}",
            &mpd.objs_cnt,
            &mpd.objs_raw
        );
        mraw.data = &mpd;
        // printf(">>> %d\n", mpd.objs_cnt);
        // printf(">>> %s\n", mpd.objs_raw);
    } else {
        LA2_NOTIMPL();
        exit(-1);
    }
    gsff_init_map(game, &mraw);

    /// Logical Map
    gsff_logical_map_tl_t lm_tl;
	json_scanf(jt_lmap.ptr, jt_lmap.len,
		"{"
        "size: %d,"
		"elems_compr_cnt: %d,"
		"elems_compr: %Q,"
		"elems_cnt: %d,"
		"inds: %Q"
		"}",
        &lm_tl.size,
		&lm_tl.elems_compr_cnt,
		&lm_tl.elems_compr_raw,
		&lm_tl.elems_cnt,
		&lm_tl.inds_raw
	);

    gsff_init_lmap(game, &lm_tl);

    /// Graph
    char* graph = NULL;
    json_scanf(jt_graph.ptr, jt_graph.len,
        "{"
        "graph: %Q"
        "}",
        &graph
    );
    graph_t* g = &game_get_lmap(game)->g;
    graph_read_from_str(g, true, graph);
}
