#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "frozen.h"
#include "frozen_ex.h"

#include "common/game/game.h"

#include "common/objs/wall.h"
#include "common/objs/door.h"
#include "common/objs/lever.h"
#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

#include "common/map/map.h"
#include "common/map/graph.h"
#include "common/map/logical_map.h"
// #include "common/level/level.h"

// static void scan_array(const char *str, int len, void *user_data);
static void scan_wall_array  (const char *str, int len, void *data);
static void scan_door_array  (const char *str, int len, void *data);
static void scan_lever_array (const char *str, int len, void *data);
static void scan_elem_input_array   (const char *str, int len, void *data);
static void scan_elem_output_array  (const char *str, int len, void *data);
static void scan_elem_logical_array (const char *str, int len, void *data);
static void scan_graph_links_array(const char *str, int len, void *data);

/// Важно: не безопасная функция.
void gsff_init_from_str_JSON_v1_1(game_t* game, char* str)
{
    printf("[i] JSON v1_1\n");
    LA2_NOTIMPL();
    // assert(game != NULL);
    // assert(str != NULL);
    //
    //
    // char* header;
    // char* body;
    //
    // char* fversion;
	// int g_gameid, g_gamehash;
	// char* descr;
	// char* author;
    //
    // int cnt, cur;
    // char* positions_raw;
    //
    // /// sequence
    // bool is_compr;
    // int acts_compr_cnt;
    // char* acts_compr_raw;
    // char* indices_raw;
    //
    // int map_size;
    // int objs_compr_cnt;
    // char* objs_compr_raw;
    // int objs_cnt;
    // char* map_inds_raw;
    //
    // int lmap_size;
    // int elems_compr_cnt;
    // char* elems_compr_raw;
    // int elems_cnt;
    // char* lmap_inds_raw;
    //
    // char* graph_raw;
    //
    // json_scanf(str, strlen(str),
    //     "{"
    //     "header:{fversion: %Q, g_gameid: %d, g_gamehash: %d, descr: %Q, author: %Q}"
    //     "body: {"
    //         "players:  {cnt: %d, cur: %d, positions: %Q},"
    //         "sequences:  {is_compr: %B, acts_compr_cnt: %d, acts_compr: %Q, indices: %Q},"
    //         "map:  {size: %d, objs_compr_cnt: %d, objs_compr: %Q, objs_cnt: %d, inds: %Q},"
    //         "lmap:  {size: %d, elems_compr_cnt: %d, elems_compr: %Q, elems_cnt: %d, inds: %Q},"
    //         "graph: {graph: %Q}"
    //     "}",
    //     /// header
    //     &fversion,
    //     &g_gameid,
    //     &g_gamehash,
    //     &descr,
    //     &author,
    //     /// sequence
    //     &cnt,
    //     &cur,
    //     &positions_raw,
    //
    //     &is_compr,
    //     &acts_compr_cnt,
    //     &acts_compr_raw,
    //     &indices_raw,
    //
    //     &map_size,
    //     &objs_compr_cnt,
    //     &objs_compr_raw,
    //     &objs_cnt,
    //     &map_inds_raw,
    //
    //     &lmap_size,
    //     &elems_compr_cnt,
    //     &elems_compr_raw,
    //     &elems_cnt,
    //     &lmap_inds_raw,
    //     /// graph raw
    //     &graph_raw
    // );
    //
    // // printf("[i] Init JSON `fversion`   : %s\n", fversion);
    // // printf("[i] Init JSON `g_gameid`   : %d\n", g_gameid);
    // // printf("[i] Init JSON `g_gamehash` : %d\n", g_gamehash);
    // // printf("[i] Init JSON `descr`      : %s\n", descr);
    // // printf("[i] Init JSON `author`     : %s\n", author);
    // //
    // // printf("[i] Init JSON `cnt`: %d\n", cnt);
    // // printf("[i] Init JSON `cur`: %d\n", cur);
    // // printf("[i] Init JSON `positions_raw`: %s\n", positions_raw);
    // //
    // // printf("[i] Init JSON `is_compr`      : %d\n", is_compr);
    // // printf("[i] Init JSON `acts_compr_cnt`: %d\n", acts_compr_cnt);
    // // printf("[i] Init JSON `acts_compr_raw`: %s\n", acts_compr_raw);
    // // printf("[i] Init JSON `indices_raw`   : %s\n", indices_raw);
    // //
    // // printf("[i] Init JSON `size`          : %d\n", map_size);
    // // printf("[i] Init JSON `objs_compr_cnt`: %d\n", objs_compr_cnt);
    // // printf("[i] Init JSON `objs_compr`    : %s\n", objs_compr_raw);
    // // printf("[i] Init JSON `objs_cnt`      : %d\n", objs_cnt);
    // // printf("[i] Init JSON `inds`          : %s\n", map_inds_raw);
    // //
    // // printf("[i] Init JSON `size`           : %d\n", lmap_size);
    // // printf("[i] Init JSON `elems_compr_cnt`: %d\n", elems_compr_cnt);
    // // printf("[i] Init JSON `elems_compr`    : %s\n", elems_compr_raw);
    // // printf("[i] Init JSON `elems_cnt`      : %d\n", elems_cnt);
    // // printf("[i] Init JSON `inds`           : %s\n", lmap_inds_raw);
    // //
    // // printf("[i] Init JSON `graph_raw`      : %s\n", graph_raw);
    //
    // gsff_init_players(game, cnt, cur, positions_raw);
    // gsff_init_sqns_compr(game,
    //     acts_compr_cnt, acts_compr_raw, indices_raw);
    //
    // gsff_init_map(game,
    //     map_size, objs_compr_cnt, objs_compr_raw, objs_cnt, map_inds_raw);
    //
    // gsff_init_lmap(game,
    //     lmap_size, elems_compr_cnt, elems_compr_raw, elems_cnt, lmap_inds_raw);
    //
    //
    // game->logical_map = logical_map_new();
    // graph_t* g = &game->logical_map->g;
    // graph_read_from_str(g, true, graph_raw);
}

static void scan_wall_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i = 0;

    map_t* map = (map_t*)data;

    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
        int value = atoi(t.ptr);
        if (value == 0) continue;

        obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, i % map->size, i / map->size);

        map_set_obj(map, value, (obj_base_t*)wall);
    }
}

static void scan_door_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i;

    map_t* map = (map_t*)data;

    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        int value, state;
        value = atoi(t.ptr);

        i++;
        json_scanf_array_elem(str, len, "", i, &t);

        state = atoi(t.ptr);

        obj_door_t* door = obj_door_new();
            obj_door_init(door, value % map->size, value / map->size, state);
            // printf("(%d %d %d)\n",  value / map->size, value % map->size, state);

        map_set_obj(map, value, (obj_base_t*)door);
    }
}

static void scan_lever_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i;

    map_t* map = (map_t*)data;

    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        int value, state;
        value = atoi(t.ptr);

        i++;
        json_scanf_array_elem(str, len, "", i, &t);

        state = atoi(t.ptr);

        obj_lever_t* lever = obj_lever_new();
            obj_lever_init(lever, value % map->size, value / map->size, state);
            // printf("(%d %d %d)\n",  value / map->size, value % map->size, state);

        map_set_obj(map, value, (obj_base_t*)lever);
    }
}

static void scan_elem_input_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i;

    logical_map_t* lm = (logical_map_t*)data;

    int value;
    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        // printf(">> !! HERE \n");
        value = atoi(t.ptr);

        elem_input_t* e = elem_input_new();
            elem_input_init_xy(e, value % lm->size, value / lm->size);
            // printf("> Elem: {%d} %d %d: \n", value, e->x, e->y);

        logical_map_insert_elem(lm, (elem_base_t*)e);
    }
}

static void scan_elem_output_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i;

    logical_map_t* lm = (logical_map_t*)data;

    int value;
    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        value = atoi(t.ptr);

        elem_output_t* e = elem_output_new();
            elem_output_init_xy(e, value % lm->size, value / lm->size);

        logical_map_insert_elem(lm, (elem_base_t*)e);
    }
}

static void scan_elem_logical_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i;

    logical_map_t* lm = (logical_map_t*)data;

    int value, logical_op;
    for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        value = atoi(t.ptr);
        i++;

        json_scanf_array_elem(str, len, "", i, &t);
        logical_op = atoi(t.ptr);

        if ((logical_op < 0) || (logical_op >= LOGICAL_OP_COUNT))
            printf("> Invalid logical op: %d\n", logical_op);

        elem_logical_t* e = elem_logical_new();
            elem_logical_init_xy(e, logical_op, value % lm->size, value / lm->size);

        logical_map_insert_elem(lm, (elem_base_t*)e);
    }
}

static void scan_graph_links_array(const char *str, int len, void *data)
{
    struct json_token t;
    int i = 0;
    graph_t* g = (graph_t*)data;


    json_scanf_array_elem(str, len, "", i, &t);
    i++;
    g->nvertices = atoi(t.ptr);

    int x, y;
    for (; json_scanf_array_elem(str, len, "", i, &t) > 0; i++)
    {
        x = atoi(t.ptr);
        i++;

        json_scanf_array_elem(str, len, "", i, &t);
        y = atoi(t.ptr);

        graph_insert_edge(g, x, y, true);
        // printf("%d %d\n", x, y);
    }
    graph_print(g);
}
