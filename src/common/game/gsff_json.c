#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "frozen.h"

#include "common/game/gsff.h"
#include "common/game/game.h"

#define PROCESS_JSON_FF_VERSION(game, fversion, fversion_code, str, handler) \
	if (strcmp(fversion, fversion_code) == 0) { \
		handler(game, str); \
		return; \
	} while(0)

void gsff_init_from_str_JSON(game_t* game, char* str)
{
    assert(game != NULL);
    assert(str != NULL);

	char* fversion = NULL;
	json_scanf(str, strlen(str),
        "{"
			"fversion: %Q"
		"}",
		&fversion
	);

	PROCESS_JSON_FF_VERSION(game,
		fversion, "1.1", str, gsff_init_from_str_JSON_v1_1);
	PROCESS_JSON_FF_VERSION(game,
		fversion, "1.2", str, gsff_init_from_str_JSON_v1_2);
	printf("[w] Not supported json ff version!\n");
}
