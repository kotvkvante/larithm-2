#include <string.h>

#include "common/player/sequence.h"
#include "common/player/sqnm.h"
#include "common/player/player.h"

#include "frozen.h"
#include "common/map/map.h"
#include "common/map/logical_map.h"

#include "common/objs/base.h"
#include "common/objs/wall.h"
#include "common/objs/door.h"
#include "common/objs/lever.h"
#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

#include "common/level/levels_list.h"
#include "common/map/map.h"
#include "common/game/game.h"
#include "utils/utils.h"

game_t* game_new()
{
    game_t* game = malloc(sizeof(game_t));
    game->map           = map_new();
    game->logical_map   = logical_map_new();
    game->players_list  = NULL;
    game->sqns_list     = NULL;
    game->player_cur    = 0;
    game->players_cnt   = 0;
    return game;
}

void game_init_from_file(game_t* game, char* filepath)
{
    const char* ext = get_filename_ext(filepath);

    gsfformat_e ff = gsff_get_fileformat((char*)ext);
    if (ff == LF_UNDEFINED) {
    	printf("[w] Undefined file format: '%s' [%s].\n", filepath, ext);
    }

    char* str = file_read(filepath);
    if(str == NULL)
    {
        printf("[w] `%s`: Unable to read file: '%s'.\n", __func__, filepath);
        return;
    }

    game_init_from_str(game, str, ff);
    game_set_need_sync(game);
    free(str);

    {
        sqnm_t* sm = sqnm_new();
        sqnm_init_default(sm);
        game->sqnm = sm;
    }
}

void game_init_from_str(game_t* game, char* str, gsfformat_e ff)
{
    switch (ff) {
        case LF_JSON:
            gsff_init_from_str_JSON(game, str);
            break;

        case LF_CFG:
            gsff_init_from_str_CFG(game, str);
            break;

        case LF_UNDEFINED:
            printf("> [w] Undefined level format.\n");
            break;

        default:
            printf("> [w] Untracked gamesave format.\n");
            break;
    }
}

void game_set_need_sync(game_t* game)
{
    game->need_sync = true;
}

bool game_get_need_sync(game_t* game)
{
    return game->need_sync;
}

void game_sync_maps(game_t* game)
{
    int index;
    obj_base_t* o;
    elem_base_t* e;

    map_t* map = game->map;
    logical_map_t* lm = game->logical_map;

    for (size_t i = 1; i < map->objs_count; i++) {
        o = map->objs_list[i];
        if (o->type == OBJ_TYPE_LEVER)
        {
            index = MAP_COORD_TO_INDEX(o->x, o->y);
            e = lm->elements_list[ lm->elements_indices[index] ];
            if(e == LOGICAL_MAP_NULL_ELEM) continue;
            if(e->type == ELEM_TYPE_INPUT)
            {
                elem_input_set_value((elem_input_t*)e, ((obj_lever_t*)o)->state);
            }
        }
    }

    for (size_t i = 1; i < lm->elements_count; i++) {
        e = lm->elements_list[i];
        if (e->type == ELEM_TYPE_OUTPUT)
        {
            edgenode_t* p = lm->g.edges[i];
            elem_output_set_value(
                (elem_output_t*)e,
                logical_map_compute_node(lm, p->y));

            o = map_get_obj_by_coord(map, e->x, e->y);
            if (o == MAP_NULL_OBJ) continue;
            switch (o->type)
            {
                case OBJ_TYPE_DOOR:
                {
                    obj_door_set_state((obj_door_t*)o, ((elem_output_t*)e)->value);
                } break;

                default: break;
            }
        }
    }

    game->need_sync = false;
}


map_t* game_get_map(game_t* game)
{
    return game->map;
}

logical_map_t* game_get_lmap(game_t* game)
{
    return game->logical_map;
}

player_t* game_get_current_player(game_t* game)
{
    return &game->players_list[game->player_cur];
}

size_t game_get_curplayer_index(game_t* game)
{
    return game->player_cur;
}

void game_compute(game_t* game)
{
    while(game_step_next(game)) {
        /* code */
    }
}

void game_compute_reverse(game_t* game)
{
    while (game_step_prev(game)) {
        /* code */
    }
}

bool game_check_win(game_t* game)
{
    map_t* map = game_get_map(game);
    bool c1 = (map->score) == (map->score_goal);

    /// Нужно отдельная проверка при game->players_cnt 1 игрока.

    player_t* player_cur = game_get_current_player(game);
    sqn_t* sqn_cur = player_get_sqn(player_cur);
    bool c2 = sqn_cur->acts_list[sqn_cur->act_next - 1].action == SQN_END;

    bool c3 = true;
    for (size_t i = 0; i < game->players_cnt; i++) {
        sqn_t* sqn = &game->sqns_list[i];
        if (sqn->acts_count != sqn->act_next) {
            c3 = false;
        }
    }

    // printf("> %d\n", c1);
    // printf("> %d\n", c2);
    // printf("> %d\n", c3);

    return c1 && c2 && c3;
}

bool game_step_next(game_t* game)
{
    map_t* map = game_get_map(game);
    player_t* p = game_get_current_player(game);
    sqn_t* sqn = player_get_sqn(p);
    bool result = false;

    action_t* m;
    if (!sqn_next(sqn, &m)) return false;

    switch (m->action) {
        case SQN_NONE: {
            result = true;
        } break;

        case SQN_START: {
            result = true;
        } break;

        case SQN_PLMOVE: {
            a_plmove_t* mplm = (a_plmove_t*)(m->data);
            player_move(p, mplm->d);
            int x = player_get_x(p);
            int y = player_get_y(p);
            result = map_is_valid_player_pos(map, x, y);
        } break;

        case SQN_PLINTERACT: {
            int x = player_get_x(p);
            int y = player_get_y(p);
            result = map_is_tile_interactable(map, x, y);
            if (result) {
                map_tile_interact(map, x, y);
                game_set_need_sync(game);
            }
        } break;

        case SQN_PLIN: {
            result = true;
        } break;

        case SQN_PLOUT: {
            a_plout_t* out = (a_plout_t*)(m->data);

            result = game_switch_player(game, out->o);
            if (!result) break; /// No switching is necessary

            player_t* cp = game_get_current_player(game);
            sqn_t* csqn = player_get_sqn(cp);
            sqn_inc(csqn);
        } break;

        case SQN_END: {
            result = true;
        } break;

        default: {
            printf("[w] Untracked case: %s\n", __func__);
            result = false;
        } break;
    }

    return result;
}

/// Reverse direciton
direction_e revdir[4] = {
    [LEFT] = RIGHT,
    [RIGHT] = LEFT,
    [UP] = DOWN,
    [DOWN] = UP,
};

bool game_step_prev(game_t* game)
{
    map_t* map = game_get_map(game);
    player_t* p = game_get_current_player(game);
    sqn_t* sqn = player_get_sqn(p);
    bool result = false;

    action_t* m;
    if (!sqn_prev(sqn, &m)) return false;

    switch (m->action) {
        case SQN_NONE: {
            result = true;
        } break;

        case SQN_START: {
            result = true;
        } break;

        case SQN_PLMOVE: {
            a_plmove_t* mplm = (a_plmove_t*)(m->data);
            player_move(p, revdir[mplm->d]);
            int x = player_get_x(p);
            int y = player_get_y(p);
            result = map_is_valid_player_pos(map, x, y);
        } break;

        case SQN_PLINTERACT: {
            int x = player_get_x(p);
            int y = player_get_y(p);
            result = map_is_tile_interactable_reverse(map, x, y);
            if (result) {
                map_tile_interact_reverse(map, x, y);
                game_set_need_sync(game);
            }
        } break;

        case SQN_PLIN: {
            a_plin_t* in = (a_plin_t*)(m->data);

            result = game_switch_player(game, in->i);
            if (!result) break; /// if no switching is necessary

            player_t* cp = game_get_current_player(game);
            sqn_t* csqn = player_get_sqn(cp);
            sqn_dec(csqn);
        } break;

        case SQN_PLOUT: {
            result = true; /// seems never reached ...?
        } break;

        case SQN_END: {
            result = true; /// seems never reached ...?
        } break;

        default: {
            printf("> Untracked action: \n");
            sqn_action_info(m);
            result = false;
        } break;
    }

    return result;
}

bool game_switch_player(game_t* game, size_t pid)
{
    /// pid = 0 is invalid player
    /// player id is out of bounds
    // if (pid == 0) return false;
    if (pid >= game->players_cnt) return false;
    if (pid == game->player_cur) return false;

    game->player_cur = pid;

    return true;
}

void game_next_player(game_t* game)
{
    if (game->player_cur == game->players_cnt-1) return;
    game->player_cur++;
}

void game_prev_player(game_t* game)
{
    if (game->player_cur == 0) return;
    game->player_cur--;
}

void game_info(game_t* game)
{
    printf("[i] Game [%p]: \n", (void*)game);
    printf("\tneed_sync = %d\n", game->need_sync);
    printf("\tmap = %p\n", (void*)game->map);
    printf("\tlmap = %p\n", (void*)game->logical_map);
    printf("\tplayer_cur = %ld\n", game->player_cur);
    printf("\tplayers_cnt = %ld\n", game->players_cnt);
    printf("\tplayers_list = %p\n", (void*)game->players_list);
    printf("\tsqns_list = %p\n", (void*)game->sqns_list);
}


void game_free(game_t* game)
{
    LA2_UNUSED(game);
    LA2_NOTIMPL();
}
