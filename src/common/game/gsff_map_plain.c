#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "common/game/game.h"
#include "common/game/gsff.h"

#include "common/objs/base.h"
#include "common/objs/wall.h"
#include "common/objs/lever.h"
#include "common/objs/door.h"
#include "common/objs/star.h"
#include "common/map/map.h"

static bool gsff_verify_map_plain(gsff_map_raw_t* mraw);
static void gsff_init_objs(map_t* map, char* objs_raw);
static void gsff_init_obj(map_t* map, char* obj_raw);

void gsff_init_map_plain(game_t* game, gsff_map_raw_t* mraw)
{
	assert(game != NULL);
	if (!gsff_verify_map_plain(mraw)) {
		exit(-1);
	}

	gsff_map_data_plain_t* mdp = (gsff_map_data_plain_t*)mraw->data;
	map_init(game->map, (size_t)mraw->size, (size_t)mdp->objs_cnt);

	gsff_init_objs(game->map, mdp->objs_raw);
}

static bool gsff_verify_map_plain(gsff_map_raw_t* mraw)
{
	assert(mraw->size > 0);

	gsff_map_data_plain_t* mdp = (gsff_map_data_plain_t*)mraw->data;
	assert(mdp->objs_cnt >= 0);
	assert(mdp->objs_raw != NULL);

	return true;
}

static void gsff_init_objs(map_t* map, char* objs_raw)
{
	const char delims[] = ";";
	int ind = 0, k = 0;
	char* p;
	char* obj_raw = strtok_r(objs_raw, delims, &p);
	while (obj_raw != NULL)
	{
		printf("[i] k = %d, obj_raw = `%s`\n", k, obj_raw);
		gsff_init_obj(map, obj_raw);
		obj_raw = strtok_r(NULL, delims, &p);
		k++;
		if (k > map->objs_max - 1)
		{
			printf("[w] k == map->objs_max \n");
			return;
		}
	}

	if (k < map->objs_max - 1)
	{
		///
		printf("[w] k < map->objs_max \n");
		return;
	}
}

static void gsff_init_obj(map_t* map, char* obj_raw)
{
	obj_type_e obj_type;
	char* p = strtok(obj_raw, ",");

	if (sscanf(p, "%d", (int*)&obj_type) != 1) {
		printf("[w] Failed to scan object type: `%s`. Trying to scan next object. . .\n ", obj_raw);
		return;
	}

	if ((obj_type < 0) || (obj_type >= OBJ_TYPE_COUNT)) {
		printf("[E] Not supported object type `%d`. Trying to scan next object. . .\n", obj_type);
		return;
	}

	int x, y;
	p = strtok(NULL, ",");
	if (sscanf(p, "%d", &x) != 1) {
		printf("[E] %d: %s\n", __LINE__, __func__);
		exit(-1);
	}

	p = strtok(NULL, ",");
	if (sscanf(p, "%d", &y) != 1) {
		printf("[E] %d: %s\n", __LINE__, __func__);
		exit(-1);
	}

	if (!map_is_on_map(map, x, y)) {
		printf("[E] %d: %s\n", __LINE__, __func__);
		exit(-1);
	}


	obj_base_t** pobj;
    switch (obj_type) {
        case OBJ_TYPE_BASE: {
			*pobj = obj_base_new();
			obj_base_init(*pobj, OBJ_TYPE_BASE, x, y);
		} break;

        case OBJ_TYPE_WALL: {
            obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, x, y);
			pobj = (obj_base_t**)&wall;
        } break;

        case OBJ_TYPE_DOOR: {
            obj_door_t* door = obj_door_new();
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
            obj_door_init(door, x, y, value);
			pobj = (obj_base_t**)&door;
        } break;

        case OBJ_TYPE_LEVER: {
            obj_lever_t* lever = obj_lever_new();
			int value;
			p = strtok(NULL, ",");
			sscanf(p, "%d", (int*)&value);
            obj_lever_init(lever, x, y, value);
			pobj = (obj_base_t**)&lever;
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_t* star = obj_star_new();
            obj_star_init(star, x, y);
			pobj = (obj_base_t**)&star;
			
			map->score_goal++;
        } break;

		/// Недостигается
        // default: {
        //     printf("[w] `%s`: untracked obj type: '%d'.\n", __func__, obj_type);
        // } break;
    }

	map_set_obj_xy(map, x, y, *pobj);
}
