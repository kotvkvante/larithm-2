#ifndef ELEM_INPUT_H
#define ELEM_INPUT_H

#include <stdbool.h>
#include "base.h"

typedef struct elem_input_t {
    elem_base_t;
    bool value;
} elem_input_t;

elem_input_t* elem_input_new();
void elem_input_init(elem_input_t* i, bool value);

void elem_input_init_xy(elem_input_t* i, int x, int y);

void elem_input_set_value(elem_input_t* i, bool value);

#endif /* end of include guard: ELEM_INPUT_H */
