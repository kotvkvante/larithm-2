#ifndef ELEM_OUTPUT_H
#define ELEM_OUTPUT_H

#include <stdbool.h>
#include "base.h"

typedef struct elem_output_t {
    elem_base_t;
    bool value;
} elem_output_t;

elem_output_t* elem_output_new();

void elem_output_init_xy(elem_output_t* i, int x, int y);
void elem_output_set_value(elem_output_t* o, bool value);

#endif /* end of include guard: ELEM_OUTPUT_H */
