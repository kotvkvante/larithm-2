#ifndef ELEM_TYPES_H
#define ELEM_TYPES_H

#include "../../utils/utils.h"

#define LOGICAL_MAP_NULL_INDEX 0
#define LOGICAL_MAP_NULL_ELEM NULL

#define ELEM_DEFAULT_TEXTURE 0

typedef enum elem_type_t
{
    ELEM_TYPE_BASE    = 0,
    ELEM_TYPE_INPUT   = 1,
    ELEM_TYPE_OUTPUT  = 2,
    ELEM_TYPE_LOGICAL = 3,
    ELEM_TYPE_COUNT
} elem_type_t;

typedef enum logical_op_t
{
    LOGICAL_OP_NOT = 0,
    LOGICAL_OP_OR  = 1,
    LOGICAL_OP_AND = 2,
    LOGICAL_OP_COUNT
} logical_op_t;

#endif /* end of include guard: ELEM_TYPES_H */
