#include <stdlib.h>
#include <stdio.h>

#include "input.h"

extern elems_gfx_callback_table_t elems_gfx_callback_table;
extern int ELEM_INPUT_TEXTURES[];

elem_input_t* elem_input_new()
{
    return malloc(sizeof(elem_input_t));
}

void elem_input_init_xy(elem_input_t* i, int x, int y)
{
    elem_base_init_xy((elem_base_t*)i, ELEM_TYPE_INPUT, x, y);

    i->value = 0;
    i->texture = ELEM_INPUT_TEXTURES[0];
    elems_update_gfx(input, i);
}

void elem_input_set_value(elem_input_t* i, bool value)
{
    i->value = value;
    i->texture = ELEM_INPUT_TEXTURES[0];
    elems_update_gfx(input, i);
}
