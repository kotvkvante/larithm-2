#include <stdlib.h>
#include <stdio.h>

#include "client/graphics/texture.h"
#include "base.h"

char* elements_labels[] = {
    [ELEM_TYPE_BASE]    = "base",
    [ELEM_TYPE_INPUT]   = "input",
    [ELEM_TYPE_OUTPUT]  = "output",
    [ELEM_TYPE_LOGICAL] = "logical",
};

int ELEM_INPUT_TEXTURES[] = {TEXTURE_ELEM_INPUT};
int ELEM_OUTPUT_TEXTURES[] = {TEXTURE_ELEM_OUTPUT};
int ELEM_LOGICAL_TEXTURES[] =
{
    TEXTURE_ELEM_LOGICAL_NOT,
    TEXTURE_ELEM_LOGICAL_OR,
    TEXTURE_ELEM_LOGICAL_AND
};

int* ELEM_TEXTURES[ELEM_TYPE_COUNT] = {
    [ELEM_TYPE_BASE   ] = 0,
    [ELEM_TYPE_INPUT  ] = ELEM_INPUT_TEXTURES,
    [ELEM_TYPE_OUTPUT ] = ELEM_OUTPUT_TEXTURES,
    [ELEM_TYPE_LOGICAL] = ELEM_LOGICAL_TEXTURES,
};

elems_gfx_callback_table_t elems_gfx_callback_table = {
    .data = NULL,
    .elem_input_update_gfx = NULL,
    .elem_output_update_gfx = NULL,
    .elem_logical_update_gfx = NULL
};

elem_base_t* elem_base_new()
{
    return malloc(sizeof(elem_base_t));
}

void elem_base_init(elem_base_t* b, elem_type_t type)
{
    b->type = type;
    b->label = elements_labels[type];
    b->texture = ELEM_TEXTURES[type][ELEM_DEFAULT_TEXTURE];
}

void elem_base_init_xy(elem_base_t* b, elem_type_t type, int x, int y)
{
    elem_base_init(b, type);
    b->x = x;
    b->y = y;
}

void elem_base_init_xyt(elem_base_t* b, elem_type_t type, int x, int y, int texture)
{
    elem_base_init_xy(b, type, x, y);
    b->texture = texture;
}

char* elem_get_name(elem_base_t* b)
{
    return b->label;
}
