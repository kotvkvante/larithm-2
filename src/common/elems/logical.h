#ifndef ELEM_LOGICAL_H
#define ELEM_LOGICAL_H

#include "base.h"

typedef struct elem_logical_t {
    elem_base_t;
    logical_op_t op;
} elem_logical_t;

elem_logical_t* elem_logical_new();
void elem_logical_init_xy(elem_logical_t* l, logical_op_t op, int x, int y);

#endif /* end of include guard: ELEM_LOGICAL_H */
