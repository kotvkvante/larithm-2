#ifndef ELEM_BASE_H
#define ELEM_BASE_H

#include "common/elems/types.h"

typedef struct elem_input_t elem_input_t;
typedef struct elem_output_t elem_output_t;
typedef struct elem_logical_t elem_logical_t;

typedef void (*elem_input_update_gfx_f)(elem_input_t* input, void* data);
typedef void (*elem_output_update_gfx_f)(elem_output_t* output, void* data);
typedef void (*elem_logical_update_gfx_f)(elem_logical_t* logical, void* data);

typedef struct elems_gfx_callback_table_t {
    void* data;
    elem_input_update_gfx_f elem_input_update_gfx;
    elem_output_update_gfx_f elem_output_update_gfx;
    elem_logical_update_gfx_f elem_logical_update_gfx;
} elems_gfx_callback_table_t;

#define elems_table_construct_name(type) elems_gfx_callback_table.elem_ ## type ## _update_gfx

#define elems_update_gfx(type, elem) \
    if(elems_table_construct_name(type) != NULL) \
        elems_table_construct_name(type)(elem, elems_gfx_callback_table.data)

#define elems_assign_callback(type, f) \
    elems_table_construct_name(type) = (f)

#define elems_assign_data(pointer) \
    elems_gfx_callback_table.data = pointer;

typedef struct elem_base_t {
    elem_type_t type;
    char* label;
    int texture;
    size_t tind;
    int x, y;
} elem_base_t;

elem_base_t* elem_base_new();
void elem_base_init_xy(elem_base_t* b, elem_type_t type, int x, int y);
void elem_base_init_xyt(elem_base_t* b, elem_type_t type, int x, int y, int texture);

char* elem_get_name(elem_base_t* b);

#endif /* end of include guard: ELEM_BASE_H */
