#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "logical.h"

extern elems_gfx_callback_table_t elems_gfx_callback_table;
extern int ELEM_LOGICAL_TEXTURES[];

char* logical_labels[] = {
    [LOGICAL_OP_NOT] = "logical not",
    [LOGICAL_OP_OR] = "logical or",
    [LOGICAL_OP_AND] = "logical and",
};

elem_logical_t* elem_logical_new()
{
    return malloc(sizeof(elem_logical_t));
}

void elem_logical_init_xy(elem_logical_t* l, logical_op_t op, int x, int y)
{
    elem_base_init_xy((elem_base_t*)l, ELEM_TYPE_LOGICAL, x, y);

    l->op = op;
    l->label = logical_labels[op];
    l->texture = ELEM_LOGICAL_TEXTURES[l->op];
}
