#include <stdlib.h>
#include <stdio.h>

#include "output.h"

extern elems_gfx_callback_table_t elems_gfx_callback_table;
extern int ELEM_OUTPUT_TEXTURES[];

elem_output_t* elem_output_new()
{
    return malloc(sizeof(elem_output_t));
}

void elem_output_init_xy(elem_output_t* o, int x, int y)
{
    elem_base_init_xy((elem_base_t*)o, ELEM_TYPE_OUTPUT, x, y);

    o->value = 0;
    // o->texture = ELEM_OUTPUT_TEXTURES[0];
    elems_update_gfx(output, o);
}

void elem_output_set_value(elem_output_t* o, bool value)
{
    o->value = value;

    elems_update_gfx(output, o);
}
