#include <string.h>

#include "common/level/llff.h"
#include "utils/utils.h"

static char* LLFF_TO_STR[] = {
	[LLF_PLAIN] = "txt",
	[LLF_JSON]  = "la2lj",
};

llfformat_e llff_get_fileformat(char* ext)
{
	IF_FF_RETURN_CODE(ext, LLF_PLAIN, LLFF_TO_STR);
	IF_FF_RETURN_CODE(ext, LLF_JSON, LLFF_TO_STR);

	return LLF_UNDEFINED;
}
