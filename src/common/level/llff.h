#ifndef LLFF_H
#define LLFF_H

typedef enum {
    /// basic plain text format
	LLF_PLAIN,
    /// format based on JSON
	LLF_JSON,

    LLF_UNDEFINED,
} llfformat_e;

llfformat_e llff_get_fileformat(char* ext);


#endif /* end of include guard: LLFF_H */
