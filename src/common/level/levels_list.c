#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

#include <unistd.h>

#include "utils/utils.h"
#include "frozen.h"

#include "common/level/levels_list.h"
#include "common/level/llff.h"
#include "common/level/llff_json.h"
#include "common/level/llff_plain.h"

/// LVLIST_DEFAULT_MAIN_FILE
#define LVLIST_DEFMAIN "lvls.txt"

lvlist_t* lvlist_new()
{
    lvlist_t* lvlist = malloc(sizeof(lvlist_t));
    lvlist->lvlems_count = 0;
    lvlist->lvlems_list = NULL;
    lvlist->data = NULL;

    lvlist->lvlem_hovered  = false;
    lvlist->lvlem_selected = false;
    lvlist->hi = 0;
    lvlist->si = 0;

    return lvlist;
}

void lvlist_init_from_file(lvlist_t* lvlist, char* filepath)
{
    const char* ext = get_filename_ext(filepath);

    llfformat_e ff = llff_get_fileformat((char*)ext);
    if (ff == LLF_UNDEFINED) {
        printf("[w] Undefined file format: `%s` [%s]\n", filepath, ext);
    }

    char* str = file_read(filepath);
    if(str == NULL)
    {
        printf("[w] Unable to read file: '%s'.\n", filepath);
        return;
    }

    lvlist_init_from_str(lvlist, ff, str);

    free(str);
}

void lvlist_init_from_str(lvlist_t* lvlist, int code, char* str)
{
    switch (code) {
        case LLF_PLAIN:
            lvlist_init_from_str_PLAIN(lvlist, str);
            break;
        case LLF_JSON:
            lvlist_init_from_str_JSON(lvlist, str);
            break;
    }
}

lvlem_t* lvlist_get_selected(lvlist_t* lvlist)
{
    if (!lvlist->lvlem_selected) return NULL;
    return &lvlist->lvlems_list[lvlist->si];
}

bool lvlist_is_selected(lvlist_t* lvlist)
{
    return lvlist->lvlem_selected;
}

void lvlist_set_hovered(lvlist_t* lvlist, int i)
{
    lvlist->lvlem_hovered = true;
    lvlist->hi = i;
}

void lvlist_unset_hovered(lvlist_t* lvlist)
{
    lvlist->lvlem_hovered = false;
}

void lvlist_set_selected(lvlist_t* lvlist, int i)
{
    lvlist->lvlem_selected = true;
    lvlist->si = i;
}

void lvlist_unset_selected(lvlist_t* lvlist)
{
    lvlist->lvlem_selected = false;
}


void lvlist_print(lvlist_t* lvlist)
{
    printf("[i] Level list: %p\n", (void*)lvlist);
    for (size_t i = 0; i < lvlist->lvlems_count; i++) {
        lvlem_t* l = &lvlist->lvlems_list[i];
        printf("\t[%d] %s[%s] %s [author: %s]\n",
            l->id,
            l->fname,
            l->format,
            l->desc,
            l->author);
    }
}

void lvlist_free(lvlist_t* lvlist)
{
    if (lvlist->data != NULL)
    {
        free(lvlist->data);
    }
    else
    {
        for (size_t i = 0; i < lvlist->lvlems_count; i++) {
            lvlem_t* l = &lvlist->lvlems_list[i];
            free(l->author);
            free(l->fname);
            free(l->format);
            free(l->desc);
        }
    }
    free(lvlist->lvlems_list);
	free(lvlist);
}

void lvlem_append_to_file(lvlem_t* lvlem, char* filename)
{
    FILE* src = fopen(filename, "r");

    char buf[256];
    snprintf(buf, 256, "%s.temp", filename);
    char* temp_filename = strdup(buf);

    printf(">>> `%s`\n", temp_filename);
    if (access(temp_filename, F_OK) == 0) {
        printf("[E] `%s`: file exits. \n", __func__);
        exit(-1);
    }
    FILE* dst = fopen(temp_filename, "w");

    char s[256];
    fgets(s, 256, src);

    int d = 0;
    sscanf(s, "%d", &d);
    d++;

    char c;
    while ((c = fgetc(src)) != EOF)
    {
        fputc(c, dst);
    }

    char a[256];
    snprintf(a, 256, "assets/lvls/community/%d.%s:%s", lvlem->id, lvlem->format, lvlem->desc);
    fputs(a, dst);

    rename(temp_filename, filename);

    free(temp_filename);
}

void lvlem_append_to_file_JSON(lvlem_t* lvlem, char* filename)
{
    char* str = file_read(filename);
    if(str == NULL)
    {
        printf("[w] `%s`: Unable to read file: '%s'.\n", __func__, filename);
        return;

    }

    int count;

    json_scanf(str, strlen(str), "{count: %d}", &count);

    char* a;
    char* b;
    if (count == 0) {
        a = ".levels";
        b = "[{id: %d, author: %Q, format: %Q, fname: %Q}]";
    } else {
        a = ".levels[]";
        b = "{id: %d, author: %Q, format: %Q, fname: %Q}";
    }
    count++;


    char buf[1024];
    struct json_out out1 = JSON_OUT_BUF(buf, 1024);
    json_setf(str, strlen(str), &out1, a, b,
        lvlem->id, lvlem->author, lvlem->format, lvlem->fname);

    FILE* fp = fopen("assets/lvls/list_of_levels.la2lj.tmp", "wb");
    struct json_out out2 = JSON_OUT_FILE(fp);
    json_setf(buf, strlen(buf), &out2, ".count", "%d", count);

    free(str);
    fclose(fp);

    rename("assets/lvls/list_of_levels.la2lj.tmp", filename);
}
