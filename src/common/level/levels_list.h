#ifndef LEVELS_LIST_H
#define LEVELS_LIST_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct lvlem_t {
    int id;
    char* author;
    char* fname;
    char* format;
    char* desc;
} lvlem_t; // lvl list element

typedef struct lvlist_t
{
    size_t lvlems_count;
    lvlem_t* lvlems_list;

    bool lvlem_hovered;
    bool lvlem_selected;
    int hi; // hovered  index
    int si; // selected index

    char* data;
} lvlist_t; // level list

lvlist_t* lvlist_new();

void lvlist_init_from_file(lvlist_t* lvlist, char* filepath);
void lvlist_init_from_str(lvlist_t* lvlist, int code, char* str);

lvlem_t* lvlist_get_selected(lvlist_t* lvlist);

bool lvlist_is_selected(lvlist_t* lvlist);
void lvlist_set_hovered(lvlist_t* lvlist, int i);
void lvlist_unset_hovered(lvlist_t* lvlist);
void lvlist_set_selected(lvlist_t* lvlist, int i);
void lvlist_unset_selected(lvlist_t* lvlist);

void lvlist_print(lvlist_t* lvlist);
void lvlist_free(lvlist_t* lvlist);

#endif /* end of include guard: LEVELS_LIST_H */
