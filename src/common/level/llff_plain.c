#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils/utils.h"

#include "common/level/levels_list.h"
#include "common/level/llff_plain.h"


void lvlist_init_from_str_PLAIN(lvlist_t* lvlist, char* str)
{
    /// Нужно: рефакторинг
    size_t count; int n;

	if (sscanf(str, "%lu%n", &count, &n) != 1) { printf("Couldn't scan count.\n"); return; }
	str += (n + 1); /// Пропустить просканированное число и символ переноса строки.

    lvlist->lvlems_count = count;
    lvlist->lvlems_list = malloc(sizeof(lvlem_t) * count);

    int len = strlen(str);
	// lvlist->data = malloc(sizeof(char) * (len + 1)); /// for '\0'
	lvlist->data = malloc(sizeof(char) * (len + 1));
	// printf("1) i = %d\n", len + 1);

	char* p = strtok(str, "\n");
	size_t k = 0;
	size_t m = 0;
	while (p != NULL)
	{
        // printf(">>> `%s`\n", p);

		lvlem_t* l = &lvlist->lvlems_list[m];
		l->fname = lvlist->data + k;
		char* cp = p;

		size_t i = 0;
		while(*cp != ':') {
			l->fname[i] = *cp;
			cp++;
			i++;
		} l->fname[i] = '\0'; i++; cp++;
		// printf("> `%s`\n", l->fname);

		l->desc = &l->fname[i];
		size_t j = 0;
		// printf(">[c] %c\n", *cp);
		while (*cp != '\n' && *cp != '\0') {
			// printf(">[c] %c\n", *cp);
			l->desc[j] = *cp;
			cp++;
			j++;
		} l->desc[j] = '\0'; j++;
		// printf("> `%s`\n", l->desc);

        l->id = m;
        l->author = NULL;
        l->format = (char*)get_filename_ext(l->fname);

		k += (j + i);
		p = strtok(NULL, "\n");
		m++;
	}
	// fwrite(lvlist->data, 1, len, stdout);
	// printf("\n");

	/// Старая версия
	// char buf[256];
    // fgetc(fp); // read newline symbol
    // size_t i = 0;
    // while (fgets(buf, 256, fp))
    // {
    //     size_t buflen = strlen(buf);
    //     buf[buflen-1] = '\0'; buflen--;
    //     char* const sep_at = strchr(buf, ':');
    //     if(sep_at != NULL)
    //     {
    //         *sep_at = '\0'; /* overwrite first separator, creating two strings. */
	//
    //         lvlem_t* l = &lvlist->lvlems_list[i];
    //         l->fname = malloc(sizeof(char) * (buflen + 1));
    //         l->desc = stpcpy(l->fname, buf) + 1;
    //         strcpy(l->desc, sep_at + 1);
	//
    //         i++;
    //     }
    // }
    // fclose(fp);
}
