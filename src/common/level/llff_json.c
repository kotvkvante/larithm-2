#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "frozen.h"
#include "common/level/levels_list.h"
#include "common/level/llff_json.h"

static void scan_array(const char *str, int len, void *user_data) {
	struct json_token t;
	int i;

	lvlist_t* lvlist = (lvlist_t*)user_data;
	int id;
	char* author;
	char* format;
	char buf[64];

	// printf("Parsing array: %.*s\n", len, str);
	for (i = 0; json_scanf_array_elem(str, len, "", i, &t) > 0; i++) {
		lvlem_t* lvlem = &lvlist->lvlems_list[i];

		lvlem->fname = NULL; /// ??
		lvlem->desc = NULL;

		json_scanf(t.ptr, t.len,
			"{id: %d, author: %Q, format: %Q, fname: %Q}",
			&lvlem->id,
			&lvlem->author,
			&lvlem->format,
			&lvlem->fname);
	}
}

void lvlist_init_from_str_JSON(lvlist_t* lvlist, char* str)
{
    struct json_token jt_levels;
	int count;
	json_scanf(str, strlen(str),
		"{ count: %d, levels: %T}",
		&count,
		&jt_levels);

	lvlist->lvlems_count = count;
	lvlist->lvlems_list  = malloc(sizeof(lvlem_t) * count);

	json_scanf(jt_levels.ptr, jt_levels.len,
		"[%M]",
		scan_array, (void*)lvlist);

}
