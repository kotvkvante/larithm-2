#ifndef HTILEX_H
#define HTILEX_H

#include <stdlib.h>

#include "common/tile/tilex.h"

/// htilex := `tile extended handler`
typedef struct htilex_t {
	tilex_t* tilexs_list;
	size_t tilexs_cnt;
	size_t tilexs_max;
} htilex_t;

void ht_init(htilex_t* ht, size_t n);
void ht_tilex_append(htilex_t* ht, float x, float y, int tt, tilexf_t tf);
void ht_tilex_append_ex(htilex_t* ht, size_t* tind, float x, float y, int tt, tilexf_t tf);

void ht_tilex_insert(htilex_t* ht, size_t n, float x, float y, int tt, tilexf_t tf);
void ht_tilex_insert_ex(htilex_t* ht, size_t* tind, size_t n, float x, float y, int tt, tilexf_t tf);

void ht_tilex_update(htilex_t* ht, size_t tind, float x, float y, int tt, tilexf_t tf);
void ht_tilex_set_tt_tf(htilex_t* ht, size_t tind, int tt, tilexf_t tf);
void ht_gen_tilex_index();

#endif /* end of include guard: HTILEX_H */
