#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "common/tile/tilex.h"

void tilex_init(tilex_t* tx, float x, float y, int tt, tilexf_t tf)
{
	assert(tx != NULL);
	tx->x = x;
	tx->y = y;
	tx->texture = tt;
	tx->transform = tf;
}

void tilex_set_tt(tilex_t* tx, size_t tt)
{
	tx->texture = tt;
}

void tilex_set_x(tilex_t* tx, float x)
{
	tx->x = x;
}

void tilex_set_y(tilex_t* tx, float y)
{
	tx->y = y;
}

void tilex_set_xy(tilex_t* tx, float x, float y)
{
	tx->x = x;
	tx->y = y;
}

void tilex_mirrorx(tilex_t* tx)
{
	tx->transform ^= MIRRORX;
}

void tilex_mirrory(tilex_t* tx)
{
	tx->transform ^= MIRRORY;
}

void tilex_print(tilex_t* tx)
{
	printf("Tilex: {%f, %f} texture: %d transform: [%d]\n", tx->x, tx->y, tx->texture, tx->transform);
}
