#include <stdint.h>
#include "common/tile/htilex.h"

void ht_init(htilex_t* ht, size_t n)
{
	ht->tilexs_cnt = 0;
	ht->tilexs_max = n;
	ht->tilexs_list = malloc(sizeof(tilex_t) * ht->tilexs_max);
}

void ht_tilex_append(htilex_t* ht, float x, float y, int tt, tilexf_t tf)
{
	ht_tilex_append_ex(ht, NULL, x, y, tt, tf);
}

void ht_tilex_append_ex(htilex_t* ht, size_t* tind, float x, float y, int tt, tilexf_t tf)
{
	tilex_init(&ht->tilexs_list[ht->tilexs_cnt], x, y, tt, tf);
	if (tind != NULL) (*tind) = ht->tilexs_cnt;
	ht->tilexs_cnt++;
}

void ht_tilex_insert(htilex_t* ht, size_t n, float x, float y, int tt, tilexf_t tf)
{
	ht_tilex_insert_ex(ht, NULL, n, x, y, tt, tf);
}

void ht_tilex_insert_ex(htilex_t* ht, size_t* tind, size_t n, float x, float y, int tt, tilexf_t tf)
{
    for (size_t j = ht->tilexs_cnt; j > n; j--) {
        ht->tilexs_list[j] = ht->tilexs_list[j-1];
	}
	tilex_init(&ht->tilexs_list[n], x, y, tt, tf);
	if (tind != NULL) (*tind) = n;
}

void ht_tilex_update(htilex_t* ht, size_t tind, float x, float y, int tt, tilexf_t tf)
{
	tilex_init(&ht->tilexs_list[tind], x, y, tt, tf);
}

void ht_tilex_set_tt_tf(htilex_t* ht, size_t tind, int tt, tilexf_t tf)
{
	ht->tilexs_list[tind].texture = tt;
	ht->tilexs_list[tind].transform = tt;
}
