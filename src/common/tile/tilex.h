#ifndef TILEX_H
#define TILEX_H

#include <stddef.h>
#include <stdint.h>

#define NOMIRROR 0

// rotation    mirror
// 00	   	   00
enum Options {
	ROTATE0   = 0,
	ROTATE90  = 1,
	ROTATE180 = 2,
	ROTATE270 = 3,
	MIRRORX = 1 << 2,
	MIRRORY = 1 << 3,
	MIRRORXY = MIRRORX | MIRRORY,
};

typedef uint32_t tilexf_t;
typedef struct tilex_t {
	float x, y;
	int texture;
	tilexf_t transform;
} tilex_t;

void tilex_init(tilex_t* tx, float x, float y, int tt, tilexf_t tf);
void tilex_set_tt(tilex_t* tx, size_t tt);
void tilex_set_tt_tf(tilex_t* tx, size_t tt, tilexf_t tf);

void tilex_set_x(tilex_t* tx, float x);
void tilex_set_y(tilex_t* tx, float y);
void tilex_set_xy(tilex_t* tx, float x, float y);

void tilex_mirrorx(tilex_t* tx);
void tilex_mirrory(tilex_t* tx);

void tilex_print(tilex_t* tx);

#endif /* end of include guard: TILEX_H */
