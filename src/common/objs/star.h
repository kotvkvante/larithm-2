#ifndef OBJ_STAR_H
#define OBJ_STAR_H

#include <stdbool.h>
#include "base.h"

typedef struct obj_star_t {
	obj_base_t;
	int is_collected;
} obj_star_t;

typedef void (*obj_star_update_gfx_f)(obj_star_t* star, void* data);

obj_star_t* obj_star_new();
void obj_star_init(obj_star_t* star, int x, int y);
bool obj_star_is_collected(obj_star_t* star);
void obj_star_collect(obj_star_t* star);
void obj_star_drop(obj_star_t* star);
void obj_star_update_texture(obj_star_t* star);

#endif /* end of include guard: OBJ_STAR_H */
