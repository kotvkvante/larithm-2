#include <stdlib.h>
#include <stdio.h>

#include "common/map/map.h"
#include "utils/utils.h"
#include "lever.h"

extern int OBJ_LEVER_TEXTURES[];
extern void* obj_gfx_data;

obj_lever_update_gfx_f obj_lever_update_gfx = NULL;

char* OBJ_LEVER_STATE_STR[] = {
    [LEVER_ON] = "On",
    [LEVER_OFF] = "Off",
};

obj_lever_t* obj_lever_new()
{
    obj_lever_t* lever = malloc(sizeof(obj_lever_t));
    return lever;
}

void obj_lever_init(obj_lever_t* lever, int x, int y, int state)
{
    obj_base_init((obj_base_t*)lever, OBJ_TYPE_LEVER, x, y);

    lever->state = state;
    obj_lever_update_texture(lever);
}

void obj_lever_turn(obj_lever_t* lever)
{
    lever->state = !lever->state;
    obj_lever_update_texture(lever);
}

void obj_lever_on(obj_lever_t* lever)
{
    lever->state = LEVER_ON;
    obj_lever_update_texture(lever);
}

void obj_lever_off(obj_lever_t* lever)
{
    lever->state = LEVER_OFF;
    obj_lever_update_texture(lever);
}

void obj_lever_update_texture(obj_lever_t* lever)
{
    ((obj_base_t*)lever)->texture = OBJ_LEVER_TEXTURES[lever->state];
    if (obj_lever_update_gfx != NULL)
        obj_lever_update_gfx(lever, obj_gfx_data);
}

void obj_lever_print_info(obj_lever_t* lever)
{
    printf("> Obj: %c {%d, %d} : %s\n",
        ((obj_base_t*)lever)->symbol,
        ((obj_base_t*)lever)->x,
        ((obj_base_t*)lever)->y,
        OBJ_LEVER_STATE_STR[lever->state]);
}
