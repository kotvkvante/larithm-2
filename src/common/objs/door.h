#ifndef OBJ_DOOR_H
#define OBJ_DOOR_H

#include "base.h"

typedef enum obj_door_state_t {
    DOOR_CLOSED = 0,
    DOOR_OPENED = 1,
} obj_door_state_t;

typedef struct obj_door_t
{
    obj_base_t;
    int is_open;
} obj_door_t;

typedef void (*obj_door_update_gfx_f)(obj_door_t* door, void* data);


obj_door_t* obj_door_new();
void obj_door_init(obj_door_t* door, int x, int y, int is_open);
void obj_door_set_state(obj_door_t* door, int is_open);
void obj_door_update_texture(obj_door_t* door);

#endif /* end of include guard: OBJ_DOOR_H */
