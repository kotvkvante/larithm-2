#ifndef OBJ_TYPES_H
#define OBJ_TYPES_H

#include <stdlib.h>
#include <stdint.h>

#define MAP_NULL_INDEX 0
#define MAP_NULL_OBJ NULL

#define OBJ_DEFAULT_SYMBOL 0
#define OBJ_DEFAULT_TEXTURE 0

typedef enum obj_type_e
{
    OBJ_TYPE_BASE = 0,
    OBJ_TYPE_WALL,
    OBJ_TYPE_DOOR,
    OBJ_TYPE_LEVER,
    OBJ_TYPE_STAR,
    OBJ_TYPE_COUNT
} obj_type_e;


#endif /* end of include guard: OBJ_TYPES_H */
