#include <stdlib.h>
#include <stdio.h>

#include "wall.h"

obj_wall_t* obj_wall_new()
{
    obj_wall_t* wall = malloc(sizeof(obj_wall_t));
    return wall;
}

void obj_wall_init(obj_wall_t* wall, int x, int y)
{
    obj_base_init((obj_base_t*)wall, OBJ_TYPE_WALL, x, y);

    wall->color = 1;
}
