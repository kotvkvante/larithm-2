#include <stdlib.h>
#include <stdio.h>

#include "common/objs/star.h"

extern int OBJ_STAR_TEXTURES[];
extern void* obj_gfx_data;
obj_star_update_gfx_f obj_star_update_gfx = NULL;

obj_star_t* obj_star_new()
{
    obj_star_t* star = malloc(sizeof(obj_star_t));
    return star;
}

void obj_star_init(obj_star_t* star, int x, int y)
{
    obj_base_init((obj_base_t*)star, OBJ_TYPE_STAR, x, y);
	star->is_collected = false;
    
    obj_star_update_texture(star);
}

bool obj_star_is_collected(obj_star_t* star)
{
	return !star->is_collected;
}

void obj_star_collect(obj_star_t* star)
{
	star->is_collected = true;
    obj_star_update_texture(star);
}

void obj_star_drop(obj_star_t* star)
{
	star->is_collected = false;
    obj_star_update_texture(star);
}

void obj_star_update_texture(obj_star_t* star)
{
    ((obj_base_t*)star)->texture = OBJ_STAR_TEXTURES[star->is_collected];

    if (obj_star_update_gfx != NULL)
        obj_star_update_gfx(star, obj_gfx_data);
}
