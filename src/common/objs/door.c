#include <stdlib.h>
#include <stdio.h>

#include "door.h"

extern int OBJ_DOOR_TEXTURES[];
extern void* obj_gfx_data;

obj_door_update_gfx_f obj_door_update_gfx = NULL;

void obj_door_switch_real (obj_door_t* door);
void obj_door_open_real   (obj_door_t* door);
void obj_door_close_real  (obj_door_t* door);

obj_door_t* obj_door_new()
{
    obj_door_t* door = malloc(sizeof(obj_door_t));
    return door;
}

void obj_door_init(obj_door_t* door, int x, int y, int is_open)
{
    obj_base_init((obj_base_t*)door, OBJ_TYPE_DOOR, x, y);

    door->is_open = is_open;
    obj_door_update_texture(door);
}

void obj_door_set_state(obj_door_t* door, int is_open)
{
    door->is_open = is_open;
    door->texture = OBJ_DOOR_TEXTURES[door->is_open];
    obj_door_update_texture(door);
}

void obj_door_switch(obj_door_t* door)
{
    obj_door_switch_real(door);
    door->texture = OBJ_DOOR_TEXTURES[door->is_open];
    obj_door_update_texture(door);
}

void obj_door_open(obj_door_t* door)
{
    obj_door_open_real(door);
    door->texture = OBJ_DOOR_TEXTURES[door->is_open];
    obj_door_update_texture(door);
}

void obj_door_close(obj_door_t* door)
{
    obj_door_close_real(door);
    door->texture = OBJ_DOOR_TEXTURES[door->is_open];
    obj_door_update_texture(door);
}

void obj_door_update_texture(obj_door_t* door)
{
    ((obj_base_t*)door)->texture = OBJ_DOOR_TEXTURES[door->is_open];
    if (obj_door_update_gfx != NULL)
        obj_door_update_gfx(door, obj_gfx_data);
}

void obj_door_switch_real(obj_door_t* door)
{ door->is_open = !door->is_open; }

void obj_door_open_real(obj_door_t* door)
{ door->is_open = DOOR_OPENED; }

void obj_door_close_real(obj_door_t* door)
{ door->is_open = DOOR_CLOSED; }
