#ifndef OBJ_BASE_H
#define OBJ_BASE_H

#include "common/objs/types.h"

// typedef void (*obj_update_f)(void* ctx, size_t* tind);

typedef struct obj_base_t {
    int type;

    char symbol;
    int  texture;
    size_t tind; /// tile index
    // tilex_t tx;

    int x, y;

    int next;
} obj_base_t;

obj_base_t* obj_base_new();
void obj_base_init(obj_base_t* base, obj_type_e type, int x, int y);
char* obj_get_name(obj_base_t* base);

obj_base_t* obj_create_random_type(int x, int y);

#endif /* end of include guard: OBJ_BASE_H */
