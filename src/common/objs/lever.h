#ifndef OBJ_LEVER_H
#define OBJ_LEVER_H

#include "base.h"

typedef enum obj_lever_state_t {
    LEVER_OFF = 0,
    LEVER_ON  = 1,
} obj_lever_state_t;

typedef struct obj_lever_t
{
    obj_base_t;

    int state;
} obj_lever_t;

typedef void (*obj_lever_update_gfx_f)(obj_lever_t* lever, void* data);

obj_lever_t* obj_lever_new();
void obj_lever_init(obj_lever_t* door, int x, int y, int state);

void obj_lever_turn(obj_lever_t* lever);
void obj_lever_on(obj_lever_t* lever);
void obj_lever_off(obj_lever_t* lever);
void obj_lever_update_texture(obj_lever_t* lever);

void obj_lever_print_info(obj_lever_t* lever);


#endif /* end of include guard: OBJ_LEVER_H */
