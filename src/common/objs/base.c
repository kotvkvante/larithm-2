#include <stdlib.h>
#include <stdio.h>

/// Нужно: common не должен зависить от заголовков
/// из директории client.
/// Также смотри src/common/elems/base.c,
/// src/common/player/player.c
#include "client/graphics/texture.h"
#include "math/random.h"

#include "base.h"
#include "door.h"
#include "wall.h"
#include "lever.h"

char* NULL_OBJECT_STR = "Null object";

char* OBJ_NAMES[] = {
    [OBJ_TYPE_BASE ] = "Base object",
    [OBJ_TYPE_WALL ] = "Wall object",
    [OBJ_TYPE_DOOR ] = "Door object",
    [OBJ_TYPE_LEVER] = "Lever object",
    [OBJ_TYPE_STAR ] = "Star object",
};

char* OBJ_SYMBOLS[] = {
    [OBJ_TYPE_BASE ] = "x",
    [OBJ_TYPE_WALL ] = "#",
    [OBJ_TYPE_DOOR ] = "Dd",
    [OBJ_TYPE_LEVER] = "Ll",
    [OBJ_TYPE_STAR ] = "S*",
};

int OBJ_BASE_TEXTURES[] = {TEXTURE_0};
int OBJ_WALL_TEXTURES[] = {TEXTURE_OBJ_WALL};
int OBJ_DOOR_TEXTURES[] = {TEXTURE_OBJ_DOOR_CLOSED, TEXTURE_OBJ_DOOR_OPENED};
int OBJ_LEVER_TEXTURES[] = {TEXTURE_OBJ_LEVER_OFF, TEXTURE_OBJ_LEVER_ON};
int OBJ_STAR_TEXTURES[] = {TEXTURE_OBJ_STAR, TEXTURE_OBJ_STAR_PICKED};

int* OBJ_TEXTURES[OBJ_TYPE_COUNT] = {
    [OBJ_TYPE_BASE ] = OBJ_BASE_TEXTURES,
    [OBJ_TYPE_WALL ] = OBJ_WALL_TEXTURES,
    [OBJ_TYPE_DOOR ] = OBJ_DOOR_TEXTURES,
    [OBJ_TYPE_LEVER] = OBJ_LEVER_TEXTURES,
    [OBJ_TYPE_STAR ] = OBJ_STAR_TEXTURES,
};

void* obj_gfx_data = NULL;

// typedef void (*obj_update_f)(void* ctx, size_t* tind);
// obj_update_f update_arr[] = {
//     [OBJ_TYPE_BASE  ] = NULL,
//     [OBJ_TYPE_WALL  ] = NULL,
//     [OBJ_TYPE_DOOR  ] = NULL,
//     [OBJ_TYPE_LEVER ] = NULL,
// };

obj_base_t* obj_base_new()
{
    return malloc(sizeof(obj_base_t));
}

void obj_base_init(obj_base_t* base, obj_type_e type, int x, int y)
{
    base->type = type;

    base->x = x;
    base->y = y;

    base->symbol = OBJ_SYMBOLS[type][OBJ_DEFAULT_SYMBOL];
    base->texture = OBJ_TEXTURES[type][OBJ_DEFAULT_TEXTURE];

    base->next = MAP_NULL_INDEX;
}

obj_base_t* obj_create_random_type(int x, int y)
{
    int obj_type = rand_int(OBJ_TYPE_WALL, OBJ_TYPE_COUNT);

    switch (obj_type) {
        /// Нужно: этот случай не достижим ? Интервалы rand_int
        case OBJ_TYPE_BASE:
            return NULL;

        case OBJ_TYPE_WALL: {
            obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, x, y);
            return (obj_base_t*)wall;
        }

        case OBJ_TYPE_DOOR: {
            obj_door_t* door = obj_door_new();
            int is_open = rand_tf();
            obj_door_init(door, x, y, is_open);
            return (obj_base_t*)door;
        }

        case OBJ_TYPE_LEVER: {
            obj_lever_t* lever = obj_lever_new();
            int is_on = rand_tf();
            obj_lever_init(lever, x, y, is_on);
            return (obj_base_t*)lever;
        }

        default:
            return NULL;
    }
}

char* obj_get_name(obj_base_t* base)
{
    if(base == NULL) return NULL_OBJECT_STR;

    return OBJ_NAMES[base->type];
}
