#ifndef OBJ_WALL_H
#define OBJ_WALL_H

#include "base.h"

typedef struct obj_wall_t {
    obj_base_t;

    int color;
} obj_wall_t;

obj_wall_t* obj_wall_new();
void obj_wall_init(obj_wall_t* wall, int x, int y);

#endif /* end of include guard: OBJ_WALL_H */
