#ifndef TREE_H
#define TREE_H

typedef struct tree_t {
	int elem_index;

	struct tree_t* parent;
	struct tree_t* left;
	struct tree_t* right;
} tree_t;

#endif /* end of include guard: TREE_H */
