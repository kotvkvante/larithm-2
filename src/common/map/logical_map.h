#ifndef LOGICAL_MAP_H
#define LOGICAL_MAP_H

#include <stdlib.h>
#include <stdbool.h>

#include "common/map/graph.h"
#include "common/tile/htilex.h"

typedef struct elem_base_t elem_base_t;
typedef struct elem_output_t elem_output_t;
typedef struct graph_t graph_t;

typedef struct logical_map_t
{
    size_t size;

    size_t elements_max;
    size_t elements_count;

    elem_base_t** elements_list;
    int* elements_indices;

    // int* input_indices;
    // int* output_indices;

    int tile_selected;
    int ts_x, ts_y;
    tilex_t ts;

    int tile_hovered;
    int th_x, th_y;
    tilex_t th;

    graph_t g;

    void* ht;
} logical_map_t;

logical_map_t* logical_map_new();

void lmap_init_default(logical_map_t* lmap);
void lmap_init(logical_map_t* lm, size_t size, int elements_max);

size_t lmap_linear_coord(logical_map_t* lm, int x, int y);
elem_base_t* lmap_get_elem_by_index(logical_map_t* lm, size_t elem_index);
elem_base_t* lmap_get_elem_by_coord(logical_map_t* lm, size_t x, size_t y);
size_t lmap_get_elem_index(logical_map_t* lm, elem_base_t* elem);



void logical_map_insert_elem(logical_map_t* lm, elem_base_t* elem);
bool logical_map_compute_node(logical_map_t* lm, int node);

// bool compute_node(logical_map_t* lm, int node);
void lmap_print_map(logical_map_t* lm);
void lmap_print_graph(logical_map_t* lm);
void lmap_info(logical_map_t* lmap);

void lmap_set_stile(logical_map_t* lmap, int i, int j);
elem_base_t* lmap_get_selected_elem(logical_map_t* lmap);

void lmap_unset_stile(logical_map_t* lmap);
void lmap_set_htile(logical_map_t* lmap, int i, int j);
void lmap_unset_htile(logical_map_t* lmap);

void lmap_get_tile_info(logical_map_t* lmap, char* buf, int x, int y);
void lmap_get_stile_info(logical_map_t* lmap, char* buf);
void lmap_get_htile_info(logical_map_t* lmap, char* buf);

void logical_map_free(logical_map_t* lmap);
#endif /* end of include guard: LOGICAL_MAP_H */
