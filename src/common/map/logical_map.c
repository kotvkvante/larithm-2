#include <stdlib.h>
#include <stdio.h>

#include "logical_map.h"
#include "graph.h"

#include "common/elems/input.h"
#include "common/elems/output.h"
#include "common/elems/logical.h"

logical_map_t* logical_map_new()
{
    logical_map_t* lmap = malloc(sizeof(logical_map_t));
    lmap->size = 0;

    lmap->elements_list   = NULL;
    lmap->elements_indices      = NULL;
    lmap->elements_max       = 0;
    lmap->elements_count     = 0;

    lmap->tile_selected = false;
    lmap->ts_x = 0; lmap->ts_y = 0;
    lmap->tile_hovered = false;
    lmap->th_x = 0; lmap->th_y = 0;

    lmap->g = (graph_t){ 0 };

    return lmap;
}

void lmap_init_default(logical_map_t* lmap)
{
    // lmap->tile_selected = false;
    // lmap->ts_x = 0;
    // lmap->ts_y = 0;
    //
    // lmap->tile_hovered = false;
    // lmap->th_x = 0;
    // lmap->th_y = 0;
}

void lmap_init(logical_map_t* lm, size_t size, int elements_max)
{
    // lmap_init_default(lm);

    lm->size = size;
    lm->elements_indices = malloc(sizeof(int) * size * size);
    for (size_t i = 0; i < size * size; i++)
    {
        lm->elements_indices[i] = LOGICAL_MAP_NULL_INDEX;
    }

    lm->elements_max = elements_max + 1;
    lm->elements_count = 1;
    lm->elements_list = malloc(sizeof(elem_base_t*) * lm->elements_max);
    for (size_t i = 0; i < lm->elements_max; i++)
    {
        lm->elements_list[i] = LOGICAL_MAP_NULL_ELEM;
    }

    graph_init(&(lm->g), True);
}

bool logical_map_tile_exists(logical_map_t* lm, int x, int y)
{
    return (x < (int)lm->size) && (x >= 0) && (y < (int)lm->size) && (y >= 0);
}

size_t lmap_linear_coord(logical_map_t* lm, int x, int y)
{
    return x + y * lm->size;
}

elem_base_t* lmap_get_elem_by_index(logical_map_t* lm, size_t elem_index)
{
    /// Важно: Небезопасно
    return lm->elements_list[elem_index];
}

elem_base_t* lmap_get_elem_by_coord(logical_map_t* lm, size_t x, size_t y)
{
    size_t index = lm->elements_indices[lmap_linear_coord(lm, x, y)];
    return lmap_get_elem_by_index(lm, index);
}

size_t lmap_get_elem_index(logical_map_t* lm, elem_base_t* elem)
{
    size_t index = lm->elements_indices[lmap_linear_coord(lm, elem->x, elem->y)];
    return index;
}

// size_t* lmap_get_index_by_coord(logical_map_t* lm, size_t x, size_t y)
// {
//
// }

void logical_map_insert_elem(logical_map_t* lm, elem_base_t* elem)
{
    lm->elements_list[lm->elements_count] = elem;
    lm->elements_indices[lmap_linear_coord(lm, elem->x, elem->y)] = lm->elements_count;
    lm->elements_count++;
}

bool logical_map_compute_node(logical_map_t* lm, int node)
{
    graph_t* g = &(lm->g);
    edgenode_t* p = g->edges[node];
    bool result;

    if(lm->elements_list[node]->type == ELEM_TYPE_INPUT)
    {
        return ((elem_input_t*)lm->elements_list[node])->value;
    }

    // if(lm->elements_list[node]->type == ELEM_TYPE_OUTPUT)
    // {
    //     return logical_map_compute_node(lm, p->y);
    // }

    if(lm->elements_list[node]->type == ELEM_TYPE_LOGICAL)
    {
        switch (((elem_logical_t*)lm->elements_list[node])->op)
        {
            case LOGICAL_OP_NOT:
                return !logical_map_compute_node(lm, p->y);

            case LOGICAL_OP_OR:
                result = False;
                while(p != NULL)
                {
                    result = result || logical_map_compute_node(lm, p->y);
                    p = p->next;
                }
                break;
            case LOGICAL_OP_AND:
                result = True;
                while(p != NULL)
                {
                    result = result && logical_map_compute_node(lm, p->y);
                    p = p->next;
                }
                break;
            default:
                printf("> logical_map_compute_node: No such logical operator.\n");
                break;
        }
    }

    // printf("> logical_map_compute_node: Never reaches.");
    return result;
}

void lmap_print_graph(logical_map_t* lm)
{
    graph_t* g = &(lm->g);

    edgenode_t* p;
    for (int i = 1; i <= g->nvertices; i++) {
        int value = 0;
        switch (lm->elements_list[i]->type) {
            case ELEM_TYPE_INPUT:
                value = ((elem_input_t*)lm->elements_list[i])->value;
                break;
            case ELEM_TYPE_OUTPUT:
                value = ((elem_output_t*)lm->elements_list[i])->value;
                break;
            default: break;
        }
        printf("%d [%s %d]: \n", i, lm->elements_list[i]->label, value);
        p = g->edges[i];
        while(p != NULL) {
            printf("\t%d [%s]\n", p->y, lm->elements_list[p->y]->label);
            p = p->next;
        }
        printf("\n");
    }
}

void lmap_print_map(logical_map_t* lm)
{
    printf(">> %d\n", lm->elements_indices[5]);
    printf(">> %d\n", lm->elements_indices[13]);

    printf(">> Elements: \n");
    for (size_t i = 1; i < lm->elements_count; i++) {
        printf("\t%s;\n", lm->elements_list[i]->label);
    }
    printf("\n");

    for (size_t j = 0; j < lm->size; j++) {
        for (size_t i = 0; i < lm->size; i++) {
            char c;
            if (lm->elements_indices[lmap_linear_coord(lm, i, j)] != LOGICAL_MAP_NULL_INDEX)
            {
                c = lm->elements_list[lm->elements_indices[lmap_linear_coord(lm, i, j)]]->label[0];
            } else c = '_';
            printf("%c", c);
        }
        printf("\n");
    }
}

void lmap_info(logical_map_t* lmap)
{
    printf("[i] Logical Map [%p]:\n", (void*)lmap);
    printf("\tsize = %ld\n", lmap->size);
    printf("\telements count = %ld\n", lmap->elements_count);
}

void lmap_set_stile(logical_map_t* lmap, int i, int j)
{
    lmap->tile_selected = true;
    lmap->ts_x = i;
    lmap->ts_y = j;
    tilex_set_xy(&lmap->ts, (float)i, (float)j);
}

elem_base_t* lmap_get_selected_elem(logical_map_t* lmap)
{
    if (!lmap->tile_selected) return NULL;
    return lmap_get_elem_by_coord(lmap, lmap->ts_x, lmap->ts_y);
}

void lmap_unset_stile(logical_map_t* lmap)
{
    lmap->tile_selected = false;
}

void lmap_set_htile(logical_map_t* lmap, int i, int j)
{
    lmap->tile_hovered = true;
    lmap->th_x = i;
    lmap->th_y = j;
    tilex_set_xy(&lmap->th, (float)i, (float)j);
}

void lmap_unset_htile(logical_map_t* lmap)
{
    lmap->tile_hovered = false;
}

void lmap_get_tile_info(logical_map_t* lmap, char* buf, int x, int y)
{
    char* elem_name = "";
    size_t index = 0;
    elem_base_t* elem = lmap_get_elem_by_coord(lmap, x, y);
    if (elem != LOGICAL_MAP_NULL_ELEM) {
        elem_name = elem_get_name(elem);
        index = lmap_get_elem_index(lmap, elem);
    }
    sprintf(buf, "> tile[%ld]: [%d %d] %s", index, x, y, elem_name);
}

void lmap_get_stile_info(logical_map_t* lmap, char* buf)
{
    if (lmap->tile_selected) lmap_get_tile_info(lmap, buf, lmap->ts_x, lmap->ts_y);
}

void lmap_get_htile_info(logical_map_t* lmap, char* buf)
{
    if (lmap->tile_hovered) lmap_get_tile_info(lmap, buf, lmap->th_x, lmap->th_y);
}

void logical_map_free(logical_map_t* lmap)
{
    free(lmap->elements_indices);
    free(lmap->elements_list);
}
