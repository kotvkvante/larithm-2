#include <stdlib.h>
#include <stdio.h>

#include "common/player/player.h"
#include "common/objs/base.h"
#include "common/objs/wall.h"
#include "common/objs/lever.h"
#include "common/objs/door.h"
#include "common/objs/star.h"
#include "common/map/map.h"
#include "common/tile/tilex.h"
#include "common/tile/htilex.h"
#include "math/random.h"

// #define MAP_RANDOM_OBJ_COUNT 8

map_t* map_new()
{
    map_t* map = malloc(sizeof(map_t));
    map->score = 0;
    map->score_goal = 0;
    /// map->sgoal = SIZE_MAX; /// Вариант инициализации
    map->size = 0;

    map->objs_indices   = NULL;
    map->objs_list      = NULL;
    map->objs_max       = 0;
    map->objs_count     = 0;

    map->tile_selected = false;
    map->ts_x = 0; map->ts_y = 0;
    map->tile_hovered = false;
    map->th_x = 0; map->th_y = 0;

    return map;
}

void map_init(map_t* map, int size, int objects_max)
{
    map->size = size;
    map->objs_indices = malloc(sizeof(size_t) * size * size);
    for (size_t i = 0; i < size * size; i++) {
        map->objs_indices[i] = MAP_NULL_INDEX;
    }

    map->objs_max = objects_max + 1;
    map->objs_count = 1;
    map->objs_list = malloc(sizeof(obj_base_t*) * map->objs_max);
    for (size_t i = 0; i < map->objs_max; i++) {
        map->objs_list[i] = MAP_NULL_OBJ;
    }
}

bool map_is_on_map(map_t* map, int x, int y)
{
    return (x < (int)map->size) && (x >= 0) && (y < (int)map->size) && (y >= 0);
}

obj_base_t* map_get_obj_by_coord(map_t* map, size_t x, size_t y)
{
    size_t index = map->objs_indices[MAP_COORD_TO_INDEX(x, y)];
    return map_get_obj_by_index(map, index);
}

obj_base_t* map_get_obj_by_index(map_t* map, size_t obj_index)
{
    return map->objs_list[obj_index];
}

obj_base_t* map_get_sobj(map_t* map)
{
    if (!map->tile_selected) return NULL;
    return map_get_obj_by_coord(map, map->ts_x, map->ts_y);
}

obj_base_t* map_get_hobj(map_t* map)
{
    LA2_UNUSED(map);
    LA2_NOTIMPL();
    return NULL;
}

void map_init_obj(map_t* map, obj_type_e type, int x, int y, int state)
{
    switch (type) {
        case OBJ_TYPE_BASE: break;

        case OBJ_TYPE_WALL: {
            obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, x, y);
            map_set_obj_xy(map, x, y, (obj_base_t*)wall);
        } break;

        case OBJ_TYPE_DOOR: {
            obj_door_t* door = obj_door_new();
            obj_door_init(door, x, y, state);
            map_set_obj_xy(map, x, y, (obj_base_t*)door);
        } break;

        case OBJ_TYPE_LEVER: {
            obj_lever_t* lever = obj_lever_new();
            obj_lever_init(lever, x, y, state);
            map_set_obj_xy(map, x, y, (obj_base_t*)lever);
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_t* star = obj_star_new();
            obj_star_init(star, x, y);
            map_set_obj_xy(map, x, y, (obj_base_t*)star);
        } break;

        default: {
            printf("[w] `%s`: untracked obj type: '%d'.\n", __func__, type);
        } break;
    }
}

void map_set_obj(map_t* map, size_t obj_index, obj_base_t* obj)
{
    map->objs_list[map->objs_count] = obj;
    map->objs_indices[obj_index] = map->objs_count;
    map->objs_count++;
}

void map_set_obj_xy(map_t* map, size_t x, size_t y, obj_base_t* obj)
{
    size_t obj_index = x + y * map->size;
    map_set_obj(map, obj_index, obj);
}


void map_set_obj_copy(map_t* map, obj_base_t* obj, size_t x, size_t y)
{
    if (obj == NULL) {
        printf("[w] `%s`: obj == NULL", __func__);
        return;
    }

    switch (obj->type) {
        case OBJ_TYPE_BASE: break;

        case OBJ_TYPE_WALL: {
            obj_wall_t* wall = obj_wall_new();
            obj_wall_init(wall, x, y);
            map_set_obj_xy(map, x, y, (obj_base_t*)wall);
        } break;

        case OBJ_TYPE_DOOR: {
            obj_door_t* door = obj_door_new();
            obj_door_init(door, x, y, ((obj_door_t*)obj)->is_open);
            map_set_obj_xy(map, x, y, (obj_base_t*)door);
        } break;

        case OBJ_TYPE_LEVER: {
            obj_lever_t* lever = obj_lever_new();
            obj_lever_init(lever, x, y, ((obj_lever_t*)obj)->state);
            map_set_obj_xy(map, x, y, (obj_base_t*)lever);
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_t* star = obj_star_new();
            obj_star_init(star, x, y);
            map_set_obj_xy(map, x, y, (obj_base_t*)star);
        } break;

        default: {
            printf("[w] `%s`: untracked obj type: '%d'.\n", __func__, obj->type);
        } break;
    }
}

// void map_set_objf(map_t* map, size_t obj_index, obj_base_t* obj, tflags_t f)
// {
//     map->objs_list[map->objs_count] = obj;
//     map->objs_indices[obj_index] = map->objs_count;
//     map->objs_count++;
//
//     //
// }

void map_info(map_t* map)
{
    printf("[i] Map [%p]:\n", (void*)map);
    printf("\tscore = %ld\n", map->score);
    printf("\tscore goal = %ld\n", map->score);
    printf("\tsize = %ld\n", map->size);
    printf("\tobjects count = %ld\n", map->objs_count);
    printf("\tobjects max = %ld\n", map->objs_max);
}

bool map_is_valid_player_pos(map_t* map, int x, int y)
{
    if (!map_is_on_map(map, x, y)) return false;
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);
    // int index = map->objs_indices[MAP_COORD_TO_INDEX(x, y)];
    // obj_base_t* obj = map->objs_list[index];

    if (obj == MAP_NULL_OBJ) return true;
    switch (obj->type) {
        case OBJ_TYPE_WALL:
            return false;
        case OBJ_TYPE_DOOR:
            return ((obj_door_t*)obj)->is_open;
        default:
            return true;
    }
}

/// it is possible to union this function and map_tile_interact
bool map_is_tile_interactable(map_t* map, int x, int y)
{
    if (!map_is_on_map(map, x, y)) return false;
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);

    if (obj == MAP_NULL_OBJ) return false;
    switch (obj->type) {
        case OBJ_TYPE_LEVER:
            return true;
        case OBJ_TYPE_STAR:
            return obj_star_is_collected((obj_star_t*)obj);
        default:
            return false;
    }
}

bool map_is_tile_interactable_reverse(map_t* map, int x, int y)
{
    if (!map_is_on_map(map, x, y)) return false;
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);

    if (obj == MAP_NULL_OBJ) return false;
    switch (obj->type) {
        case OBJ_TYPE_LEVER:
            return true;
        case OBJ_TYPE_STAR:
            return !obj_star_is_collected((obj_star_t*)obj);
        default:
            return false;
    }
}

void map_tile_interact(map_t* map, int x, int y)
{
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);
    switch (obj->type)
    {
        case OBJ_TYPE_LEVER: {
            obj_lever_turn((obj_lever_t*)obj);
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_collect((obj_star_t*)obj);
            map->score++;
        } break;
    }
}

void map_tile_interact_reverse(map_t* map, int x, int y)
{
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);
    switch (obj->type)
    {
        case OBJ_TYPE_LEVER: {
            obj_lever_turn((obj_lever_t*)obj);
        } break;

        case OBJ_TYPE_STAR: {
            obj_star_drop((obj_star_t*)obj);
            map->score--;
        } break;
    }
}



void map_set_stile(map_t* map, int i, int j)
{
    map->tile_selected = true;
    map->ts_x = i;
    map->ts_y = j;
    tilex_set_xy(&map->ts, (float)i, (float)j);
}

void map_unset_stile(map_t* map)
{
    map->tile_selected = false;
}

void map_set_htile(map_t* map, int i, int j)
{
    map->tile_hovered = true;
    map->th_x = i;
    map->th_y = j;
    tilex_set_xy(&map->th, (float)i, (float)j);
}

void map_unset_htile(map_t* map)
{
    map->tile_hovered = false;
}

void map_get_tile_info(map_t* map, char* buf, int x, int y)
{
    char* obj_name = "";
    obj_base_t* obj = map_get_obj_by_coord(map, x, y);
    if (obj != MAP_NULL_OBJ) obj_name = obj_get_name(obj);
    sprintf(buf, "> tile: [%d %d] %s", x, y, obj_name);
}

void map_get_stile_info(map_t* map, char* buf)
{
    if (!map->tile_selected) return;
    map_get_tile_info(map, buf, map->ts_x, map->ts_y);
}

void map_get_htile_info(map_t* map, char* buf)
{
    if (!map->tile_hovered) return;
    map_get_tile_info(map, buf, map->th_x, map->th_y);
}

void map_free(map_t* map)
{
    LA2_UNUSED(map);
    LA2_NOTIMPL();
    // free(map->objects_indices);
    // free(map->objects_list);
}
