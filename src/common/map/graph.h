#ifndef GRAPH_H
#define GRAPH_H

#define MAXV 64

#include <stdbool.h>
#include "utils/utils.h"

typedef struct edgenode_t {
    int y;
    struct edgenode_t *next;
} edgenode_t;

typedef struct graph_t {
    edgenode_t* edges[MAXV+1];
    int degree[MAXV+1];
    int nvertices;
    int nedges;
    bool directed;
} graph_t;

graph_t* graph_new();
void graph_init(graph_t* g, bool directed);
// void graph_read(graph_t* g, bool directed);
void graph_read_from_str(graph_t* g, bool directed, char* str);
void graph_read_from_file(graph_t* g, bool directed, char* filepath);
void graph_insert_edge(graph_t* g, int x, int y, bool directed);
void graph_print(graph_t* g);

#endif /* end of include guard: GRAPH_H */
