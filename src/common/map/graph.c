#include <stdlib.h>
#include <stdio.h>

#include "graph.h"

graph_t* graph_new()
{
    return malloc(sizeof(graph_t));
}

edgenode_t* edgenode_new()
{
    return malloc(sizeof(edgenode_t));
}

void graph_init(graph_t* g, bool directed)
{
    g->nvertices = 0;
    g->nedges    = 0;
    g->directed = directed;
    for (int i = 0; i <= MAXV; i++)
    {
        g->degree[i] = 0;
        g->edges[i] = NULL;
    }
}

/// Нужно: Исправить небезопасное сканирование строки
void graph_read_from_str(graph_t* g, bool directed, char* str)
{
    int n; /// Число прочитанных байтов строки
    int m;
    int x, y;
    graph_init(g, directed);
    if (sscanf(str, "%d %d%n", &(g->nvertices), &m, &n) != 2) {
        printf("[w] Failed to read graph from string: `%s`\n", str);
        return;
    }

    str+=n;
    // printf("[i] %d %d\n", g->nvertices, m);

    for (int i = 0; i < m; i++) {
        sscanf(str, "%d %d%n", &x, &y, &n);
        str+=n;
        // printf("[i] %d %d\n", x, y);
        graph_insert_edge(g, x, y, directed);
    }
}

void graph_read_from_file(graph_t* g, bool directed, char* filepath)
{
    char* str = file_read(filepath);
    if (str == NULL) {
        printf("[E] Failed to read file: `%s`\n", filepath);
        return;
    }
    graph_read_from_str(g, directed, str);

    free(str);
}

// void graph_read(graph_t* g, bool directed)
// {
//     int m;
//     int x, y;
//
//     graph_init(g, directed);
//     scanf("%d %d", &(g->nvertices), &m);
//     for (int i = 1; i <= m; i++) {
//         scanf("%d %d", &x, &y);
//         graph_insert_edge(g, x, y, directed);
//     }
// }

void graph_insert_edge(graph_t* g, int x, int y, bool directed)
{
    edgenode_t *p = edgenode_new();
    // p->weight = 0;
    p->y = y;
    p->next = g->edges[x];
    g->edges[x] = p;
    g->degree[x]++;
    if (directed == false)
        graph_insert_edge(g, y, x, True);
    else
        g->nedges++;
}

void graph_print(graph_t* g)
{
    printf("[i] Graph: %p\n", (void*)g);
    edgenode_t* p;
    for (int i = 0; i < g->nvertices; i++) {
        printf("\t%d: ", i);
        p = g->edges[i];
        while(p != NULL) {
            printf(" %d", p->y);
            p = p->next;
        }
        printf("\n");
    }
}
