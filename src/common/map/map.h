#ifndef MAP_H
#define MAP_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "common/objs/types.h"
#include "common/elems/base.h"
#include "common/tile/htilex.h"

#define MAP_COORD_TO_INDEX(x, y) ((x) + (y) * (map->size))
#define MAP_INDEX_TO_X(index) ((index) % (map->size))
#define MAP_INDEX_TO_Y(index) ((index) / (map->size))

typedef struct player_t player_t;
typedef struct obj_base_t obj_base_t;

// typedef enum {
//     TF_SOLID = 1 << 0,
//     TF_INTERRACTABLE = 1 << 1,
//     TF_TRIG_ON_LEAVE = 1 << 2,
//     TF_TRIG_ON_ENTER = 1 << 3,
//     TF_COLLECTABLE   = 1 << 4,
// } tflags_e;

typedef uint32_t tflags_t;

typedef struct map_t
{
    size_t score; /// ~ collected stars count
    size_t score_goal;

    size_t size;
    size_t* objs_indices;
    /// tile flags
    // tflags_t* tflags;

    obj_base_t** objs_list;
    size_t objs_max;
    size_t objs_count;

    //
    bool tile_selected;
    int ts_x, ts_y;
    tilex_t ts;

    bool tile_hovered;
    int th_x, th_y;
    tilex_t th;

    void* ht;
} map_t;

map_t* map_new();

void map_init(map_t* map, int size, int objects_max);

void map_repr_init(map_t* map);
bool map_is_on_map(map_t* map, int x, int y);

void map_init_obj(map_t* map, obj_type_e type, int x, int y, int state);
void map_set_obj(map_t* map, size_t obj_index, obj_base_t* obj);
void map_set_obj_xy(map_t* map, size_t x, size_t y, obj_base_t* obj);
void map_set_obj_copy(map_t* map, obj_base_t* obj, size_t x, size_t y);

// obj_base_t* map_get_obj(map_t* map, size_t obj_index);
obj_base_t* map_get_obj_by_index(map_t* map, size_t obj_index);
obj_base_t* map_get_obj_by_coord(map_t* map, size_t x, size_t y);
obj_base_t* map_get_sobj(map_t* map); /// sobj := selected obj
obj_base_t* map_get_hobj(map_t* map); /// hobj := hovered obj

// player stuff
bool map_is_valid_player_pos(map_t* map, int x, int y);
bool map_is_tile_interactable(map_t* map, int x, int y);
bool map_is_tile_interactable_reverse(map_t* map, int x, int y);
void map_tile_interact(map_t* map, int x, int y);
void map_tile_interact_reverse(map_t* map, int x, int y);


// void map_print(map_t* map);
void map_info(map_t* map);

/// stile = selected tile
/// htile = hovered tile
void map_set_stile(map_t* map, int i, int j);
void map_unset_stile(map_t* map);
void map_set_htile(map_t* map, int i, int j);
void map_unset_htile(map_t* map);

void map_get_tile_info(map_t* map, char* buf, int x, int y);
void map_get_stile_info(map_t* map, char* buf);
void map_get_htile_info(map_t* map, char* buf);


void map_free(map_t* map);

#endif /* end of include guard: MAP_H */
