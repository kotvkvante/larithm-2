#ifndef CONF_READER_H
#define CONF_READER_H

typedef struct StackNode StackNode;

StackNode* checkStackNode(StackNode* sn, char* name);

char* getGroupName(char* s, char** group);
char* getNameStr(char* s, char** name, char** str);
char* makeStackName(char* group, char* name);

int getConfigInt(StackNode* sn, char* name);
float getConfigFloat(StackNode* sn, char* name);
char* getConfigStr(StackNode* sn, char* name);
// StackNode* loadConfig(char* filename);

int   isSpace(char* s);
char* dropSpace(char* s);
char* matchSymbol(char* s, char c);
int   isAlpha(char* s);
int   isNumeric(char* s);
char* getToken(char* s, char** token);
char* getString(char* s, char** str);


#endif /* end of include guard: CONF_READER_H */
