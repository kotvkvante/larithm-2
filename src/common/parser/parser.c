#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "common/parser/parser.h"
#include "common/parser/stacknode.h"
#include "common/parser/conf_reader.h"

extern unsigned int str_cnt;

// StackNode* parse_file(char* filename)
// {
//     FILE* fp = fopen(filename, "r");
//     if(!fp)
//     {
//       printf("Can't open file: \'%s\'\n", filename);
//       exit(0);
//     }
//
//     fseek(fp, 0, SEEK_END);
//     long int size = ftell(fp);
//     fseek(fp, 0, SEEK_SET);
//     char* buf = malloc(size +  1);
//     fread(buf, 1, size, fp);
//     fclose(fp);
//
//     ((char*)buf)[size]='\0';
//
//     StackNode* sn = parse_str(buf);
//
//     free(buf);
//     return sn;
// }

StackNode* parse_str(char* fbuf)
{
    size_t len = strlen(fbuf);
    ((char*)fbuf)[len-1]='\0';

    char* s = fbuf;
    StackNode* sn = NULL;

    str_cnt = 0;

    char* name;
    char* str;
    char* group = NULL;

    while (*s != '\0')
    {
        s = dropSpace(s);
        if(*s == '[')
        {
          if (group != NULL) free(group);
          s = getGroupName(s, &group);
        }
        else if (isAlpha(s) || isNumeric(s))
        {
          s = getNameStr(s, &name, &str);
          char* groupname = makeStackName(group, name);
          sn = pushStackNode(sn);
          setStackNodeName(sn, groupname);
          setStackNodeData(sn, str, strlen(str)+1);
          free(name);
          free(str);
          free(groupname);
        }
        else if(*s != 0x00)
        {
          printf("(%d) Error: name or chapter expected.\n", str_cnt+1);
          exit(0);
        }
    }
    if (group != NULL) free(group);

    return sn;
}
