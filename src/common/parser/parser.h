#ifndef PARSER_H
#define PARSER_H

typedef struct StackNode StackNode;

StackNode* parse_str(char* fbuf);
StackNode* parse_file(char* filename);

#endif /* end of include guard: PARSER_H */
