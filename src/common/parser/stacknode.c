#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stacknode.h"

StackNode* pushStackNode(StackNode *sn)
{
	StackNode *t = (StackNode*) malloc(sizeof(StackNode));
	t->name = NULL;
	t->data = NULL;
	t->next = sn;
	return t;
}

StackNode* popStackNode(StackNode *sn)
{
	if (!sn) return NULL;
	if (sn->name) free(sn->name);
	if (sn->data) free(sn->data);

	StackNode *t = sn->next;
	free(sn);

	return t;
}

StackNode* findStackNode(StackNode *sn, char *name)
{
	while (sn)
	{
		if (strcmp(sn->name, name) == 0)
			return sn;
		sn = sn->next;
	}

	return NULL;
}

void freeStack(StackNode *sn)
{
	while (sn)
	{
		sn = popStackNode(sn);
	}
}

void setStackNodeName(StackNode *sn, char *name)
{
	if (sn->name) free(sn->name);

	sn->name = (char*) malloc(strlen(name) + 1);
	strcpy(sn->name, name);
}

void setStackNodeData(StackNode *sn, void *data, unsigned int size)
{
	if (sn->data) free(sn->data);

	sn->data = malloc(size);
	memcpy(sn->data, data, size);
}
