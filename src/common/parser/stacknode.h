#ifndef STACKNODE_H
#define STACKNODE_H

typedef struct StackNode
{
  char* name;
  void* data;
  struct StackNode* next;
} StackNode;

StackNode* pushStackNode(StackNode* sn);
StackNode* popStackNode(StackNode* sn);
StackNode* findStackNode(StackNode* sn, char* name);
void freeStack(StackNode* sn);

void setStackNodeName(StackNode* sn, char* name);
void setStackNodeData(StackNode* sn, void* data, unsigned int size);
#endif /* end of include guard: STACKNODE_H */
