#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stacknode.h"

#ifndef CONF_READER_C
#define CONF_READER_C

unsigned int str_cnt;

int isSpace(char* s)
{
	return (*s == 0x20) || (*s == 0x09) || (*s == 0x07) || (*s == 0x0a) || (*s == 0x0d);
}

char* dropSpace(char* s)
{
	while ((isSpace(s) || *s == ';' || *s == '#') && (*s != 0x00))
	{
		if (*s == ';' || *s == '#')
			while (*s != 0x0a && *s != 0x00) s++;
		if (*s == 0x0a) str_cnt++;
		if (*s == 0x00) return s;
		s++;
	}

	return s;
}

char* matchSymbol(char* s, char c)
{
	s = dropSpace(s);
	if (*s == c)
	{
		s++;
		return s;
	}
	else
	{
		printf("(%d) \'%c\' expected, but \'%c\' received.\n", str_cnt + 1, c, *s);
		exit(0);
	}
}

int isAlpha(char* s)
{
	return ((*s >= 'a') && (*s <= 'z')) || ((*s >= 'A') && (*s <= 'Z'));
}

int isNumeric(char* s)
{
	return (*s >= '0') && (*s <= '9');
}

char* getToken(char* s, char** token)
{
	s = dropSpace(s);
	char* start = s;
	char c;
	while ((isNumeric(s) || *s == '.' || isAlpha(s) || *s == '_') && *s != 0x00)
	{
		s++;
	}

	*token = (char*)malloc(sizeof(char) * (s - start + 1));
	if (*s != 0x00)
	{
		c = *s;
		*s = 0x00;
		strcpy(*token, start);
		*s = c;
	}
	else
	{
		strcpy(*token, start);
	}

	return s;
}

char* getString(char* s, char** str)
{
	s = dropSpace(s);
	char c = *s;
	s++;

	char* start = s;
	while ((*s != 0x00) && (*s != c))
	{
		if (*s == 0x0a) str_cnt++;
		s++;
	}

	if (*s == c)
	{
		*s = 0x00;
		unsigned int len = s - start + 1;
		*str = (char*) malloc(len);
		strcpy(*str, start);
		*s = c;
	}
	else
	{
		printf("\'%c\' missing.\n", c);
		exit(0);
	}

	s++;
	return s;
}

StackNode* checkStackNode(StackNode *sn, char* name)
{
	StackNode *t = findStackNode(sn, name);
	if (!t)
	{
		printf("Can't find property '%s'\n", name);
		exit(0);
	}
	else
		return t;
}

int getConfigInt(StackNode *sn, char* name)
{
	StackNode *t = checkStackNode(sn, name);
	return atoi(t->data);
}

float getConfigFloat(StackNode *sn, char* name)
{
	StackNode *t = checkStackNode(sn, name);
	return atof(t->data);
}

char* getConfigStr(StackNode *sn, char* name)
{
	StackNode *t = checkStackNode(sn, name);
	return t->data;
}

char* getGroupName(char* s, char** group)
{
	s = matchSymbol(s, '[');
	s = getToken(s, group);
	s = matchSymbol(s, ']');
	return s;
}

char* getNameStr(char* s, char** name, char** str)
{
	s = getToken(s, name);
	s = matchSymbol(s, '=');
	s = getString(s, str);
	return s;
}

char* makeStackName(char* group, char* name)
{
	char* groupname;
	char* splitter = ".";
	if (!group)
	{
		group = "";
		splitter = "";
	}

	groupname = (char*) malloc(strlen(group) + strlen(splitter) + strlen(name) + 1);
	groupname[0] = 0x00;
	strcat(groupname, group);
	strcat(groupname, splitter);
	strcat(groupname, name);
	return groupname;
}

#endif /*end of include guard: CONF_READER_C */
