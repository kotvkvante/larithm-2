from fastapi import FastAPI
import psycopg
from psycopg.rows import dict_row
from fastapi.responses import PlainTextResponse

app = FastAPI()

try:
    connection = psycopg.connect(
        "host=localhost dbname=larithm user=postgres password=111",
        row_factory=dict_row)
    cursor = connection.cursor()
    print("Seccesfull: ", connection)
except Exception as e:
    print(e)


@app.get("/")
def root():
    return {"Message": "Hello world!" }

@app.get("/levels")
def get_levels():
    levels = cursor.execute("""SELECT id, author, format FROM levels LIMIT 5""").fetchall()

    if (levels is None):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail = f"Levels was not found")

    return {"count": len(levels), "levels": levels}


@app.get("/levels/{id}")
def get_level(id: int):
    cursor.execute("""SELECT id, author, format FROM levels WHERE id = %s""", (id, ))
    level = cursor.fetchone()

    if (level is None):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail = f"Level with id: {id} was not found")

    return level

@app.get("/levels/{id}/raw_data", response_class=PlainTextResponse)
def get_level_raw_data(id: int):
    cursor.execute("""SELECT raw_data FROM levels WHERE id = %s""", (id, ))
    raw_data = cursor.fetchone()

    if (raw_data is None):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail = f"post with id: {id} was not found")


    return raw_data["raw_data"]
