import socket

from _thread import *

from config import *

OK = True
NOT_OK = False

def _process_hi(parsed):
    return True, "hi"

def _process_connect(parsed):
    cmd, args = parsed
    return True, args

def _process_bye(parsed):
    return False, "bye"

def _process_default(parsed):
    return True, parsed[0]

COMMAND_PROCESS = {
    "hi": _process_hi,
    "connect": _process_connect,
    "start": _process_default,
    "stop": _process_default,
    "close": _process_default,
    "bye": _process_bye,
}

class ServerPython():
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.is_inited = False

    def init(self):
        if self.is_inited:
            return

        self.server = socket.socket()
        self.server.bind((self.ip, self.port))
        self.server.listen()
        self.is_inited = True

    def recv_message(self, connection, message_size=DEFAULT_MESSAGE_SIZE):
        data = connection.recv(message_size)
        message = data.decode('utf-8')
        return message

    def parse_message(self, message):
        return message.split(" ", 1)

    def process_parsed_message(self, parsed_message):
        process = COMMAND_PROCESS.get(parsed_message[0])
        ok, response = process(parsed_message)
        return ok, response
        # print("Server >", "1")
        # connection.sendall(str.encode("1"))

    def send_response(self, connection, responce):
        connection.sendall(str.encode(responce))
        # print("Server >", "bye")
        # break


    def process(self, connection):
        message = self.recv_message(connection)
        parsed = self.parse_message(message)
        ok, response = self.process_parsed_message(parsed)

        # print(command, args)

        self.send_response(connection, response)

        return ok
        # result  = self.process_command(command, args)
        # return result


    def connection_handler(self, connection):
        # connection.send(str.encode('You are now connected to the replay server...'))
        data = connection.recv(DEFAULT_MESSAGE_SIZE)
        message = data.decode('utf-8')
        message, client_id = message.split(" ")
        if message == "hi":
            print("Client_{} >".format(client_id), message)
            connection.sendall(str.encode("hi"))
            print("Server >", "hi")
        else:
            connection.close()
            return

        running = True
        while running:
            running = self.process(connection)

            # data = connection.recv(MESSAGE_SIZE)
            # message = data.decode('utf-8')
            # print("Client_{} >".format(client_id), message)
        connection.close()

    def accept_connections(self, server):
        connection, address = server.accept()
        print(f'Connected to: {address[0]}:{str(address[1])}')
        start_new_thread(self.connection_handler, (connection, ))

    def loop(self):
        while(True):
            self.accept_connections(self.server)

    def close(self):
        self.server.close()


def main():
    dm = ServerPython(*ADDR_PORT)

    dm.init()
    dm.loop()
    dm.close()


if __name__ == '__main__':
    main()
