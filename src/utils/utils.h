#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

// Нужно: убрать эти определения
#define False 0
#define True 1

#define LA2_UNUSED(x) (void)(x)
#define LA2_DEAFULT_CASE() {}
#define LA2_DEAFULT_CASE_WARN() { printf("[W] Default case: `%s`.\n", __func__); }

#define LA2_NOTIMPL() printf("> Not Implemented: '%s' \n", __func__)

#define LA2_COUNTER(var, type, initval, incval, maxval) \
    static type var = initval; \
    var += incval; \
    if (var > maxval) var = initval

/* #define LA2_REVCOUNTER(var, type, initval, incval, maxval) \
     static type var = initval; \
     var += incval; \
     if (var < maxval) var = initval */


#define LA2_BCOUNTER(var) LA2_COUNTER(var, int, 0, 1, 1)
#define LA2_ICOUNTER(var, initval, incval, maxval) LA2_COUNTER(var, int, initval, incval, maxval)

#define IF_FF_RETURN_CODE(str, code, str_to_code) \
	if (strcmp(str, str_to_code[code]) == 0) { \
		return code; \
	} while(0)

#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

const char* get_filename_ext(const char *filename);
char* file_read(const char *path);


#endif /* end of include guard: UTILS_H */
