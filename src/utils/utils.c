#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const char* get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

/// Украдено из библиотеки frozen
char* file_read(const char *path)
{
    FILE *fp;
    char *data = NULL;
    if ((fp = fopen(path, "rb")) == NULL)
    {

    }
    else if (fseek(fp, 0, SEEK_END) != 0)
    {
        fclose(fp);
    }
    else
    {
        long size = ftell(fp);
        if (size > 0 && (data = (char *) malloc(size + 1)) != NULL) {
            fseek(fp, 0, SEEK_SET); /* Some platforms might not have rewind(), Oo */
            if (fread(data, 1, size, fp) != (size_t) size) {
                free(data);
                data = NULL;
            } else {
            data[size] = '\0';
            }
        }
        fclose(fp);
    }
    return data;
}
