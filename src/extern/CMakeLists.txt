# add_subdirectory(glfw)

# glad
if(USE_LIB_GLAD)
    add_library(glad STATIC
        glad/src/glad.c
    )

    target_include_directories(glad PUBLIC
        glad/include
    )

    target_link_libraries(larithm2
        glad
    )
endif(USE_LIB_GLAD)

if(USE_LIB_FROZEN)
    add_library(frozen STATIC
        "frozen/frozen.c"
        "frozen_ex/frozen_ex.c"
    )

    target_include_directories(frozen PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/frozen/"
        "${CMAKE_CURRENT_SOURCE_DIR}/frozen_ex/"
    )

    target_link_libraries(larithm2
        frozen
    )
endif(USE_LIB_FROZEN)

# nuklear
target_include_directories(laclient PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/Nuklear"
)


# stb
target_include_directories(laclient PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/stb/"
)
