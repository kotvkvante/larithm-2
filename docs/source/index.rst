.. larithm2 documentation master file, created by
   sphinx-quickstart on Fri Sep 29 09:42:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to larithm2's documentation!
====================================

**Larithm2** (/la'rithm 2/) is a computer game.

.. note::
    This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Contents
--------

.. toctree::

    build
    game


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
