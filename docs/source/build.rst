Build
=====

This is instructions discribe how to build Larithm 2 on Linux (Ubuntu).
First of all you need to install:

#. CMake
#. GCC


Source code
-----------

.. code-block:: console

    git clone https://github.com/kotvkvante/larithm2.git

Configure
---------

Basic configuration:

.. code-block:: console

    cd larithm2
    mkdir _build
    cd _build
    cmake ../

Debug configuration:

.. code-block:: console

    cd larithm2
    mkdir _debug
    cd _debug
    cmake -DCMAKE_BUILD_TYPE=Debug ../

Compile
-------

.. code-block:: console

    make
    make install

Run
---

.. code-block:: console

    ./laclient
