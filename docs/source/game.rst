Game
====

Idea
----

Main idea of the Larithm 2 game is build sequence of commands to collect all stars on map.
You can't see your solution step by step, only execute all of it. This means you need to think it over
in your head and then enter. This is inspired by turing machine. See: turing_machine_.

.. _turing_machine: https://en.wikipedia.org/wiki/Turing_machine

Game level is consists of map and logical map. You can change your character coordinates on map with movements commands.
You can interract with map objects through operate command.
Map actions are trasferred to the logical map and then calculated and sent back to the map to affect objects.

Modes
-----

There are different modes you can play in:

#. Singleplayer (work in progress)
#. Multiplayer (not implemented)
#. Map Editor (not implemented)
